//
//  UIImage+SchemeManager.m
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "UIImage+SchemeManager.h"

@implementation UIImage (SchemeManager)

+ (UIImage *)backButtonBackground
{
    return [UIImage imageNamed:@"backbtn_white"];
}

+ (UIImage *)overlayCloseButton
{
    return [UIImage imageNamed:@"close_btn"];
}

+ (UIImage *)overlayScanButtonBackground
{
    return [UIImage imageNamed:@"scan_btn_overlay"];
}

+ (UIImage *)overlayAbortButtonBackground
{
    return [UIImage imageNamed:@"abort_btn"];
}

+ (UIImage *)locationSelected
{
    return [UIImage imageNamed:@"marker_active"];
}
+ (UIImage *)locationNotSelected
{
    return [UIImage imageNamed:@"locationicon"];
}
@end
