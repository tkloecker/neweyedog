//
//  UIImage+SchemeManager.h
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SchemeManager)

+ (UIImage *)backButtonBackground;

+ (UIImage *)overlayCloseButton;

+ (UIImage *)overlayScanButtonBackground;
+ (UIImage *)overlayAbortButtonBackground;

+ (UIImage *)locationSelected;
+ (UIImage *)locationNotSelected;
@end
