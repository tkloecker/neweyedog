//
//  SchemeHolderViewController.h
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIColor+SchemeManager.h"
#import "UIImage+SchemeManager.h"

@interface SchemeHolderViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *navigationView;
@property (nonatomic, weak) IBOutlet UIButton *m_navBackBtn;
@property (nonatomic, weak) IBOutlet UILabel *m_labelNavTitle;

/**
 *  Used for adding appearence in childs. Call super to perform basic config.
 */
- (void)shvcConfigureAppearence;
@end