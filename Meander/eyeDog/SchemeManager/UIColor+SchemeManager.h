//
//  UIColor+SchemeManager.h
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (SchemeManager)

+ (UIColor *)navigationBarColor;
+ (UIColor *)backButtonTitleColor;
+ (UIColor *)titleFontColor;

+ (UIColor *)mainViewBackgroundColor;

+ (UIColor *)labelsTextColor;
+ (UIColor *)buttonsTextColor;
+ (UIColor *)buttonsBackgroundColor;

+ (UIColor *)overlayColor;

+ (UIColor *)notActiveTextColor;
+ (UIColor *)activeTextColor;
@end
