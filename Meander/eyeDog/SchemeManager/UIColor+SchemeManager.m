//
//  UIColor+SchemeManager.m
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "UIColor+SchemeManager.h"

@implementation UIColor (SchemeManager)

+ (UIColor *)navigationBarColor
{
    return [UIColor colorWithRed:0.235 green:0.498 blue:0.54 alpha:1];
}

+ (UIColor *)backButtonTitleColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)titleFontColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)mainViewBackgroundColor
{
    return [UIColor colorWithWhite:0.925 alpha:1];
}

+ (UIColor *)labelsTextColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)buttonsTextColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)buttonsBackgroundColor
{
    return [UIColor colorWithRed:0.957 green:0.505 blue:0.157 alpha:1];
}

+ (UIColor *)overlayColor
{
    return [UIColor colorWithWhite:0 alpha:0.9];
}

+ (UIColor *)activeTextColor
{
    return [UIColor colorWithRed:0.957 green:0.505 blue:0.157 alpha:1];
}

+ (UIColor *)notActiveTextColor
{
    return [UIColor colorWithWhite:0.67 alpha:1];
}
@end
