//
//  SchemeHolderViewController.m
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "SchemeHolderViewController.h"

#import "HHTabListController.h"

@interface SchemeHolderViewController ()

@end

@implementation SchemeHolderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self shvcConfigureAppearence];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)shvcConfigureAppearence
{
    //Navigation view
    self.navigationView.backgroundColor = [UIColor navigationBarColor];
    
    //Back button
    [self.m_navBackBtn setTitleColor:[UIColor backButtonTitleColor] forState:UIControlStateNormal];
    [self.m_navBackBtn setBackgroundImage:[UIImage backButtonBackground] forState:UIControlStateNormal];
    
    //Title label
    self.m_labelNavTitle.backgroundColor = [UIColor clearColor];
    self.m_labelNavTitle.textColor = [UIColor titleFontColor];
    self.m_labelNavTitle.text = self.title;
    
    self.view.backgroundColor = [UIColor mainViewBackgroundColor];
}

/**
 *  Navigation action
 *
 *  @param sender Button expected
 */
- (IBAction)onTouchNavBackBtn:(id)sender
{
    HHTabListController *tabListController = [self tabListController];
    [tabListController goToMainPage];
    /*if ([self.parentViewController respondsToSelector:@selector(goToMainPage)]) {
        [(id)self.parentViewController goToMainPage];
    } else {
        [self.navigationController popViewControllerAnimated: YES];
    }
     */
}
@end
