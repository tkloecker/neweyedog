
#import "Node.h"
#import "Util.h"

@implementation Node

const unsigned long ID_POSITION = 0;
const unsigned long LEVEL_POSITION = 1;
const unsigned long X_POSITION = 2;
const unsigned long Y_POSITION = 3;
const unsigned long TYPE_POSITION = 4;
const unsigned long NAME_POSITION = 5;
const unsigned long ZONE_POSITION = 6;
const unsigned long CATEGORY_POSITION = 7;
const unsigned long PUSH_FACTOR_POSITION = 8;
const unsigned long SHOW_TOP_VIEW_POSITION = 9;
const unsigned long NUM_NODE_ENTRIES = 10;

const double elevatorConst = 10;

-(id) initWithId:(int) _theId
           level:(int) _level
               x:(int) _x
               y:(int) _y
            type:(NSString*) _type
          nameId:(NSString*) _nameId
            zone:(NSString*) _zone
        category:(int) _category
      pushFactor:(int) _pushFactor
     showTopView:(BOOL) _showTopView
{
    self = [super init];
    if (!self) {
        return nil;
    }
    theId = _theId;
    level = _level;
    x = _x;
    y = _y;
    type = _type;
    nameId = _nameId;
    zone = _zone;
    category = _category;
    pushFactor = _pushFactor;
    showTopView = _showTopView;
    numNeighbours = 0;
    return self;
}
-(BOOL) topViewVisible {
    return showTopView;
}
-(void)incNumNeighbours{
    numNeighbours++;
}
-(void)setNumNeighboursToZero {
    numNeighbours = 0;
}


-(void) retrieveNameFromNameIdWithDictinary: (NSMutableDictionary*) dict {
    name = [dict objectForKey: nameId];
}


- (int) getId{
    return theId;
}

- (NSString*) getIdAndName {
    return [[NSString alloc] initWithFormat: @"%d (%@)", theId, name];
}

- (int) getLevel{
    return level;
}

- (int) getX{
    return x;
}

- (int) getY{
    return y;
}

- (NSString*) getType {
    return type;
}

- (NSString*) getNameId {
    return nameId;
}

- (NSString*) getName{
    //return [[NSString alloc] initWithFormat: @"%d (%@)", theId, name];
    return name;
}

- (NSString*) getZone{
    return zone;
}

- (int) getCategory {
    return category;
}

- (int) getPushFactor {
    return pushFactor;
}

- (double) getDist {
    return dist;
}

-(int) getPrev{
    return prev;
}

-(int) getNumNeighbours {
    return numNeighbours;
}

- (int*) getNeighbours{
    return neighbours;
}

- (int*) getNeighbInd {
    return neighbInd;
}

- (double) getDistanceToNeighbour :(int) neighbourIndex {
    return distances[neighbourIndex];
}

- (int)getPathLength: (int)index {
    return pathLengths[index];
}

- (int*)getPath: (int)index {
    return paths[index];
}

- (int*)getPathLengths {
    return pathLengths;
}

- (void) setDist: (double) _dist {
    dist = _dist;
}

- (void) setDistInfinite {
    dist = INFINITY;
}

- (void) setPrev: (int) _prev {
    prev = _prev;
}

- (void) setPathLengths: (int*) _pathLengths {
    pathLengths = _pathLengths;
}

- (void) setPaths: (int**) _paths {
    paths = _paths;
}

- (void) allocWithNumNeighbours:(int) _numNeighbours
                      numNodes:(int)numNodes {
    connections = [[NSMutableArray alloc] init];
    neighbours = malloc(_numNeighbours * sizeof(int));
    numNeighbours = _numNeighbours;
    neighbInd = malloc((1+numNodes) * sizeof(int));
    memset(neighbInd, -1, (1+numNodes) * sizeof(int));
    distances = malloc(_numNeighbours * sizeof(double));
    serverPictures = [[NSMutableArray alloc] init];
    for (int i=0; i<numNeighbours; i++){
        [serverPictures addObject: [[NSMutableArray alloc] init]];
    }
}
-(void)addConnection: (Connection*) conn {
    [connections addObject: conn];
}
-(NSMutableArray*) getConnections {
    return connections;
}
-(Connection*) getConnectionToNode: (int)targetNodeId {
    for (int i=0; i<[connections count]; i++){
        Connection* result = [connections objectAtIndex: i];
        int to = [result getTo];
        if ( to == targetNodeId ){
            return result;
        }
    }
    return nil;
}

- (void) logToConsole {
    NSLog(@"name: %@, id: %d, (lvl, x, y) = (%d, %d, %d), type: %@, zone: %@", name, theId, level, x, y, type, zone);
}

- (void) logToConsole2 {
    NSLog(@"name: %@, id: %d, (lvl, x, y) = (%d, %d, %d), type: %@, zone: %@, neighbours: %@", name, theId, level, x, y, type, zone, [Util intArr2String: neighbours length: numNeighbours]);
}

- (void) logToConsole3 {
    NSLog(@"name: %@, id: %d, (lvl, x, y) = (%d, %d, %d), type: %@, zone: %@, neighbours: %@, distances: %@",
          name, theId, level, x, y, type, zone, [Util intArr2String: neighbours length: numNeighbours], [Util doubleArr2String: distances length: numNeighbours]);
}

- (void) calcDistances: (NSMutableArray*) nodes {
    for (int i=0; i<numNeighbours; i++){
        int neighbourId = neighbours[i];
        Node* neighbourNode = [nodes objectAtIndex: neighbourId];
        distances[i] = [self euclidianDistanceTo: neighbourNode];
    }
}

- (double)getEuclidianPathLength: (int)index {
    return euclidianPathLengths[index];
}

- (void) setEuclidianPathLengths: (double*) _euclidianPathLengths {
    euclidianPathLengths = _euclidianPathLengths;
}

- (double) euclidianDistanceTo: (Node*) otherNode {
    int dx = x - [otherNode getX];
    int dy = y - [otherNode getY];
    int dLvl = level - [otherNode getLevel];
    double dLvl1 = dLvl * elevatorConst;
    return sqrt(dx*dx + dy*dy + dLvl1*dLvl1);
}

- (BOOL) isJunction{
    return ( [type isEqualToString: @"Junction"] || [type isEqualToString: @"Escalator"] );
}

- (BOOL) isDestination{
    return ( [type isEqualToString: @"Destination"] );
}

- (double) getRemainingDistanceTo: (int) destinationNodeId
                  withNextNodeId: (int) nextNodeId
                    pictureIndex: (int) pictureIndex {
    double dist1 = [self getEuclidianPathLength: destinationNodeId];
    int i = neighbInd[nextNodeId];
    double len = distances[i];
    int totalPicsOnThisEdge = [self getNumPicturesToNodeId: nextNodeId];
    double subtract = ( len * (1+pictureIndex) ) / (1+totalPicsOnThisEdge);
    return dist1 - subtract;
}
// pictures
- (void)setPictures: (NSMutableArray*) pics {
    pictures = pics;
}

- (void) addServerPicture: (ServerPicture*) pic {
    int to = [pic getTo];
    int neighbourIndex = neighbInd[to];
    [[serverPictures objectAtIndex: neighbourIndex] addObject: pic];
}

- (void) sortServerPictures {
    for (int i=0; i<numNeighbours; i++){
        NSMutableArray* pics = [serverPictures objectAtIndex: i];
        NSArray* sorted = [pics sortedArrayUsingSelector: @selector(compare:)];
        [serverPictures setObject: sorted atIndexedSubscript: i];
    }
}

- (Picture*) getPictureToNeighbour: (int) neighbourIndex
                 WithPictureIndex: (int) picIndex {
    if ([[pictures objectAtIndex: neighbourIndex] isEqual: [NSNull null]]){
        return nil;
    }
    NSMutableArray* pics = [pictures objectAtIndex: neighbourIndex];
    for (int p=0; p<[pics count]; p++){
        Picture* pic = [pics objectAtIndex: p];
        if ([pic getIndex] == picIndex){
            return pic;
        }
    }
    return nil;
}

- (int) getNumPicturesToNeighbourWithIndex: (int) neighbourIndex {
    if ([[pictures objectAtIndex: neighbourIndex] isEqual: [NSNull null]]){
        return 0;
    }
    return (int)[(NSMutableArray*)[pictures objectAtIndex: neighbourIndex] count];
}

- (void) attachServerPictures {
    for (int i=0; i<numNeighbours; i++){
        NSArray* servPics = [serverPictures objectAtIndex: i];
        int numServ = (int)[servPics count];
        if (numServ != 0){
            NSMutableArray* mergedPictures = [[NSMutableArray alloc] init];
            for (int picIndex = 0; picIndex < numServ; picIndex++){
                ServerPicture* sp = [servPics objectAtIndex: picIndex];
                int pictureIndex = [sp getIndex];   // picture indices as far as file names go start at 1; picIndex, on the other hand is the array position
                if (pictureIndex != picIndex + 1){
                    NSLog(@"unexpected data error / server pictures missing: node %d, neighbour %d, picture %d", theId, neighbours[i], picIndex + 1);
                }
                Picture* lp = [self getPictureToNeighbour: i WithPictureIndex: pictureIndex];
                Picture* mergedPicture = nil;
                if (lp == nil){
                    mergedPicture = [[Picture alloc] initWithServerPicture: sp];
                } else {
                    mergedPicture = lp;
                    [mergedPicture attachServerPicture: sp];
                }
                [mergedPictures addObject: mergedPicture];
            }
            [pictures setObject: mergedPictures atIndexedSubscript: i];
        }
    }
}

- (void) logPicsToConsole {
    for (int i=0; i<numNeighbours; i++){
        NSLog(@"neighbour %d (node %d)", i, neighbours[i]);
        if ([[pictures objectAtIndex: i] isEqual: [NSNull null]]){
            NSLog(@"no pictures");
        } else {
            NSMutableArray* pics = [pictures objectAtIndex: i];
            for (int p=0; p<[pics count]; p++){
                [[pics objectAtIndex: p] logToConsole];
            }
        }
    }
}

- (void) logNonLocalPicsToConsole {
    for (int i=0; i<numNeighbours; i++){
        NSLog(@"neighbour %d (node %d)", i, neighbours[i]);
        if ([[pictures objectAtIndex: i] isEqual: [NSNull null]]){
            NSLog(@"no pictures");
        } else {
            NSMutableArray* pics = [pictures objectAtIndex: i];
            for (int p=0; p<[pics count]; p++){
                Picture* pic = [pics objectAtIndex: p];
                if ([pic picIsHTTP]){
                    [pic logToConsole];
                }
            }
        }
    }
}
- (void) loadFromServerAndStoreInDocumentsIfNecessary {
    for (int i=0; i<numNeighbours; i++){
        if ([[pictures objectAtIndex: i] isEqual: [NSNull null]]){
        } else {
            NSMutableArray* pics = [pictures objectAtIndex: i];
            for (int p=0; p<[pics count]; p++){
                Picture* pic = [pics objectAtIndex: p];
                if ([pic picIsHTTP]){
                    [pic getFromServerAndStore];
                }
            }
        }
    }
}

- (void) logPicsToConsoleShort {
    for (int i=0; i<numNeighbours; i++){
        NSLog(@"neighbour %d (node %d)", i, neighbours[i]);
        if ([[pictures objectAtIndex: i] isEqual: [NSNull null]]){
            NSLog(@"no pictures");
        } else {
            NSMutableArray* pics = [pictures objectAtIndex: i];
            for (int p=0; p<[pics count]; p++){
                [[pics objectAtIndex: p] logToConsoleShort];
            }
        }
    }
}

- (NSMutableArray*) getPicturesToNodeId: (int)nodeId {
    int neighbourIndex = neighbInd[nodeId];
    if (neighbourIndex == -1){
        return nil;
    }
    if ([[pictures objectAtIndex: neighbourIndex] isEqual: [NSNull null]]){
        return nil;
    }
    return [pictures objectAtIndex: neighbourIndex];
}

- (int) getNumPicturesToNodeId: (int)nodeId {
    int neighbourIndex = neighbInd[nodeId];
    if (neighbourIndex == -1){
        return 0;
    }
    if ([[pictures objectAtIndex: neighbourIndex] isEqual: [NSNull null]]){
        return 0;
    }
    return (int)[(NSMutableArray *)[pictures objectAtIndex: neighbourIndex] count];
}


@end

