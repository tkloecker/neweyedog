/*
 this class fetches the csv files from the server, if necessary
 */
@interface CSVimporter : NSObject {
    
    long long csvResourcesTimeStamp;
    long long localCsvLastMod; // time stamp of last modified date of csv files (updated everytime that new csv are d'loaded from server)
    long long serverLastMod;
    
    BOOL loadingNodesFromServerDone;             // set to YES upon succesfull loading of the nodes csv file from server
    BOOL loadingConnectionsFromServerDone;       // ditto connections csv
    BOOL loadingPicturesFromServerDone;          // ditto pictures csv (NOT the pictures themselves)
    BOOL loadingLangDestsFromServerDone;         // ditto for destinations_languages.csv (name id to name in language translation)
    BOOL loadingDestGroupsFromServerDone;        // ditto for des_groups.csv (names of destination groups)
    BOOL loadingTextFromServerDone;              // ditto for text_languages.csv (ui elements captions in various languages)
    BOOL errorOccuredTryingToLoadCsvsFromServer;   // error in one of the preceding 3 downloads
    BOOL csvFromServerFetchedHandlerCalled;        // handler that must be called after all 3 CSV's have arrived - has it already been called?
    
}

+ (CSVimporter*) sharedInstance;

- (void) Initialize;

@end
