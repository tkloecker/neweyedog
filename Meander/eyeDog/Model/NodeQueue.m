#import "NodeQueue.h"

@interface NodeQueue ()

-(int) removeElementAt: (int)index;
-(int) getIndexOfMinimumDistNode: (NSMutableArray*) nodes;

@end

@implementation NodeQueue

-(id) initWithLength: (int) _length {
    self = [super init];
    if (!self) {
        return nil;
    }
    length = _length;
    idList = malloc(length * sizeof(int));
    for (int i=1; i<=length; i++){
        idList[i-1] = i;
    }
    return self;
}

-(int) getLength {
    return length;
}
-(int) removeElementAt: (int)index {
    int result = idList[index];
    length--;
    idList[index] = idList[length];
    return result;
}
-(int) getIndexOfMinimumDistNode: (NSMutableArray*) nodes {
    double minDist = INFINITY;
    int result = -1;
    for (int i=0; i<length; i++){
        int id = idList[i];
        Node* node = [nodes objectAtIndex: id];
        double dist = [node getDist];
        if (dist < minDist){
            minDist = dist;
            result = i;
        }
    }
    return result;
}
-(Node*) getMinDistNode: (NSMutableArray*) nodes {
    int indexOfMin = [self getIndexOfMinimumDistNode: nodes];
    if (indexOfMin == -1){
        NSLog(@"%@", @"data error: graph is not connected!");
        return nil;
    }
    int id = [self removeElementAt: indexOfMin];
    return [nodes objectAtIndex: id];
}

@end