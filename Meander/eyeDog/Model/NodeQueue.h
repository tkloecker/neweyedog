#import <Foundation/Foundation.h>
#import "Node.h"

@interface NodeQueue : NSObject {
    int* idList;
    int length;
}
-(id) initWithLength: (int) _length;
-(int) getLength;
-(Node*) getMinDistNode: (NSMutableArray*) nodes;
@end
