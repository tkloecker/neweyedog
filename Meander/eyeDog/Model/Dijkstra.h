/*
 reads csv files, runs dijkstra algorithm
 */

@class CategoryItem;

@interface Dijkstra : NSObject {
    
    int numNodes;
    NSMutableArray* nodes;
    NSMutableArray *cats;
    NSMutableDictionary* nodeNamesDict;
    NSMutableDictionary* textDict;
    
}

+ (Dijkstra*) sharedInstance;

- (void) Initialize;

- (void) initLocal;    // init using locally stored csv files (after making sure they are current)
- (void) ChangeLanguage: (NSString *) language;

- (int) getNumNodes;
- (Node*) getNodeFromId: (int) theId;
- (int*) getPathIdListFrom: (Node*) fromNode ToNode: (Node*) toNode;
- (int) getPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode;
- (double) getEuclidianPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode;
- (NSMutableArray*) getAngleListFrom: (Node*) fromNode ToNode: (Node*) toNode;

- (NSInteger) getCatCount;
- (CategoryItem *) getCategoryAtIndex: (NSInteger) nIndex;
- (NSMutableArray *) getCategories;
- (NSMutableDictionary*) getNodeNamesDict;
- (NSMutableDictionary*) getTextDict;

@end
