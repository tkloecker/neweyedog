//
//  SingletonClass.h
//  VisitorBook
//
//  Created by wony on 12/27/13.
//  Copyright 2013 wony. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletonClass : NSObject

+ (id)sharedInstance;

@end
