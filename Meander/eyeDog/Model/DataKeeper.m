//
//  DataKeeper.m
//  eyeDog
//
//  Created by wony on 12/27/13.
//  Copyright 2013 wony. All rights reserved.
//

#import "DataKeeper.h"
#import "Dijkstra.h"
#import "NodeListItem.h"

#import "UIColor+SchemeManager.h"

@implementation DataKeeper

@synthesize m_strLastErrorMessage;
@synthesize qrcodeDelegate;
@synthesize m_reader;

@synthesize m_strQRCode;
@synthesize m_strLanguage;

@synthesize m_bDontShowAgainHelp;

- (id)init
{
    self = [super init];
    if (self) {
        
        ////////////////////////////////////////////
        //// Initialize
        ////////////////////////////////////////////
        
        m_reader = [ZBarReaderViewController new];

        
        ////////////////////////////////////////////
    }
    
    return self;
}
- (void) Initialize {
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    [dijkstra Initialize];
}
- (void) Initialize2 {
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    _locations = [[NSMutableArray alloc] init];
    
    self.sources = [[NSMutableArray alloc] init];
    self.destinations = [[NSMutableArray alloc] init];
    
    int numNodes = [dijkstra getNumNodes];
    for (int i = 1; i <= numNodes; i++){
        Node* node = [self getNodeFromId: i];
        if ([node isJunction] || [node isDestination]){
            [self.sources addObject: node];
        }
        if ([node isDestination]){
            [self.destinations addObject: node];
        }
    }
    //[self testGetSources2];
    //[self testGetOrderedDestinations];
    NSLog(@"end of initialize2");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_END_INIT2 object:nil];

}
- (void) testGetSources {
    NSArray* s = [self getSources];
    for (int i=0; i<[s count]; i++){
        NodeListItem* source = [s objectAtIndex: i];
        [source logToConsole];
    }
}
- (void) testGetSources2 {
    NSArray* s = [self getSourcesForCategory: 5];
    for (int i=0; i<[s count]; i++){
        NodeListItem* source = [s objectAtIndex: i];
        [source logToConsole];
    }
}
- (void) testGetOrderedDestinations {
    NSArray* s = [self getDistanceOrderedDestinationListWithSourceNodeId: 6];
    for (int i=0; i<[s count]; i++){
        NodeListItem* source = [s objectAtIndex: i];
        [source logToConsole];
    }
}
- (void) testGetOrderedDestinations2 {
    NSArray* s = [self getDistanceOrderedDestinationListWithSourceNodeId: 6 ForCategory: 15];
    for (int i=0; i<[s count]; i++){
        NodeListItem* source = [s objectAtIndex: i];
        [source logToConsole];
    }
}
- (NSArray*) getSources {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    int numNodes = [dijkstra getNumNodes];
    for (int i = 1; i <= numNodes; i++){
        Node* node = [self getNodeFromId: i];
        if ([node isDestination]){
            NodeListItem* item = [[NodeListItem alloc] initWithNodeId: i];
            [result addObject: item];
        }
    }
    return [result sortedArrayUsingSelector: @selector(compareAlphabetically:)];
}
- (NSArray*) getAlphabeticallyOrderedDestinations {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    int numNodes = [dijkstra getNumNodes];
    for (int i = 1; i <= numNodes; i++){
        Node* node = [self getNodeFromId: i];
        if ([node isDestination]){
            NodeListItem* item = [[NodeListItem alloc] initWithNodeId: i];
            [result addObject: item];
        }
    }
    return [result sortedArrayUsingSelector: @selector(compareByPushFactorAndAlphabet:)];
}
- (NSArray*) getDistanceOrderedDestinationListWithSourceNodeId: (int) sourceNodeId {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    int numNodes = [dijkstra getNumNodes];
    Node* sourceNode = [self getNodeFromId: sourceNodeId];
    for (int i = 1; i <= numNodes; i++){
        Node* node = [self getNodeFromId: i];
        if ([node isDestination]){
            double distanceFromSource = [sourceNode getEuclidianPathLength: i];
            NodeListItem* item = [[NodeListItem alloc] initWithNodeId: i distance: distanceFromSource];
            [result addObject: item];
        }
    }
    return [result sortedArrayUsingSelector: @selector(compareByPushFactorAndDistance:)];
}


- (NSArray*) getSourcesForCategory: (int) cat {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    int numNodes = [dijkstra getNumNodes];
    for (int i = 1; i <= numNodes; i++){
        Node* node = [self getNodeFromId: i];
        if ([node isDestination] && [node getCategory] == cat){
            NodeListItem* item = [[NodeListItem alloc] initWithNodeId: i];
            [result addObject: item];
        }
    }
    return [result sortedArrayUsingSelector: @selector(compareAlphabetically:)];
}

- (NSArray*) getAlphabeticallyOrderedDestinationsForCategory: (int) cat {
    return [self getSourcesForCategory: cat];
}

- (NSArray*) getAlhpabeticallyOrderedDestinationListWithSourceNodeId: (int) sourceNodeId
                                                         ForCategory: (int) cat {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    int numNodes = [dijkstra getNumNodes];
    Node* sourceNode = [self getNodeFromId: sourceNodeId];
    for (int i = 1; i <= numNodes; i++){
        Node* node = [self getNodeFromId: i];
        if ([node isDestination] && [node getCategory] == cat){
            double distanceFromSource = [sourceNode getEuclidianPathLength: i];
            NodeListItem* item = [[NodeListItem alloc] initWithNodeId: i distance: distanceFromSource];
            [result addObject: item];
        }
    }
    return [result sortedArrayUsingSelector: @selector(compareByPushFactorAndAlphabet:)];
}

- (NSArray*) getDistanceOrderedDestinationListWithSourceNodeId: (int) sourceNodeId
                                                   ForCategory: (int) cat {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    int numNodes = [dijkstra getNumNodes];
    Node* sourceNode = [self getNodeFromId: sourceNodeId];
    for (int i = 1; i <= numNodes; i++){
        Node* node = [self getNodeFromId: i];
        if ([node isDestination] && [node getCategory] == cat){
            double distanceFromSource = [sourceNode getEuclidianPathLength: i];
            NodeListItem* item = [[NodeListItem alloc] initWithNodeId: i distance: distanceFromSource];
            [result addObject: item];
        }
    }
    return [result sortedArrayUsingSelector: @selector(compareByPushFactorAndDistance:)];
}

- (Node*) getNodeFromId: (int) theId {
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    return [dijkstra getNodeFromId: theId];
}

- (int*) getPathIdListFrom: (Node*) fromNode ToNode: (Node*) toNode{
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    return [dijkstra getPathIdListFrom: fromNode ToNode: toNode];
}

- (int) getPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode {
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    return [dijkstra getPathLengthFrom: fromNode ToNode: toNode];
}

- (double) getEuclidianPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode {
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    return [dijkstra getEuclidianPathLengthFrom: fromNode ToNode: toNode];
}

- (NSMutableArray*) getAngleListFrom: (Node*) fromNode ToNode: (Node*) toNode {
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    return [dijkstra getAngleListFrom: fromNode ToNode: toNode];
}

- (void) ResetPathFrom: (Node*) fromNode ToNode: (Node*) toNode {
    
    [_locations removeAllObjects];
    
    if (fromNode == toNode){
        return;
    }
    
    int pathLength = [self getPathLengthFrom: fromNode ToNode: toNode];
    int* path = [self getPathIdListFrom: fromNode ToNode: toNode];
    
    NSMutableArray* angles = [self getAngleListFrom: fromNode ToNode: toNode];
    NSString* pathString = [Util intArr2String: path length: pathLength];
    
    NSLog(@"%@", pathString);
    //positive angles are left turs, negative right turns
    
    for (int i = 0; i < pathLength; i++) {
        BOOL showTopView = NO;
        NSString* connectionText = @"";
        NSString* iconPath = @"";
        int next2nextId = -1;
        
        if (i < pathLength - 2) {
            next2nextId = path[i + 2];
            showTopView = YES;
        }
        
        int nodeId = path[i];
        Node* node = [self getNodeFromId: nodeId];
        double remainingDistance = [node getEuclidianPathLength: [toNode getId]];
        
        int numPics = -1;
        int nextNodeId = -1;
        NSMutableArray* pictures = nil;
        int levelChange = 0;
        
        if (i < pathLength - 1) {
            nextNodeId = path[i + 1];
            numPics = [node getNumPicturesToNodeId: nextNodeId];
            pictures = [node getPicturesToNodeId: nextNodeId];
            Connection* conn = [node getConnectionToNode: nextNodeId];
            Node* nextNode= [self getNodeFromId: nextNodeId];
            showTopView = [nextNode topViewVisible];
            if (conn == nil){
                NSLog(@"connection object is nil - ERROR - this shouldn't have happened!");
            } else {
                connectionText = [conn getText];
                iconPath = [conn getIconPath];
            }
        }
        
        NSLog(@"node id: %d, remaining distance: %7.3f, number of pictures: %d", nodeId, remainingDistance, numPics);
        
        NSString* dir;
        
        for (int j = 0; j < numPics; j++ ){
            NSString* connText = @"";
            NSString* icPath = @"";
            dir = [self getTextByID: 4];
            if (j == numPics - 1) {
                connText = connectionText;
                icPath = iconPath;
                // everything except the last picture between 2 nodes is always "straight", and the last picture between 2 nodes might be something else
                if ([[angles objectAtIndex: i] isEqual: [NSNull null]]){
                    
                    Node* nextNode = [self getNodeFromId: nextNodeId];
                    int i2 = i + 1;
                    int currentLevel = [node getLevel];
                    int targetLevel = [nextNode getLevel];
                    levelChange = targetLevel - currentLevel;
                    int previousLevel = targetLevel;
                    
                    while (true){
                        i2++;
                        if (i2 >= pathLength){
                            break;
                        }
                        Node* futureNode = [self getNodeFromId: path[i2]];
                        int futureLevel = [futureNode getLevel];
                        if (futureLevel == previousLevel){
                            break;
                        }
                        targetLevel = futureLevel;
                        levelChange = targetLevel - currentLevel;
                        
                    }
                    if (levelChange < 0) {
                        //dir = @"Go down";
                        dir = [[NSString alloc] initWithFormat: @"%@ %d", [self getTextByID: 60], targetLevel];
                        //dir = [[NSString alloc] initWithFormat: @"downTo %d", targetLevel];
                    } else {
                        //dir = @"Go up";
                        dir = [[NSString alloc] initWithFormat: @"%@ %d", [self getTextByID: 8], targetLevel];
                        //dir = [[NSString alloc] initWithFormat: @"upTo %d", targetLevel];
                    }
                } else {
                    double angle = [[angles objectAtIndex: i] doubleValue];
                    dir = [self angleString: angle]; // positive = left , negative = right
                }
            }
            Picture* pic = [pictures objectAtIndex: j];
            NSNumber *remainDistance = [NSNumber numberWithDouble: [node getRemainingDistanceTo: [toNode getId] withNextNodeId: nextNodeId pictureIndex: j]];
            NSNumber* lc = [NSNumber numberWithInt: levelChange];
            NSDictionary *dictItem = [NSDictionary dictionaryWithObjectsAndKeys:
                                      pic, @"Picture",
                                      remainDistance, @"Distance",
                                      dir, @"Direction",
                                      [angles objectAtIndex: i], @"Angle",
                                      lc, @"levelChange",
                                      [NSNumber numberWithInt: nodeId], @"previd",
                                      [NSNumber numberWithInt: nextNodeId], @"nodeid",
                                      [NSNumber numberWithInt: next2nextId], @"nextid",
                                      [NSNumber numberWithInt: j], @"curstep",
                                      [NSNumber numberWithInt: numPics], @"totalstep",
                                      connText, @"connectionText",
                                      icPath, @"iconPath",
                                      [NSNumber numberWithBool: showTopView], @"showTopView",
                                      nil];
            [_locations addObject: dictItem];
            //NSLog(@"picture %d remaining distance: %7.3f, direction: %@", j, , dir);
        }
    }
    
    [self ShowCurrentLocation];
}

- (void) ShowCurrentLocation {
    for (NSInteger nIndex = 0; nIndex < [_locations count]; nIndex++) {
        NSDictionary *dictItem = [_locations objectAtIndex: nIndex];
        
        NSLog(@"%@, %7.3f, %@", [[dictItem objectForKey:@"Picture"] getFileName], [[dictItem objectForKey:@"Distance"] doubleValue], [dictItem objectForKey: @"Direction"]);
    }
}

- (NSString*) angleString: (double)angle {
    if (angle > 125) {
        return [self getTextByID: 2];
    }
    if (angle > 55){
        return [self getTextByID: 3];
    }
    if (angle > 10){
        return [self getTextByID: 1];
    }
    if (angle < -125) {
        return [self getTextByID: 6];
    }
    if (angle < -55){
        return [self getTextByID: 7];
    }
    if (angle < -10){
        return [self getTextByID: 5];
    }
    return [self getTextByID: 4];
}

#pragma mark - Category

- (NSInteger) getCategoryCount {
    return [[Dijkstra sharedInstance] getCatCount];
}

- (CategoryItem *) getCategoryItemAtIndex: (NSInteger) nIndex {
    return [[Dijkstra sharedInstance] getCategoryAtIndex: nIndex];
}

- (NSMutableArray *) getCategories {
    return [[Dijkstra sharedInstance] getCategories];
}

#pragma mark - Language Text

- (NSString *) getTextByID: (NSInteger) nTextID {
    if (nTextID < 1) {
        return @"";
    }
    
    return [[[Dijkstra sharedInstance] getTextDict] objectForKey: [NSString stringWithFormat:@"%d", nTextID]];
}

- (void) ChangeLanguage: (NSString *) language {
    [[Dijkstra sharedInstance] ChangeLanguage: language];
}

#pragma mark - Local Management

// All datas will be written in local file
// Image List, User Name, User Password, User setting information

- (BOOL)saveDataToFile
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setObject:[NSNumber numberWithBool: m_bDontShowAgainHelp] forKey:@"DontShowAgainHelp"];
        
    BOOL bSuccess = [dic writeToFile:[self dataFilePath] atomically:YES];
    
    if (bSuccess) {
        NSLog(@"Write to file successfully. dic=%@", dic);
    } else {
        NSLog(@"Write to file failed");
    }
    
    return bSuccess;
}

- (BOOL)loadDataFromFile
{
    NSString *filePath = [self dataFilePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:filePath];
        
        NSNumber *bDontShow = [dic objectForKey:@"DontShowAgainHelp"];
        
        if (bDontShow == nil) {
            [self setM_bDontShowAgainHelp: NO];
        } else {
            [self setM_bDontShowAgainHelp: [bDontShow boolValue]];
        }
        
        return YES;
    }
    
    return NO;
}

- (NSString*)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"eyeDog.plist"];
}

#pragma mark - QRCode

- (void) onScanQRCode : (UIViewController *) viewCtrl {
    //ZBarReaderViewController *reader = [ZBarReaderViewController new];
    m_reader.readerDelegate = self;
    m_reader.showsHelpOnFail = NO;
    m_reader.showsZBarControls = NO;
    m_reader.wantsFullScreenLayout = NO;
  //  m_reader.extendedLayoutIncludesOpaqueBars = YES;
  //  m_reader.edgesForExtendedLayout = UIRectEdgeTop;
    
    ZBarImageScanner *scanner = m_reader.scanner;
    m_reader.readerView.torchMode = 0;
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
    [scanner setSymbology:ZBAR_QRCODE
                   config:ZBAR_CFG_ENABLE
                       to:1];
    
    
    
    // present and release the controller
    //[viewCtrl.navigationController pushViewController: m_reader animated: YES];
    
    //Add layers
    
    CGFloat height = 54;
    
    UIButton *button = (UIButton *)[m_reader.view viewWithTag: 101];
    if ([m_reader.view viewWithTag: 1000] == nil) {
        CGRect frame = [[UIScreen mainScreen] bounds];
        
        UIView *navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, height + 20)];
        navigationView.backgroundColor = [UIColor navigationBarColor];
        navigationView.tag = 1000;
        
        UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 20)];
        bar.backgroundColor = [UIColor blackColor];
        
        [navigationView addSubview:bar];
        
        [m_reader.view addSubview:navigationView];
    }
    
    if ([m_reader.view viewWithTag: 101] == nil) {
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 20 + (height - 44) / 2, 74, 44)];
        [button setBackgroundImage:[UIImage imageNamed:@"qrbackbtn"] forState: UIControlStateNormal];
        [button setContentEdgeInsets: UIEdgeInsetsMake(2, 10, 0, 0)];
        [button setTitle: [self getTextByID: 62] forState: UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState: UIControlStateNormal];
        [button setTag: 101];
        
        [button addTarget:qrcodeDelegate action:@selector(OnTouchBackBtn) forControlEvents:UIControlEventTouchUpInside];
       [m_reader.view addSubview: button];
        
        UIButton *helpbtn = [[UIButton alloc] initWithFrame:CGRectMake(279, 22 + (height - 40) / 2, 40, 40)];
        [helpbtn setImage:[UIImage imageNamed:@"helpicon"] forState: UIControlStateNormal];
        [helpbtn addTarget:qrcodeDelegate action:@selector(OnTouchHelpBtn) forControlEvents:UIControlEventTouchUpInside];
        [helpbtn setTag: 102];
        [m_reader.view addSubview: helpbtn];
    } else {
        [button setTitle: [self getTextByID: 62] forState: UIControlStateNormal];
        [button removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:qrcodeDelegate action:@selector(OnTouchBackBtn) forControlEvents:UIControlEventTouchUpInside];
        
        [(UIButton *)[m_reader.view viewWithTag: 102] removeTarget: nil action:NULL forControlEvents:UIControlEventTouchUpInside];
        [(UIButton *)[m_reader.view viewWithTag: 102] addTarget:qrcodeDelegate action:@selector(OnTouchHelpBtn) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([m_reader.view viewWithTag: 103] == nil) {

        UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0, 20, 320, height)];
        [label setText: [self getTextByID: 66]];
        [label setTextColor: [UIColor whiteColor]];
        [label setTextAlignment: NSTextAlignmentCenter];
        [label setTag: 103];
        [m_reader.view addSubview: label];
    } else {
        [(UILabel *)[m_reader.view viewWithTag: 103] setText: [self getTextByID: 66]];
    }
    
    [viewCtrl presentViewController: m_reader animated: YES completion: nil];

    //[viewCtrl.view addSubview:m_reader.readerView];
    //[m_reader release];
    
}

#pragma mark - QR Code Delegate

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    //resultText.text = symbol.data;
    [self setM_strQRCode: symbol.data];
    
    NSLog(@"QR Code = %@", m_strQRCode);
    //[self setM_strQRCode: @"http://snipstamp.fandura.com/qr/2_1000000002"];
    [reader dismissViewControllerAnimated:NO completion:^(void) {
        if (qrcodeDelegate != nil) {
            [qrcodeDelegate SuccessToReadQRCode];
        } else {
            [qrcodeDelegate FailedToReadQRCode];
        }
    }
     ];
    
   
    
    // EXAMPLE: do something useful with the barcode image
    //    resultImage.image =
    //    [info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    
}


@end
