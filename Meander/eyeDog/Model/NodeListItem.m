//
//  NodeListItem.m
//  Meander
//
//  Created by mack on 23.02.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "NodeListItem.h"
#import "Dijkstra.h"
#import "Node.h"

@implementation NodeListItem

- (id) initWithNodeId: (int) _nodeId {
    self = [super init]; if (!self){ return nil; }
    nodeId = _nodeId;
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    Node* node = [dijkstra getNodeFromId: nodeId];
    NSString* nodeName = [node getName];
    title = nodeName;
    distance = 0;
    return self;
}

- (id) initWithNodeId: (int) _nodeId
             distance: (double) _distance {
    self = [super init]; if (!self){ return nil; }
    nodeId = _nodeId;
    distance = _distance;
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    Node* node = [dijkstra getNodeFromId: nodeId];
    NSString* nodeName = [node getName];
    double seconds = distance / walkingSpeedInMetersPerSecond;
    title = nodeName;
    subtitle = [[NSString alloc] initWithFormat: @"(%dm / %@)", (int)round(distance), [Util formattedSecondsString: seconds]];
    return self;    
}


- (int) getNodeId {
    return nodeId;
}

- (NSString*) getTitle {
    return title;
}

- (NSString *) getSubTitle {
    return subtitle;
}

- (double) getDistance {
    return distance;
}

- (Node*) getNode {
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    Node* node = [dijkstra getNodeFromId: nodeId];
    return node;
}

- (int) getPushFactor {
    Node* node = [self getNode];
    return [node getPushFactor];
}

- (int) getCategory {
    Node* node = [self getNode];
    return [node getCategory];
}

- (BOOL) isFeatured {
    return ([self getPushFactor] != -1);
}

- (NSComparisonResult)compareAlphabetically:(NodeListItem*) that {
    NSString* otherTitle = [that getTitle];
    return [title compare: otherTitle];
}

- (NSComparisonResult)compareByDistance:(NodeListItem*) that {
    NSNumber* myDist = [[NSNumber alloc] initWithDouble: distance];
    NSNumber* otherDist = [[NSNumber alloc] initWithDouble: [that getDistance]];
    return [myDist compare: otherDist];
}

- (NSComparisonResult)compareByPushFactorAndDistance:(NodeListItem*) that {
    NSNumber* myDist = [[NSNumber alloc] initWithDouble: distance];
    NSNumber* otherDist = [[NSNumber alloc] initWithDouble: [that getDistance]];
    NSNumber* myPush = [[NSNumber alloc] initWithInt: -[self getPushFactor]];
    NSNumber* otherPush = [[NSNumber alloc] initWithInt: -[that getPushFactor]];
    NSComparisonResult pushCompResult = [myPush compare: otherPush];
    if ([myPush isEqual: otherPush]){
        return [myDist compare: otherDist];
    } else {
        return pushCompResult;
    }
}

- (NSComparisonResult)compareByPushFactorAndAlphabet:(NodeListItem*) that {
    NSString* myTitle = [self getTitle];
    NSString* otherTitle = [that getTitle];
    NSNumber* myPush = [[NSNumber alloc] initWithInt: -[self getPushFactor]];
    NSNumber* otherPush = [[NSNumber alloc] initWithInt: -[that getPushFactor]];
    NSComparisonResult pushCompResult = [myPush compare: otherPush];
    if ([myPush isEqual: otherPush]){
        return [myTitle compare: otherTitle];
    } else {
        return pushCompResult;
    }
}

- (void) logToConsole {
    NSLog(@"node: %d, title: %@, push: %d, cat: %d", nodeId, title, [self getPushFactor], [self getCategory]);
}

@end
