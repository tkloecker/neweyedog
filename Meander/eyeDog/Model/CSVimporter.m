#import "CSVimporter.h"
#import "Util.h"
#import "Server.h"
#import "Dijkstra.h"

@implementation CSVimporter

static CSVimporter* soleInstance = nil;

NSString* const lastUpdatedFileName = @"lastUpdateFromServerTimeStamp.txt";

- (id) init {
    self = [super init];
    if (!self){
        return nil;
    }
    return self;
}

- (void) Initialize {
    // http://stackoverflow.com/questions/9968050/how-to-disable-afnetworking-cache
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                            diskCapacity:0
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    csvResourcesTimeStamp = 1392446511;
    [self calcLocalLastUpdated];   // find out last updated of last d'loaded version
    [self checkServerLastUpdated]; // find out last updated of csv's on server - if that's newer, must first download from server, then initLocal, else immediately initLocal
}

+ (CSVimporter*) sharedInstance {
    if (soleInstance == nil){
        soleInstance = [[super alloc] init];
    }
    return soleInstance;
}

- (void) calcLocalLastUpdated{
    // lastUpdateFromServerTimeStamp.txt in documents folder has last mod date (time stamp) of the last csv's d'loaded from server
    // - unless no csv's were ever d'loaded from server (we just have the resources) - then csvResourcesTimeStamp is returned.
    NSLog(@"%@", @"calcLocalLastUpdated");
    if ([Util docsFileExists: lastUpdatedFileName]){
        localCsvLastMod = csvResourcesTimeStamp;
        NSString* fileContents = [Util stringFromDocsFile: lastUpdatedFileName];
        localCsvLastMod = [fileContents longLongValue];
        NSLog(@"time stamp of last server update: %lld", localCsvLastMod);
    } else {
        localCsvLastMod = csvResourcesTimeStamp;
        NSLog(@"never updated from server, timestamp of resources data: %lld", localCsvLastMod);
    }
}
- (void) checkServerLastUpdated {
    NSString* serverPath = [Server serverPath];
    NSLog(@"%@", @"checkServerLastUpdated");
    NSString *urlAsString = [Util concat2Strings: serverPath str2: @"lastUpdated.txt"];
    NSURLRequest* urlRequest = [Util requestFromString: urlAsString];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error) {
                               
                               if ([data length] > 0  && error == nil){
                                   NSString *fileContents = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   serverLastMod = [fileContents longLongValue];
                                   NSLog(@"server csv last updated: %lld", serverLastMod);
                                   if (localCsvLastMod >= serverLastMod){
                                       NSLog(@"%@", @"local csv's are current, using them");
                                       [[Dijkstra sharedInstance] initLocal];
                                   } else {
                                       NSLog(@"%@", @"local csv's outdated, getting csv's from server");
                                       [self loadCsvsFromServer];
                                   }
                               }
                               else if ([data length] == 0 && error == nil){
                                   NSLog(@"error server last updated file was empty.");
                                   [[Dijkstra sharedInstance] initLocal];
                               }
                               else if (error != nil){
                                   NSLog(@"Error downloading server last updated file %@", error);
                                   [[Dijkstra sharedInstance] initLocal];
                               }
                           }
     ];
}

- (void) loadCsvsFromServer {
    NSLog(@"load CSVs from server");
    NSString* serverPathCsv = [Server serverPathCSV];
    NSString* nodesUrlString = [Util concat2Strings: serverPathCsv str2: @"nodes.csv"];
    NSString* connectionsUrlString = [Util concat2Strings: serverPathCsv str2: @"connections.csv"];
    NSString* picturesUrlString = [Util concat2Strings: serverPathCsv str2: @"pictures.csv"];
    NSString* langDestsUrlString = [Util concat2Strings: serverPathCsv str2: @"destinations_languages.csv"];
    NSString* destGroupsUrlString = [Util concat2Strings: serverPathCsv str2: @"des_groups.csv"];
    NSString* uiTextUrlString = [Util concat2Strings: serverPathCsv str2: @"text_languages.csv"];
    loadingNodesFromServerDone = NO;
    loadingConnectionsFromServerDone = NO;
    loadingPicturesFromServerDone = NO;
    loadingLangDestsFromServerDone = NO;
    loadingDestGroupsFromServerDone = NO;
    loadingTextFromServerDone = NO;
    errorOccuredTryingToLoadCsvsFromServer = NO;
    csvFromServerFetchedHandlerCalled = NO;
    //NSString* path0 = [[NSBundle mainBundle] pathForResource: @"pictures" ofType:@"csv"];
    //NSString* csvPath = [path0 stringByDeletingLastPathComponent]; NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    NSLog(@"%@", nodesUrlString);
    NSLog(@"%@", connectionsUrlString);
    NSLog(@"%@", picturesUrlString);
    NSLog(@"%@", langDestsUrlString);
    NSLog(@"%@", destGroupsUrlString);
    NSLog(@"%@", uiTextUrlString);
    NSURLRequest* nodesRequest = [Util requestFromString: nodesUrlString];
    NSOperationQueue* nodesQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:nodesRequest queue: nodesQueue
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error) {
                               
                               if ([data length] > 0  && error == nil){
                                   //NSString* fileContents = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   //NSLog(@"nodes:\n%@", fileContents);
                                   //NSString* serverNodesLocalTempPath = [csvPath stringByAppendingPathComponent: @"serverNodes.csv"];
                                   NSString* serverNodesLocalTempPath = [Util docsFilePath: @"serverNodes.csv"];
                                   NSLog(@"serverNodesLocalTempPath: %@", serverNodesLocalTempPath);
                                   [data writeToFile: serverNodesLocalTempPath atomically: YES];
                                   loadingNodesFromServerDone = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if ([data length] == 0 && error == nil){
                                   NSLog(@"error: server nodes file was empty.");
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if (error != nil){
                                   NSLog(@"Error downloading server nodes file %@", error);
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                           }
     ];
    
    NSURLRequest* connectionsRequest = [Util requestFromString: connectionsUrlString];
    NSOperationQueue* connectionsQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest: connectionsRequest queue: connectionsQueue
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error) {
                               
                               if ([data length] > 0  && error == nil){
                                   //NSString* serverConnectionsLocalTempPath = [csvPath stringByAppendingPathComponent: @"serverConnections.csv"];
                                   NSString* serverConnectionsLocalTempPath = [Util docsFilePath: @"serverConnections.csv"];
                                   //NSLog(@"%@", serverConnectionsLocalTempPath);
                                   [data writeToFile: serverConnectionsLocalTempPath atomically: YES];
                                   loadingConnectionsFromServerDone = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if ([data length] == 0 && error == nil){
                                   NSLog(@"error: server connections file was empty.");
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if (error != nil){
                                   NSLog(@"Error downloading server connections file %@", error);
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                           }
     ];
    
    NSURLRequest* picsRequest = [Util requestFromString: picturesUrlString];
    NSOperationQueue* picsQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest: picsRequest queue: picsQueue
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error) {
                               
                               if ([data length] > 0  && error == nil){
                                   //NSString* serverPicturesLocalTempPath = [csvPath stringByAppendingPathComponent: @"serverPictures.csv"];
                                   NSString* serverPicturesLocalTempPath = [Util docsFilePath: @"serverPictures.csv"];
                                   //NSLog(@"%@", serverPicturesLocalTempPath);
                                   [data writeToFile: serverPicturesLocalTempPath atomically: YES];
                                   loadingPicturesFromServerDone = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if ([data length] == 0 && error == nil){
                                   NSLog(@"error: server pictures csv file was empty.");
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if (error != nil){
                                   NSLog(@"Error downloading server pictures csv file %@", error);
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                           }
     ];

    NSURLRequest* langDestsRequest = [Util requestFromString: langDestsUrlString];
    NSOperationQueue* langDestsQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest: langDestsRequest queue: langDestsQueue
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error) {
                               
                               if ([data length] > 0  && error == nil){
                                   NSString* langDestsLocalTempPath = [Util docsFilePath: @"langDests.csv"];
                                   [data writeToFile: langDestsLocalTempPath atomically: YES];
                                   loadingLangDestsFromServerDone = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if ([data length] == 0 && error == nil){
                                   NSLog(@"error: lang dests csv file was empty.");
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if (error != nil){
                                   NSLog(@"Error downloading lang dests csv file %@", error);
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                           }
     ];
    
    NSURLRequest* destGroupsRequest = [Util requestFromString: destGroupsUrlString];
    NSOperationQueue* destGroupsQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest: destGroupsRequest queue: destGroupsQueue
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error) {
                               
                               if ([data length] > 0  && error == nil){
                                   NSString* destGroupsLocalTempPath = [Util docsFilePath: @"serverDestGroups.csv"];
                                   [data writeToFile: destGroupsLocalTempPath atomically: YES];
                                   loadingDestGroupsFromServerDone = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if ([data length] == 0 && error == nil){
                                   NSLog(@"error: server dest groups csv file was empty.");
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if (error != nil){
                                   NSLog(@"Error downloading destination groups csv file %@", error);
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                           }
     ];
    
    NSURLRequest* uiTextRequest = [Util requestFromString: uiTextUrlString];
    NSOperationQueue* uiTextQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest: uiTextRequest queue: uiTextQueue
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error) {
                               
                               if ([data length] > 0  && error == nil){
                                   NSString* uiTextLocalTempPath = [Util docsFilePath: @"serverUiText.csv"];
                                   [data writeToFile: uiTextLocalTempPath atomically: YES];
                                   loadingTextFromServerDone = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if ([data length] == 0 && error == nil){
                                   NSLog(@"error: server ui text csv file was empty.");
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                               else if (error != nil){
                                   NSLog(@"Error downloading ui text csv file %@", error);
                                   errorOccuredTryingToLoadCsvsFromServer = YES;
                                   if ([self readyToInit]){ [self csvFromServerFetchedHandler]; }
                               }
                           }
     ];
}

- (BOOL) readyToInit{
    // returns true, if csv data from server has arrived, and init has not been called yet.
    if (csvFromServerFetchedHandlerCalled){
        return NO; // already called
    }
    BOOL everythingLoaded = loadingNodesFromServerDone && loadingConnectionsFromServerDone && loadingPicturesFromServerDone
        && loadingLangDestsFromServerDone && loadingDestGroupsFromServerDone && loadingTextFromServerDone;
    BOOL ready = everythingLoaded || errorOccuredTryingToLoadCsvsFromServer;
    csvFromServerFetchedHandlerCalled = ready;
    return ready;
}

- (void) csvFromServerFetchedHandler {
    if (errorOccuredTryingToLoadCsvsFromServer){
        NSLog(@"%@", @"error fetching csv.");
        // just use the local version then
        [[Dijkstra sharedInstance] initLocal];
    } else {
        NSLog(@"%@", @" ========== fetched CSVs from server. ===============");
        [Util noICloudCopyFileWithSource: [Util docsFilePath: @"serverNodes.csv"] target: [Util docsFilePath: @"nodes.csv"]];
        [Util noICloudCopyFileWithSource: [Util docsFilePath: @"serverConnections.csv"] target: [Util docsFilePath: @"connections.csv"]];
        [Util noICloudCopyFileWithSource: [Util docsFilePath: @"serverPictures.csv"] target: [Util docsFilePath: @"pictures.csv"]];
        [Util noICloudCopyFileWithSource: [Util docsFilePath: @"langDests.csv"] target: [Util docsFilePath: @"destinations_languages.csv"]];
        [Util noICloudCopyFileWithSource: [Util docsFilePath: @"serverDestGroups.csv"] target: [Util docsFilePath: @"des_groups.csv"]];
        [Util noICloudCopyFileWithSource: [Util docsFilePath: @"serverUiText.csv"] target: [Util docsFilePath: @"text_languages.csv"]];
        // delete temp files
        NSFileManager* dfm = [NSFileManager defaultManager];
        NSError* error;
        [dfm removeItemAtPath: [Util docsFilePath: @"serverNodes.csv"] error: &error];
        [dfm removeItemAtPath: [Util docsFilePath: @"serverConnections.csv"] error: &error];
        [dfm removeItemAtPath: [Util docsFilePath: @"serverPictures.csv"] error: &error];
        [dfm removeItemAtPath: [Util docsFilePath: @"langDests.csv"] error: &error];
        [dfm removeItemAtPath: [Util docsFilePath: @"serverDestGroups.csv"] error: &error];
        [dfm removeItemAtPath: [Util docsFilePath: @"serverUiText.csv"] error: &error];
        [Util writeStringToDocsFile: [[NSString alloc] initWithFormat: @"%lld", serverLastMod] fileName:lastUpdatedFileName];
        [[Dijkstra sharedInstance] initLocal];
    }
}

@end
