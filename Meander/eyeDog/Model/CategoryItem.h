//
//  Category.h
//  Meander
//
//  Created by Wony on 2/26/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryItem : NSObject

@property (nonatomic) NSInteger nID;
@property (nonatomic) NSInteger nLanguageID;
@property (nonatomic, strong) NSString *iconID;
@property (nonatomic, strong) NSDictionary *name;

- (NSString *) getTitle;

@end
