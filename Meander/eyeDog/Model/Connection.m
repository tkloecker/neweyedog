//
//  Connection.m
//  eyeDog
//
//  Created by mack on 22.04.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "Connection.h"

@implementation Connection

-(id) initWithFrom: (int) _from
                To: (int) _to
              Text: (NSString*) _text
             Delay: (double) _delay
             Width: (double) _width
          IconPath: (NSString*) _iconPath {
    self = [super init];
    if (!self) {
        return nil;
    }
    from = _from;
    to = _to;
    text = _text;
    delay = _delay;
    width = _width;
    iconPath = _iconPath;
    return self;
}

-(int)getFrom {
    return from;
}
-(int)getTo {
    return to;
}
-(NSString*) getText {
    return text;
}
-(double) getDelay {
    return delay;
}
-(double) getWidth {
    return width;
}
-(NSString*) getIconPath {
    return iconPath;
}


@end
