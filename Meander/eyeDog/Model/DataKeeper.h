//
//  DataKeeper.h
//  Meander
//
//  Created by wony on 12/27/13.
//  Copyright 2013 wony. All rights reserved.
//  

#import "SingletonClass.h"
#import "ZBarSDK.h"

@protocol QRCodeReaderDelegate
@optional
- (void) SuccessToReadQRCode;
- (void) FailedToReadQRCode;
- (void) OnTouchBackBtn;
- (void) OnTouchHelpBtn;
@end

#define walkingSpeedInMetersPerSecond 1.0f

@interface DataKeeper : SingletonClass <ZBarReaderDelegate> {
    
    NSString    *m_strLastErrorMessage;
    
    // QR Code
    NSString *m_strQRCode;
}

@property (nonatomic, assign) id <QRCodeReaderDelegate> qrcodeDelegate;

@property (nonatomic, retain) NSString *m_strQRCode;
@property (nonatomic, retain) NSString    *m_strLastErrorMessage;
@property (nonatomic, retain) NSString    *m_strLanguage;

@property (strong, nonatomic)     Node* source;
@property (strong, nonatomic)     Node* destination;

@property (strong, nonatomic) NSMutableArray *sources;
@property (strong, nonatomic) NSMutableArray *destinations;

@property (strong, nonatomic) NSMutableArray *locations;

@property (nonatomic, retain) ZBarReaderViewController *m_reader;

// Setting

@property (nonatomic) BOOL m_bDontShowAgainHelp;


- (void) Initialize;
- (void) Initialize2;

- (void) ResetPathFrom: (Node*) fromNode ToNode: (Node*) toNode;

- (Node*) getNodeFromId: (int) id;
- (int*) getPathIdListFrom: (Node*) fromNode ToNode: (Node*) toNode;
- (int) getPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode;
- (double) getEuclidianPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode;
- (NSMutableArray*) getAngleListFrom: (Node*) fromNode ToNode: (Node*) toNode;

- (NSArray*) getSources;
- (NSArray*) getAlphabeticallyOrderedDestinations;
- (NSArray*) getAlhpabeticallyOrderedDestinationListWithSourceNodeId: (int) sourceNodeId ForCategory: (int) cat;
- (NSArray*) getDistanceOrderedDestinationListWithSourceNodeId: (int) sourceNodeId;

- (NSArray*) getSourcesForCategory: (int) cat;
- (NSArray*) getAlphabeticallyOrderedDestinationsForCategory: (int) cat;
- (NSArray*) getDistanceOrderedDestinationListWithSourceNodeId: (int) sourceNodeId
                                                   ForCategory: (int) cat;

- (NSInteger) getCategoryCount;
- (CategoryItem *) getCategoryItemAtIndex: (NSInteger) nIndex;
- (NSMutableArray *) getCategories;
- (NSString*) angleString: (double)angle;

- (NSString *) getTextByID: (NSInteger) nTextID;
- (void) ChangeLanguage: (NSString *) language;

- (void) onScanQRCode : (UIViewController *) viewCtrl;

- (BOOL)saveDataToFile;
- (BOOL)loadDataFromFile;
- (NSString*)dataFilePath;

@end
