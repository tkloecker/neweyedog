
#import "ServerPicture.h"

@implementation ServerPicture

- (id) initWithFrom:(int) _from
                to:(int) _to
             index:(int) _index
              path:(NSString*) _path
         timeStamp:(long long) _timeStamp {
    self = [super init]; if (!self){ return nil; }
    from = _from;
    to = _to;
    index = _index;
    path = _path;
    timeStamp = _timeStamp;
    return self;
}

- (int) getFrom {
    return from;
}

- (int) getTo {
    return to;
}

- (int) getIndex {
    return index;
}

- (NSString*) getPath {
    return path;
}

- (long long) getTimeStamp {
    return timeStamp;
}

- (NSComparisonResult)compare:(ServerPicture*) that {
    int myIdx = [self getIndex];
    int otherIdx = [that getIndex];
    if (myIdx > otherIdx){
        return 1;
    }
    if (myIdx < otherIdx){
        return -1;
    }
    return 0;
}

@end
