
#import <Foundation/Foundation.h>

@interface Util : NSObject

+(BOOL)containsOnlyNumberDigits:(NSString*) string;
+(BOOL)containsOnlyNumberDigitsAndMinus:(NSString*) string;
+(NSString*) intArr2String:(int*) arr
                    length:(int) length;
+(NSString*) doubleArr2String:(double*) arr
                       length:(int) length;
+(NSDate*) dateFromTimeStamp: (long long) timeStamp;

+(NSDate*) lastModifiedDateOfCsvfile: (NSString*) name;
+(long long) lastModifiedTimeStampOfCsvfile: (NSString*) name;
+(long long) docFileLastModifiedTimeStamp: (NSString*) fileName;
+(long long) docFileCreatedTimeStamp: (NSString*) fileName;
+(long long) localCsvLastModTimStamp: (NSString*) fileName;
+(void) sizeTest;
+(NSString*) concat2Strings: (NSString*) str1
                       str2:(NSString*) str2;
+(NSString*) concat3Strings: (NSString*) str1
                       str2:(NSString*) str2
                       str3:(NSString*) str3;
+(NSString*) concat4Strings: (NSString*) str1
                       str2:(NSString*) str2
                       str3:(NSString*) str3
                       str4:(NSString*) str4;
+(NSURLRequest*) requestFromString: (NSString*) urlString;
+(void) copyFileWithSource: (NSString*)source
                    target: (NSString*)target;
+(BOOL) noICloudCopyFileWithSource: (NSString*)source
                            target: (NSString*)target;
+(void) writeFileContentsToLog: (NSString*) path;

+(NSString*) docsFilePath: (NSString*) fileName;
+(void) writeStringToDocsFile : (NSString*) contents
                      fileName: (NSString*) fileName;
+(BOOL) writeStringToDocsFileNoICloudBackup : (NSString*) contents
                                    fileName: (NSString*) fileName;
+(NSString*) stringFromDocsFile: (NSString*) fileName;

+(BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)fileURL;

+(NSString*) getLocalCsv: (NSString*) fileName;
+(BOOL) docsFileExists: (NSString*) fileName;
+(void) setLastModDateOfFile: (NSString*) path
                 toTimeStamp: (long long) timeStamp;
+(long long) lastModifiedTimeStampOfFile:(NSString*) path;
+(NSString*) formattedSecondsString: (double) seconds;

+(CGPoint) intersectionWithX0 :(double) x0
                            y0: (double) y0
                            x1: (double) x1
                            y1: (double) y1
                            x2: (double) x2
                            y2: (double) y2
                            x3: (double) x3
                            y3: (double) y3;

+(BOOL) linearIndependentWithX0 :(double) x0
                              y0: (double) y0
                              x1: (double) x1
                              y1: (double) y1
                              x2: (double) x2
                              y2: (double) y2
                              x3: (double) x3
                              y3: (double) y3;

@end
