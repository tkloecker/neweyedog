
#import "Picture.h"

@implementation Picture

- (id) initWithFrom:(int) _from
                to:(int) _to
             index:(int) _index
              path:(NSString*) _path
         timeStamp:(long long) _timeStamp
        isResource:(BOOL) _isResource {
    self = [super init]; if (!self){ return nil; }
    from = _from;
    to = _to;
    index = _index;
    path = _path;
    timeStamp = _timeStamp;
    isResource = _isResource;
    serverPath = @"";
    serverTimeStamp = 0;
    isHTTP = NO;
    return self;
}

- (id) initWithServerPicture: (ServerPicture*) serverPic {
    self = [super init]; if (!self){ return nil; }
    from = [serverPic getFrom];
    to = [serverPic getTo];
    index = [serverPic getIndex];
    path = @"";
    timeStamp = 0;
    serverPath = [serverPic getPath];
    serverTimeStamp = [serverPic getTimeStamp];
    isResource = NO;
    isHTTP = YES;
    return self;
}
-(void) getFromServerAndStore {
    if (!isHTTP){
        return;
    }
    NSURLRequest* req = [Util requestFromString: serverPath];
    NSOperationQueue* q = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest: req queue: q
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error) {
                               
                               if ([data length] > 0  && error == nil){
                                   //UIImage* image = [UIImage imageWithData: data];
                                   NSString* pictureFileExtension = [serverPath pathExtension];
                                   NSString* fileName = [[NSString alloc] initWithFormat: @"%d_%d_%d.%@", from, to, index, pictureFileExtension];
                                   NSString* newPath = [Util docsFilePath: fileName];
                                   [data writeToFile: newPath atomically: YES];
                                   timeStamp = serverTimeStamp;
                                   isHTTP = NO;
                                   path = newPath;
                               }
                           }
     ];
}
- (int) getFrom {
    return from;
}

- (int) getTo {
    return to;
}

- (int) getIndex {
    return index;
}

- (NSString*) getLocalPath {
    return path;
}

- (NSString*) getServerPath {
    return serverPath;
}

- (NSString*) getPath {
    if (timeStamp >= serverTimeStamp){
        return path;
    } else {
        return serverPath;
    }
}

- (NSString*) getFileName {
    return [[self getPath] lastPathComponent];
}

- (long long) getTimeStamp {
    return timeStamp;
}

- (BOOL) picIsResource {
    return isResource;
}

- (BOOL) picIsHTTP {
    return isHTTP;
}

- (void) attachServerPicture: (ServerPicture*) serverPic {
    serverPath = [serverPic getPath];
    serverTimeStamp = [serverPic getTimeStamp];
    if (timeStamp >= serverTimeStamp){
        isHTTP = NO;
    } else {
        isHTTP = YES;
        isResource = NO;
    }
}

- (void) logToConsole {
    NSLog(@"from: %d, to: %d, index: %d, timeStamp: %lld, serverTS: %lld, path: %@, server path: %@, effective path: %@, resource: %d, http: %d", from, to, index, timeStamp, serverTimeStamp, path, serverPath, [self getPath], isResource, isHTTP);
}

- (void) logToConsoleShort {
    NSLog(@"from: %d, to: %d, index: %d, timeStamp: %lld, serverTS: %lld, path: %@, resource: %d, http: %d", from, to, index, timeStamp, serverTimeStamp, [self getPath], isResource, isHTTP);
}

@end
