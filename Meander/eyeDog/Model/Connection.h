//
//  Connection.h
//  eyeDog
//
//  Created by mack on 22.04.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Connection : NSObject {
    
    int from;
    int to;
    NSString* text;
    double delay;
    double width;
    NSString* iconPath;
}

-(id) initWithFrom: (int) _from
                To: (int) _to
              Text: (NSString*) _text
             Delay: (double) _delay
             Width: (double) _width
          IconPath: (NSString*) _iconPath;

-(int)getFrom;
-(int)getTo;
-(NSString*) getText;
-(double) getDelay;
-(double) getWidth;
-(NSString*) getIconPath;

@end
