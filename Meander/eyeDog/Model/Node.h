
#import <Foundation/Foundation.h>
#import "Picture.h"
#import "ServerPicture.h"
#import "Connection.h"

@interface Node : NSObject {
    
    int theId;
    int level;
    int x;
    int y;
    NSString* type;
    NSString* nameId;
    NSString* name;
    NSString* zone;
    
    int category;
    int pushFactor;
    
    BOOL showTopView;
    
    int numNeighbours;
    int* neighbours;
    
    int* neighbInd;
    double* distances;
    NSMutableArray* pictures;
    NSMutableArray* serverPictures;
    NSMutableArray* connections;
    
    double dist;
    double* euclidianPathLengths;
    int prev;
    
    int* pathLengths;
    int** paths;
    
}

-(id) initWithId:(int) _theId
           level:(int) _level
               x:(int) _x
               y:(int) _y
            type:(NSString*) _type
          nameId:(NSString*) _nameId
            zone:(NSString*) _zone
        category:(int) _category
      pushFactor:(int) _pushFactor
     showTopView:(BOOL) _showTopView;
-(BOOL) topViewVisible;
-(void)incNumNeighbours;
-(void)setNumNeighboursToZero;
-(void)addConnection: (Connection*) conn;
-(Connection*) getConnectionToNode: (int)targetNodeId;
-(NSMutableArray*) getConnections;
-(int) getId;
-(NSString*) getIdAndName;
-(int) getLevel;
-(int) getX;
-(int) getY;
-(NSString*) getType;
-(NSString*) getNameId;
-(NSString*) getName;
-(NSString*) getZone;
-(int) getCategory;
-(int) getPushFactor;
-(double) getDist;
-(int) getPrev;
-(int) getNumNeighbours;
-(int*) getNeighbours;
-(int*) getNeighbInd;
-(double) getDistanceToNeighbour :(int) neighbourIndex;
-(int)getPathLength: (int)index;
-(int*)getPath: (int)index;
-(int*)getPathLengths;

-(void) allocWithNumNeighbours:(int) numNeighbours
                      numNodes:(int) numNodes;

-(void) logToConsole;
-(void) logToConsole2;
-(void) logToConsole3;

-(void) setDist: (double) _dist;
-(void) setDistInfinite;
-(void) setPrev: (int) _prev;
-(void) setPathLengths: (int*) _pathLengths;
-(void) setPaths: (int**) _paths;

-(void) calcDistances: (NSMutableArray*) nodes;

-(void) setEuclidianPathLengths: (double*) _euclidianPathLengths;
-(double)getEuclidianPathLength: (int)index;
-(double) euclidianDistanceTo: (Node*) otherNode;

-(BOOL) isJunction;
-(BOOL) isDestination;

-(double) getRemainingDistanceTo: (int) destinationNodeId
                  withNextNodeId: (int) nextNodeId
                    pictureIndex: (int) pictureIndex;
-(void) retrieveNameFromNameIdWithDictinary: (NSMutableDictionary*) dict;

//  pictures
-(void)setPictures: (NSMutableArray*) pics;
-(void) addServerPicture: (ServerPicture*) pic;
-(void) sortServerPictures;
-(void) attachServerPictures;
-(void) logPicsToConsole;
-(void) logNonLocalPicsToConsole;
-(void) logPicsToConsoleShort;
-(NSMutableArray*) getPicturesToNodeId: (int)nodeId;
-(int) getNumPicturesToNodeId: (int)nodeId;
- (void) loadFromServerAndStoreInDocumentsIfNecessary;

extern const unsigned long ID_POSITION;
extern const unsigned long LEVEL_POSITION;
extern const unsigned long X_POSITION;
extern const unsigned long Y_POSITION;
extern const unsigned long TYPE_POSITION;
extern const unsigned long NAME_POSITION;
extern const unsigned long ZONE_POSITION;
extern const unsigned long NUM_NODE_ENTRIES;
extern const unsigned long CATEGORY_POSITION;
extern const unsigned long PUSH_FACTOR_POSITION;
extern const unsigned long SHOW_TOP_VIEW_POSITION;
@end
