
#import <Foundation/Foundation.h>

@interface ServerPicture : NSObject {
    
    int from;
    int to;
    int index;
    NSString* path;
    long long timeStamp;
    
}

- (id) initWithFrom:(int) _from
                to:(int) _to
             index:(int) _index
              path:(NSString*) _path
         timeStamp:(long long) _timeStamp;

- (int) getFrom;
- (int) getTo;
- (int) getIndex;
- (NSString*) getPath;
- (long long) getTimeStamp;
- (NSComparisonResult)compare:(ServerPicture*) that;

@end
