//
//  NodeListItem.h
//  Meander
//
//  Created by mack on 23.02.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NodeListItem : NSObject {
    
    NSString* title;
    NSString* subtitle;
    int nodeId;
    double distance;
    
}

- (id) initWithNodeId: (int) _nodeId;

- (id) initWithNodeId: (int) _nodeId
             distance: (double) _distance;

- (int) getNodeId;

- (Node*) getNode;
- (int) getPushFactor;
- (int) getCategory;
- (BOOL) isFeatured;

- (NSString*) getTitle;
- (NSString *) getSubTitle;

- (double) getDistance;

- (NSComparisonResult)compareAlphabetically:(NodeListItem*) that;

- (NSComparisonResult)compareByDistance:(NodeListItem*) that;

- (NSComparisonResult)compareByPushFactorAndDistance:(NodeListItem*) that;
- (NSComparisonResult)compareByPushFactorAndAlphabet:(NodeListItem*) that;

-(void) logToConsole;

@end
