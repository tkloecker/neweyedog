
#import "Util.h"
#include <sys/xattr.h>

@implementation Util

+(BOOL)containsOnlyNumberDigits:(NSString*) string {
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
        return YES;
    } else {
        return NO;
    }
}
+(BOOL)containsOnlyNumberDigitsAndMinus:(NSString*) string{
    //return [string isMatchedByRegex:@"^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$"];
    NSPredicate *pred = [NSPredicate predicateWithFormat: @"SELF MATCHES%@", @"^-?\\d+$"];
    return [pred evaluateWithObject: string];
}
+(NSString*) intArr2String:(int*) arr
                    length:(int) length{
    if (length == 0){
        return @"empty";
    }
    NSMutableString* result = [[NSMutableString alloc] initWithFormat: @"%d", arr[0]];
    for (int i=1; i<length; i++){
        [result appendFormat: @",%d", arr[i]];
    }
    return result;
}
+(NSString*) doubleArr2String:(double*) arr
                       length:(int) length{
    if (length == 0){
        return @"empty";
    }
    NSMutableString* result = [[NSMutableString alloc] initWithFormat: @"%10.5f", arr[0]];
    for (int i=1; i<length; i++){
        [result appendFormat: @",%10.5f", arr[i]];
    }
    return result;
}
+(NSDate*) dateFromTimeStamp: (long long) timeStamp {
    NSTimeInterval _interval = timeStamp;
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:_interval];
    return date;
}
+(NSDate*) lastModifiedDateOfCsvfile: (NSString*) name{
    NSString* path = [[NSBundle mainBundle] pathForResource: name ofType:@"csv"];
    NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
    NSDate *result = [fileAttribs fileModificationDate]; //fileCreationDate
    return result;
}
+(long long) lastModifiedTimeStampOfCsvfile: (NSString*) name {
    NSString* path = [[NSBundle mainBundle] pathForResource: name ofType:@"csv"];
    NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
    NSDate *result = [fileAttribs fileModificationDate]; //fileCreationDate
    return (long long)[result timeIntervalSince1970];
}
+(long long) docFileLastModifiedTimeStamp: (NSString*) fileName {
    NSError* error;
    NSFileManager* dfm = [NSFileManager defaultManager];
    NSString* path = [self docsFilePath: fileName];
    if ([dfm fileExistsAtPath: path]){
        NSDictionary* fileAttribs = [dfm attributesOfItemAtPath: path error: &error];
        NSDate *result = [fileAttribs fileModificationDate];
        return (long long)[result timeIntervalSince1970];
    } else {
        return 0;
    }
}
+(long long) lastModifiedTimeStampOfFile:(NSString*) path { // use only if you know that file exists
    NSError* error;
    NSFileManager* dfm = [NSFileManager defaultManager];
    NSDictionary* fileAttribs = [dfm attributesOfItemAtPath: path error: &error];
    NSDate* result = [fileAttribs fileModificationDate];
    return (long long)[result timeIntervalSince1970];
}
+(long long) docFileCreatedTimeStamp: (NSString*) fileName {
    NSError* error;
    NSFileManager* dfm = [NSFileManager defaultManager];
    NSString* path = [self docsFilePath: fileName];
    if ([dfm fileExistsAtPath: path]){
        NSDictionary* fileAttribs = [dfm attributesOfItemAtPath: path error: &error];
        NSDate *result = [fileAttribs fileCreationDate];
        return (long long)[result timeIntervalSince1970];
    } else {
        return 0;
    }
}
+(long long) localCsvLastModTimStamp: (NSString*) fileName {
    long long resourceLM = [self lastModifiedTimeStampOfCsvfile: fileName];
    long long docLM = [self docFileLastModifiedTimeStamp: [fileName stringByAppendingString: @".csv"]];
    return MAX(resourceLM, docLM);
}
+(NSString*) getLocalCsv: (NSString*) fileName {
    NSString* fileNameCsv = [fileName stringByAppendingString: @".csv"];
    NSString* path1 = [[NSBundle mainBundle] pathForResource: fileName ofType:@"csv"];
    NSString* path2 = [self docsFilePath: fileNameCsv];
    long long lm1 = [self lastModifiedTimeStampOfCsvfile: fileName];
    long long lm2 = [self docFileLastModifiedTimeStamp: fileNameCsv];
    NSString* path;
    if (lm1 >= lm2){
        path = path1;
    } else {
        path = path2;
    }
    NSError* error;
    NSString* result = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    return result;
}
+(void) sizeTest{
    NSLog(@"Primitive sizes:");
    NSLog(@"The size of a char is: %lu.", sizeof(char));
    NSLog(@"The size of short is: %lu.", sizeof(short));
    NSLog(@"The size of int is: %lu.", sizeof(int));
    NSLog(@"The size of long is: %lu.", sizeof(long));
    NSLog(@"The size of long long is: %lu.", sizeof(long long));
    NSLog(@"The size of a unsigned char is: %lu.", sizeof(unsigned char));
    NSLog(@"The size of unsigned short is: %lu.", sizeof(unsigned short));
    NSLog(@"The size of unsigned int is: %lu.", sizeof(unsigned int));
    NSLog(@"The size of unsigned long is: %lu.", sizeof(unsigned long));
    NSLog(@"The size of unsigned long long is: %lu.", sizeof(unsigned long long));
    NSLog(@"The size of a float is: %lu.", sizeof(float));
    NSLog(@"The size of a double is %lu.", sizeof(double));
    
    NSLog(@"Ranges:");
    NSLog(@"CHAR_MIN:   %c",   CHAR_MIN);
    NSLog(@"CHAR_MAX:   %c",   CHAR_MAX);
    NSLog(@"SHRT_MIN:   %i",  SHRT_MIN);    // signed short int
    NSLog(@"SHRT_MAX:   %i",  SHRT_MAX);
    NSLog(@"INT_MIN:    %i",   INT_MIN);
    NSLog(@"INT_MAX:    %i",   INT_MAX);
    NSLog(@"LONG_MIN:   %li",  LONG_MIN);    // signed long int
    NSLog(@"LONG_MAX:   %li",  LONG_MAX);
    NSLog(@"ULONG_MAX:  %lu",  ULONG_MAX);   // unsigned long int
    NSLog(@"LLONG_MIN:  %lli", LLONG_MIN);   // signed long long int
    NSLog(@"LLONG_MAX:  %lli", LLONG_MAX);
    NSLog(@"ULLONG_MAX: %llu", ULLONG_MAX);  // unsigned long long int
}
+(NSString*) concat2Strings: (NSString*) str1
                       str2:(NSString*) str2 {
    NSString* result = [str1 stringByAppendingString: str2];
    return result;
}
+(NSString*) concat3Strings: (NSString*) str1
                       str2:(NSString*) str2
                       str3:(NSString*) str3{
    NSString* temp = [str1 stringByAppendingString: str2];
    return [temp stringByAppendingString: str3];
}
+(NSString*) concat4Strings: (NSString*) str1
                       str2:(NSString*) str2
                       str3:(NSString*) str3
                       str4:(NSString*) str4{
    NSString* temp = [str1 stringByAppendingString: str2];
    NSString* temp2 = [temp stringByAppendingString: str3];
    return [temp2 stringByAppendingString: str4];
}
+(NSURLRequest*) requestFromString: (NSString*) urlString{
    NSURL* url = [NSURL URLWithString:urlString];
    NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
    return urlRequest;
}
+(void) copyFileWithSource: (NSString*)source
                    target: (NSString*)target{
    NSError* error;
    NSString* contents = [NSString stringWithContentsOfFile: source encoding: NSUTF8StringEncoding error: &error];
    [contents writeToFile: target atomically: YES encoding: NSUTF8StringEncoding error: &error];
}
+(BOOL) noICloudCopyFileWithSource: (NSString*)source
                            target: (NSString*)target{
    NSError* error;
    NSString* contents = [NSString stringWithContentsOfFile: source encoding: NSUTF8StringEncoding error: &error];
    [contents writeToFile: target atomically: YES encoding: NSUTF8StringEncoding error: &error];
    NSURL* u = [NSURL fileURLWithPath: target];
    return [self addSkipBackupAttributeToItemAtURL: u];
    
}
+(void) writeFileContentsToLog: (NSString*) path{
    NSError* error;
    NSString* fileContents = [NSString stringWithContentsOfFile: path encoding: NSUTF8StringEncoding error: &error];
    NSLog(@"%@", fileContents);
}
+(NSString*) docsFilePath: (NSString*) fileName {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent: fileName];
    return filePath;
}
+(void) writeStringToDocsFile : (NSString*) contents
                      fileName: (NSString*) fileName {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent: fileName];
    [contents writeToFile:filePath atomically:TRUE encoding:NSUTF8StringEncoding error:NULL];
}
+(BOOL) writeStringToDocsFileNoICloudBackup : (NSString*) contents
                                    fileName: (NSString*) fileName {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent: fileName];
    NSLog(@"writing to file: %@", filePath);
    [contents writeToFile:filePath atomically:TRUE encoding:NSUTF8StringEncoding error:NULL];
    NSURL* u = [NSURL fileURLWithPath: filePath];
    return [self addSkipBackupAttributeToItemAtURL: u];
}
+(NSString*) stringFromDocsFile: (NSString*) fileName {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent: fileName];
    NSError* error;
    NSString* contents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error: &error];
    return contents;
}
+(BOOL) docsFileExists: (NSString*) fileName {
    NSString* path = [self docsFilePath: fileName];
    NSFileManager* dfm = [NSFileManager defaultManager];
    return ([dfm fileExistsAtPath: path]);
}
+(BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)fileURL {
    
    // First ensure the file actually exists
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]]) {
        NSLog(@"File %@ doesn't exist!",[fileURL path]);
        return NO;
    }
    
    // Determine the iOS version to choose correct skipBackup method
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    
    if ([currSysVer isEqualToString:@"5.0.1"]) {
        const char* filePath = [[fileURL path] fileSystemRepresentation];
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        NSLog(@"Excluded '%@' from backup",fileURL);
        return result == 0;
    }
    else if (&NSURLIsExcludedFromBackupKey) {
        NSError *error = nil;
        BOOL result = [fileURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        if (result == NO) {
            NSLog(@"Error excluding '%@' from backup. Error: %@",fileURL, error);
            return NO;
        }
        else { // Succeeded
            NSLog(@"Excluded '%@' from backup",fileURL);
            return YES;
        }
    } else {
        // iOS version is below 5.0, no need to do anything
        return YES;
    }
}
+(void) setLastModDateOfFile: (NSString*) path
                 toTimeStamp: (long long) timeStamp {
    NSFileManager* dfm = [NSFileManager defaultManager];
    NSDate* date = [self dateFromTimeStamp: timeStamp];
    NSDictionary* atts = [NSDictionary dictionaryWithObjectsAndKeys: date, NSFileModificationDate, nil];
    [dfm setAttributes: atts ofItemAtPath: path error: NULL];
}
+(NSString*) formattedSecondsString: (double) seconds {
    int s = (int)round(seconds);
    if (s < 60){
        //return [[NSString alloc] initWithFormat: @"%d sec", s];
        return @"<1min";
    }
    int minutes = (s + 30)/60;
    return [[NSString alloc] initWithFormat:@"%dmin", minutes];
    
}

+(CGPoint) intersectionWithX0 :(double) x0
                            y0: (double) y0
                            x1: (double) x1
                            y1: (double) y1
                            x2: (double) x2
                            y2: (double) y2
                            x3: (double) x3
                            y3: (double) y3 {
    double a00 = y0 - y1; double a01 = x1 - x0;
    double a10 = y2 - y3; double a11 = x3 - x2;
    double det = a00 * a11 - a01 * a10;
    double det01 = x1 * y0 - x0 * y1;
    double det23 = x3 * y2 - x2 * y3;
    // a00 x + a01 y = det01
    // a10 x + a11 y = det23
    // (a11  -a01) (det01) = (x * det)
    // (-a10  a00) (det02)   (y * det)
    double x = (a11 * det01 - a01 * det23) / det;
    double y = (a00 * det23 - a10 * det01) / det;
    return CGPointMake(x, y);
}
+(BOOL) linearIndependentWithX0 :(double) x0
                              y0: (double) y0
                              x1: (double) x1
                              y1: (double) y1
                              x2: (double) x2
                              y2: (double) y2
                              x3: (double) x3
                              y3: (double) y3 {
    double a00 = y0 - y1; double a01 = x1 - x0;
    double a10 = y2 - y3; double a11 = x3 - x2;
    double det = a00 * a11 - a01 * a10;
    double absDet = fabs(det);
    return (absDet >= 0.001);
}


@end