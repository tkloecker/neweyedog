
#import "Dijkstra.h"
#import "CSVimporter.h"
#import "Util.h"
#import "Node.h"
#import "NodeQueue.h"
#import "Picture.h"
#import "ServerPicture.h"
#import "CategoryItem.h"
#import "Server.h"
#import "DataKeeper.h"
#import "Connection.h"

@implementation Dijkstra

NSString* const pictureFileExtension = @"jpg";

static Dijkstra* soleInstance = nil;

- (id) init {
    self = [super init];
    if (!self){
        return nil;
    }
    return self;
}

+ (Dijkstra*) sharedInstance {
    if (soleInstance == nil){
        soleInstance = [[super alloc] init];
    }
    return soleInstance;
}


- (void) Initialize {
    [[CSVimporter sharedInstance] Initialize];
    //[self initLocal]; //put in this line and comment out the previous line if you want to always use the local stuff, and not contact the server.
}

- (void) initLocal {
    NSLog(@"init local");
    [self importNodes];         // read nodes from csv into data structure
    [self importConnections];   // read connections from csv into data structure
    // calc distances to neighbour nodes
    for (int i = 1; i <= numNodes; i++) {
        [[nodes objectAtIndex: i] calcDistances: nodes];
    }
    [self pictureInit];            // looks in resources folder and docs folder for existing pictuers, and stores paths and last modified dates of them in suitable node object.
    [self serverPictureInit];      // looks at pictuers.csv to find out about possible updated pictures on the server.
    [self calcShortestPaths];           // run dijkstra
    /*
     for (int i = 1; i <= numNodes; i++) {
        Node* node = [nodes objectAtIndex: i];
        [node logToConsole3];
        [node logPicsToConsoleShort];
    }
    */
    nodeNamesDict = [self importDestinationsLanguages: [[DataKeeper sharedInstance] m_strLanguage]];
    textDict = [self importUiText: [[DataKeeper sharedInstance] m_strLanguage]];
    for (int i=1; i<=numNodes; i++){
        Node* node = [nodes objectAtIndex: i];
        [node retrieveNameFromNameIdWithDictinary: nodeNamesDict];
    }
    for (int i=1; i<=numNodes; i++){
        Node* node = [nodes objectAtIndex: i];
        [node loadFromServerAndStoreInDocumentsIfNecessary];
    }
    [[DataKeeper sharedInstance] Initialize2];
    [self importCategories];
}

- (void) ChangeLanguage: (NSString *) language {
    nodeNamesDict = [self importDestinationsLanguages: language];
    textDict = [self importUiText:language];
    for (int i=1; i<=numNodes; i++){
        Node* node = [nodes objectAtIndex: i];
        [node retrieveNameFromNameIdWithDictinary: nodeNamesDict];
    }
    for (int i=1; i<=numNodes; i++){
        Node* node = [nodes objectAtIndex: i];
        [node loadFromServerAndStoreInDocumentsIfNecessary];
    }
    [[DataKeeper sharedInstance] Initialize2];
    [self importCategories];
}

- (NSMutableDictionary*) getNodeNamesDict{
    return nodeNamesDict;
}

- (NSMutableDictionary*) getTextDict{
    return textDict;
}

-(void) serverPictureInit {
    NSString* serverPathIMG = [Server serverPathIMG];
    int const fromPosition = 0;
    int const toPosition = 1;
    int const indexPosition = 2;
    int const fileExtensionPosition = 3;
    int const timeStampPosition = 4;
    NSString* picsFileContents = [Util getLocalCsv: @"pictures"];
    NSArray* lines =  [picsFileContents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    int numLines = [lines count];
    for (int i=0; i<numLines; i++){
        NSString* line = [lines objectAtIndex:i];
        NSArray* entries = [line componentsSeparatedByString:@","];
        int entryCount = [entries count];
        if (entryCount >= 5){
            NSString* fromString = [entries objectAtIndex: fromPosition];
            NSString* toString = [entries objectAtIndex: toPosition];
            NSString* indexString = [entries objectAtIndex: indexPosition];
            NSString* fileExtension = [entries objectAtIndex: fileExtensionPosition];
            NSString* timeStampString = [entries objectAtIndex: timeStampPosition];
            BOOL valid1 = [Util containsOnlyNumberDigits: fromString];
            BOOL valid2 = [Util containsOnlyNumberDigits: toString];
            BOOL valid3 = [Util containsOnlyNumberDigits: indexString];
            BOOL valid4 = YES;
            BOOL valid5 = [Util containsOnlyNumberDigits: timeStampString];
            BOOL valid = valid1 && valid2 && valid3 && valid4 && valid5;
            if (valid) {
                int from = [fromString intValue];
                int to = [toString intValue];
                int index = [indexString intValue];
                long long timeStamp = [timeStampString longLongValue];
                //NSLog(@"%@   %d from: %d, to: %d, index: %d, extension: %@, timeStamp: %lld", line, entryCount, from, to, index, fileExtension, timeStamp);
                Node* node = [nodes objectAtIndex: from];
                NSString* path = [[NSString alloc] initWithFormat: @"%@%d_%d_%d.%@", serverPathIMG, from, to, index, fileExtension];
                ServerPicture* pic = [[ServerPicture alloc] initWithFrom:  from to: to index: index path: path timeStamp: timeStamp];
                [node addServerPicture: pic];
                
            } else {
                //NSLog(@"%@ - not valid", line);
            }
        }
    }
    for (int i = 1; i <= numNodes; i++) {
        Node* node = [nodes objectAtIndex: i];
        [node sortServerPictures];
        [node attachServerPictures];
    }
}

- (void) pictureInit {
    for (int i=1; i<=numNodes; i++){
        Node* node = [nodes objectAtIndex: i];
        [node logToConsole3];
        int numNeighbours = [node getNumNeighbours];
        int* neighbours = [node getNeighbours];
        NSMutableArray* picsArr = [[NSMutableArray alloc] init];
        for (int n=0; n<numNeighbours; n++){
            int neighbourId = neighbours[n];
            //NSLog(@"%d", neighbourId);
            int picIndex = 0;
            NSMutableArray* pics = nil;
            while (YES){
                BOOL isResource;
                picIndex++;
                NSString* path;
                NSString* fileName = [[NSString alloc] initWithFormat: @"%d_%d_%d.%@", i, neighbourId, picIndex, pictureFileExtension];
                if ([Util docsFileExists: fileName]){
                    //NSLog(@"%@ exists in docs", fileName);
                    path = [Util docsFilePath: fileName];
                    isResource = NO;
                } else {
                    NSString* fileNameWithoutExtension = [[NSString alloc] initWithFormat: @"%d_%d_%d", i, neighbourId, picIndex];
                    path = [[NSBundle mainBundle] pathForResource: fileNameWithoutExtension  ofType: pictureFileExtension];
                    if (path == nil){
                        //NSLog(@"%@ exists nowhere", fileName);
                        break;
                    } else {
                        isResource = YES;
                        //NSLog(@"%@ exists in resources. path is: %@", fileName, path);
                    }
                }
                long long timeStamp = [Util lastModifiedTimeStampOfFile: path];
                //NSLog(@"last modified: %lld", timeStamp);
                Picture* pic = [[Picture alloc] initWithFrom: i to: neighbourId index: picIndex path: path timeStamp: timeStamp isResource: isResource];
                if (pics == nil){
                    pics = [[NSMutableArray alloc] init];
                }
                [pics addObject: pic];
            } // while
            if (pics == nil){
                [picsArr addObject: [NSNull null]];
            } else {
                [picsArr addObject: pics];
            }
        } // neighbours
        [node setPictures: picsArr];
    } // nodes
}
- (NSMutableDictionary*) importUiText: (NSString *) language {
    NSString* fileContents = [Util getLocalCsv: @"text_languages"];
    NSString* chosenLanguage = language;
    // ...
    // to do for you, Lin
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];

    NSArray* lines = [fileContents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    NSInteger numLines = [lines count];
    
    NSString* headerLine = [lines objectAtIndex: 0];
    NSArray* headerEntries = [headerLine componentsSeparatedByString:@","];

    NSLog(@"Dutch: %d", [headerEntries indexOfObject: @"Dutch"]);
    NSLog(@"English: %d", [headerEntries indexOfObject: @"English"]);
    NSLog(@"German: %d", [headerEntries indexOfObject: @"German"]);
    NSLog(@"French: %d", [headerEntries indexOfObject: @"French"]);
    NSLog(@"Turkish: %d", [headerEntries indexOfObject: @"Turkish"]);
    NSLog(@"Danish: %d", [headerEntries indexOfObject: @"Danish"]);
    NSLog(@"Swedish: %d", [headerEntries indexOfObject: @"Swedish"]);
    NSLog(@"Norwegian: %d", [headerEntries indexOfObject: @"Norwegian"]);
    NSLog(@"Spanish: %d", [headerEntries indexOfObject: @"Spanish"]);
    NSLog(@"Italian: %d", [headerEntries indexOfObject: @"Italian"]);
    NSLog(@"Chinese: %d", [headerEntries indexOfObject: @"Chinese"]);
    NSInteger chosenLangPos = [headerEntries indexOfObject: chosenLanguage];
    NSLog(@"chosen language pos: %d", chosenLangPos);

    
    for (NSInteger nIndex = 1; nIndex < numLines; nIndex++) {
        
        NSString* line = [lines objectAtIndex:nIndex];
        NSArray* entries = [line componentsSeparatedByString:@","];
        
        if ([entries count] > chosenLangPos){
            NSString* key = [entries objectAtIndex: 0];
            NSString* value = [entries objectAtIndex: chosenLangPos];
            //NSLog(@"key: %@, value: %@", key, value);
            [dict setObject: value forKey: key];
        }
    }
    
    return dict;
}

- (void)importNodes {
    NSString* nodesFileContents = [Util getLocalCsv: @"nodes"];
    nodes = [[NSMutableArray alloc] init];
    // add fake node at the beginning, because ids start at 1, and ids should be identical with array index.
    Node* fakeNode = [[Node alloc] initWithId: 0 level: 0 x: 0 y: 0 type: @"fake" nameId: @"fake" zone: @"fake" category: 0 pushFactor: -1 showTopView: NO];
    [fakeNode allocWithNumNeighbours: 0 numNodes: 0];
    [nodes addObject: fakeNode];
    NSArray* lines = [nodesFileContents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    if (NO){
        NSLog(@"!!! error loading nodes csv file!!!");
        return;
    } else {}
    int numLines = [lines count];
    for (int i=0; i<numLines; i++){
        NSString* line = [lines objectAtIndex:i];
        //NSLog(@"%@", line);
        NSArray* entries = [line componentsSeparatedByString:@","];
        int entryCount = [entries count];
        //NSLog(@"********* line = %d, entries: %d",i, entryCount);
        if (entryCount >= NUM_NODE_ENTRIES - 3) {
            NSString* idString = [entries objectAtIndex: ID_POSITION];
            NSString* lvlString = [entries objectAtIndex: LEVEL_POSITION];
            NSString* xString = [entries objectAtIndex: X_POSITION];
            NSString* yString = [entries objectAtIndex: Y_POSITION];
            NSString* zoneString = [entries objectAtIndex: ZONE_POSITION];
            NSString* categoryString = @"";
            NSString* pushFactorString = @"";
            NSString* showTopViewString = @"";
            if (entryCount >= NUM_NODE_ENTRIES - 2){
                categoryString = [entries objectAtIndex: CATEGORY_POSITION];
            }
            if (entryCount >= NUM_NODE_ENTRIES - 1){
                pushFactorString = [[entries objectAtIndex: PUSH_FACTOR_POSITION] stringByReplacingOccurrencesOfString: @"\r" withString: @""];
            }
            if (entryCount >= NUM_NODE_ENTRIES){
                showTopViewString = [[entries objectAtIndex: SHOW_TOP_VIEW_POSITION] stringByReplacingOccurrencesOfString: @"\r" withString: @""];
                showTopViewString = [showTopViewString uppercaseString];
            }
            BOOL valid1 = [Util containsOnlyNumberDigits: idString];
            BOOL valid2 = [Util containsOnlyNumberDigitsAndMinus: lvlString];
            BOOL valid3 = [Util containsOnlyNumberDigitsAndMinus: xString];
            BOOL valid4 = [Util containsOnlyNumberDigitsAndMinus: yString];
            BOOL valid = valid1 && valid2 && valid3 && valid4;
            //NSLog(@"valid: %d %d %d %d %d", valid1, valid2, valid3, valid4, valid);
            //NSLog(@"%d", [zoneString length]);
            if (valid){
                int theId = [idString intValue];
                int level = [lvlString intValue];
                int x = [xString intValue];
                int y = [yString intValue];
                NSString* type = [entries objectAtIndex: TYPE_POSITION];
                NSString* nameId = [entries objectAtIndex: NAME_POSITION];
                NSString* zone = zoneString;
                int pushFactor = -1;
                if ([pushFactorString length] > 0 && [Util containsOnlyNumberDigits: pushFactorString]){
                    pushFactor = [pushFactorString intValue];
                }
                BOOL showTopView = YES;
                if ([showTopViewString isEqualToString: @"FALSE"]){
                    showTopView = NO;
                }
                int category = [categoryString intValue];
                Node* node = [[Node alloc] initWithId: theId level: level x: x y: y type: type nameId: nameId zone: zone category: category pushFactor: pushFactor showTopView: showTopView];
                //[node logToConsole];
                [nodes addObject:node];
            }
        }
    }
    numNodes = [nodes count] - 1;
    NSLog(@"number of nodes: %d", numNodes);
}
-(NSMutableDictionary*) importDestinationsLanguages: (NSString *) language {
    NSString* chosenLanguage = language;
    //NSString* chosenLanguage = @"English";
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    NSString* destLangFileContents = [Util getLocalCsv: @"destinations_languages"];
    NSArray* lines = [destLangFileContents componentsSeparatedByString:@"\r\n"];
    NSString* headerLine = [lines objectAtIndex: 0];
    NSArray* headerEntries = [headerLine componentsSeparatedByString:@","];
    NSLog(@"Dutch: %d", [headerEntries indexOfObject: @"Dutch"]);
    NSLog(@"English: %d", [headerEntries indexOfObject: @"English"]);
    NSLog(@"German: %d", [headerEntries indexOfObject: @"German"]);
    NSLog(@"French: %d", [headerEntries indexOfObject: @"French"]);
    NSLog(@"Turkish: %d", [headerEntries indexOfObject: @"Turkish"]);
    NSLog(@"Danish: %d", [headerEntries indexOfObject: @"Danish"]);
    NSLog(@"Swedish: %d", [headerEntries indexOfObject: @"Swedish"]);
    NSLog(@"Norwegian: %d", [headerEntries indexOfObject: @"Norwegian"]);
    NSLog(@"Spanish: %d", [headerEntries indexOfObject: @"Spanish"]);
    NSLog(@"Italian: %d", [headerEntries indexOfObject: @"Italian"]);
    NSLog(@"Chinese: %d", [headerEntries indexOfObject: @"Chinese"]);
    int chosenLangPos = [headerEntries indexOfObject: chosenLanguage];
    NSLog(@"chosen language pos: %d", chosenLangPos);
    int numLines = [lines count];
    for (int i=1; i<numLines; i++) {
        NSString* line = [lines objectAtIndex: i];
        NSArray* entries = [line componentsSeparatedByString: @","];
        if ([entries count] > chosenLangPos){
            NSString* key = [entries objectAtIndex: 0];
            NSString* value = [entries objectAtIndex: chosenLangPos];
            //NSLog(@"key: %@, value: %@", key, value);
            [dict setObject: value forKey: key];
        }
    }
    return dict;
}
-(void)importConnections {
    int fromPos = 0;
    int toPos = 1;
    int textPos = 2;
    int delayPos = 3;
    int widthPos = 4;
    int iconPos = 5;
    NSString* connectionsFileContents = [Util getLocalCsv: @"connections"];
    NSArray* lines = [connectionsFileContents componentsSeparatedByString:@"\n"];
    //NSArray* lines = [connectionsFileContents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    if (NO){
        NSLog(@"!!! error loading connictions csv file!!!");
    } else {}
    int numLines = [lines count];
    NSMutableArray* connections = [[NSMutableArray alloc] init];
    for (int i=1; i<numLines; i++){
        NSString* line = [lines objectAtIndex: i];
        NSArray* entries = [line componentsSeparatedByString:@","];
        int entryCount = [entries count];
        if (entryCount >= 5){
            int from = [[entries objectAtIndex: fromPos] intValue];
            int to = [[entries objectAtIndex: toPos] intValue];
            NSString* text = [entries objectAtIndex: textPos];
            double delay = [[entries objectAtIndex: delayPos] doubleValue];
            double width = [[entries objectAtIndex: widthPos] doubleValue];
            NSString* iconPath = @"";
            if (entryCount >= 6){
                iconPath = [entries objectAtIndex: iconPos];
            }
            Connection* conn = [[Connection alloc] initWithFrom: from To: to Text: text Delay: delay Width: width IconPath: iconPath];
            [connections addObject: conn];
            Node* node = [nodes objectAtIndex: from];
            [node incNumNeighbours];
        }
    }
    for (int i = 1; i <= numNodes; i++) {
        Node* node = [nodes objectAtIndex: i];
        [node allocWithNumNeighbours: [node getNumNeighbours] numNodes: numNodes];
        [node setNumNeighboursToZero];
    }
    for (int i=0; i<[connections count]; i++){
        Connection* conn = [connections objectAtIndex: i];
        int from = [conn getFrom];
        int to = [conn getTo];
        Node* node = [nodes objectAtIndex: from];
        int index = [node getNumNeighbours];
        [node incNumNeighbours];
        [node getNeighbours][index] = to;
        [node getNeighbInd][to] = index;
        [node addConnection: conn];
    }
}

-(void)importCategories {
    NSString* fileContents = [Util getLocalCsv: @"des_groups"];
    NSArray* lines = [fileContents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    
    if (NO){
        NSLog(@"!!! error loading connictions csv file!!!");
    } else {}
    
    int numLines = [lines count];
    if (cats == nil) {
        cats = [[NSMutableArray alloc] init];
    } else {
        [cats removeAllObjects];
    }
    
    for (int i = 0; i < numLines; i++) {
        
        if (i == 0)
            continue;
        
        NSString *line = [lines objectAtIndex: i];
        NSArray* entries = [line componentsSeparatedByString:@","];
        
        if (entries == nil || [entries count] <= 4 || [[entries objectAtIndex: 1] length] <= 0) {
            continue;
        }
        
        NSInteger nID = [[entries objectAtIndex: 0] integerValue];
        BOOL bNodeExist = NO;
        
        for (NSInteger nIndex = 0; nIndex < [nodes count]; nIndex++) {
            Node *node = [nodes objectAtIndex: nIndex];
            
            if ([node getCategory] == nID && [node isJunction] == NO) {
                bNodeExist = YES;
                break;
            }
        }
        
        if (bNodeExist == NO)
            continue;
        
        CategoryItem *cat = [[CategoryItem alloc] init];
        
        cat.nID = nID;
        cat.iconID = [entries objectAtIndex: 1];
        
        cat.name = [[NSDictionary alloc] initWithObjectsAndKeys:[entries objectAtIndex: 2], @"English", [entries objectAtIndex: 3], @"Dutch", [entries objectAtIndex: 4], @"German", [entries objectAtIndex: 5], @"French", [entries objectAtIndex: 6], @"Turkish", [entries objectAtIndex: 7], @"Danish", [entries objectAtIndex: 8], @"Swedish", [entries objectAtIndex: 9], @"Norwegian", [entries objectAtIndex: 10], @"Spanish", [entries objectAtIndex: 11], @"Italian", [entries objectAtIndex: 12], @"Chinese", nil];
        
        [cats addObject: cat];
    }
}


- (void) calcShortestPaths {
    for (int i = 1; i <= numNodes; i++) {
        Node* node = [nodes objectAtIndex: i];
        if ([node isJunction] || [node isDestination]){
            [self dijkstra: i];
        }
    }
}
- (void) dijkstra: (int)sourceIndex {
    NodeQueue* q = [[NodeQueue alloc] initWithLength: numNodes];
    
    for (int i=1; i<=numNodes; i++){
        Node* node = [nodes objectAtIndex: i];
        [node setDistInfinite];
        [node setPrev: -1];
    }
    
    Node* sourceNode = [nodes objectAtIndex: sourceIndex];
    [sourceNode setDist: 0];
    
    while ([q getLength] > 0) {
        Node* currentNode = [q getMinDistNode: nodes];
        int currentNodeIndex = [currentNode getId];
        double distToCurrent = [currentNode getDist];
        int numNeighbours = [currentNode getNumNeighbours];
        
        for (int n = 0; n < numNeighbours; n++) {
            
            int neighbourId = [currentNode getNeighbours][n];
            Node* neighbour = [nodes objectAtIndex: neighbourId];
            double distCurrentToNeighbour = [currentNode getDistanceToNeighbour: n];
            double alternativeDistance = distToCurrent + distCurrentToNeighbour;
            
            if (alternativeDistance < [neighbour getDist]) {
                [neighbour setDist: alternativeDistance];
                [neighbour setPrev: currentNodeIndex];
            }
        }
    }
    
    int* pathLengths = malloc((numNodes+1) * sizeof(int));
    double* euclidianPathLengths = malloc((numNodes+1) * sizeof(double));
    
    
    for (int i = 1; i <= numNodes; i++) {
        pathLengths[i] = 0;
        euclidianPathLengths[i] = [[nodes objectAtIndex: i] getDist];
        
        int current = i;
        while (true) {
            pathLengths[i]++;
            if (current == sourceIndex){
                break;
            }
            
            current = [[nodes objectAtIndex: current] getPrev];
            
            if (current == -1) {
                break;
            }
        }
    }
    
    int** paths = malloc((numNodes + 1) * sizeof(int *));
    
    for (int i = 1; i <= numNodes; i++) {
        paths[i] = malloc(pathLengths[i] * sizeof(int));
        int current = i;
        int pos = pathLengths[i];
        
        while (true) {
            pos--;
            paths[i][pos] = current;
            
            if (current == sourceIndex){
                break;
            }
            
            current = [[nodes objectAtIndex: current] getPrev];
            
            if (current == -1){
                break;
            }
        }
    }
    
    [sourceNode setPathLengths: pathLengths];
    [sourceNode setPaths: paths];
    [sourceNode setEuclidianPathLengths: euclidianPathLengths];
    
}

- (Node*) getNodeFromId: (int) theId {
    return [nodes objectAtIndex: theId];
}
-(int*) getPathIdListFrom: (Node*) fromNode ToNode: (Node*) toNode{
    int destinationId = [toNode getId];
    int* thePath = [fromNode getPath: destinationId];
    return thePath;
}
-(int) getPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode {
    int destinationId = [toNode getId];
    int pathLength = [fromNode getPathLength: destinationId];
    return pathLength;
}
-(double) getEuclidianPathLengthFrom: (Node*) fromNode ToNode: (Node*) toNode {
    return [fromNode getEuclidianPathLength: [toNode getId]];
}
-(NSMutableArray*) getAngleListFrom: (Node*) fromNode ToNode: (Node*) toNode {
    int pathLength = [self getPathLengthFrom: fromNode ToNode: toNode];
    int* thePath = [self getPathIdListFrom: fromNode ToNode: toNode];
    NSMutableArray* result = [[NSMutableArray alloc] initWithCapacity: pathLength-1];
    for (int i=1; i<pathLength-1;i++){
        Node* previous = [nodes objectAtIndex: thePath[i-1]];
        Node* current = [nodes objectAtIndex: thePath[i]];
        Node* next = [nodes objectAtIndex: thePath[i+1]];
        int prevX = [previous getX];
        int prevY = [previous getY];
        int curX = [current getX];
        int curY = [current getY];
        int nextX = [next getX];
        int nextY = [next getY];
        int dx1 = curX - prevX;
        int dy1 = curY - prevY;
        int dx2 = nextX - curX;
        int dy2 = nextY - curY;
        int dLvl = [next getLevel] - [current getLevel];
        double len1 = sqrt(dx1 * dx1 + dy1 * dy1);
        double len2 = sqrt(dx2 * dx2 + dy2 * dy2);
        double len12 = len1 * len2;
        int deter = dy1 * dx2 - dx1 * dy2;
        double angle = asin(deter/len12);
        double angleDegrees = angle * 180 / M_PI;
        if (dLvl == 0){
            if (len12 == 0) {
                [result setObject: [NSNull null] atIndexedSubscript: i-1];
            } else {
                // 0 = straight
                // > 0 = take a left
                // < 0 = take a right
                int scalpro = dx1 * dx2 + dy1 * dy2;
                // scalpro >= 0 -> angle is correct and between -90 degrees and 90 degrees
                // scalpro < 0 -> we have sharp turn > 90 or < -90, and must correct angle
                if (scalpro < 0){
                    if (angleDegrees > 0){
                        angleDegrees = 180.0 - angleDegrees;
                    } else {
                        angleDegrees = -180.0 - angleDegrees;
                    }
                }
                //angleDegrees = 170; --> throws error: to do for Lin!
                [result setObject: [[NSNumber alloc] initWithDouble: angleDegrees] atIndexedSubscript: i-1];
            }
        } else {
            [result setObject: [NSNull null] atIndexedSubscript: i-1];
        }
        
    }
    [result setObject: [NSNull null] atIndexedSubscript: pathLength - 2]; // angle at destination impossible to calculate / know
    return result;
}
- (int) getNumNodes {
    return numNodes;
}

- (NSInteger) getCatCount {
    return [cats count];
}

- (CategoryItem *) getCategoryAtIndex: (NSInteger) nIndex {
    return [cats objectAtIndex: nIndex];
}

- (NSMutableArray *) getCategories {
    return cats;
}

@end
