#import <Foundation/Foundation.h>
#import "SingletonClass.h"

@interface Server : NSObject

+ (NSString*) serverPath;
+ (NSString*) serverPathCSV;
+ (NSString*) serverPathIMG;


@end
