//
//  Category.m
//  Meander
//
//  Created by Wony on 2/26/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "CategoryItem.h"

@implementation CategoryItem

- (NSString *) getTitle {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    return [_name objectForKey: dataKeeper.m_strLanguage];
}

@end
