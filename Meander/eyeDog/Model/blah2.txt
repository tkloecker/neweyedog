changes:

nodes.csv now has 2 additional columns called "category" and "push factor" , both are ints

both are optional

default category is 0

default push factor is -1

if all categories are 0, use single list (no grouping), as before

if categories != 0 occur, then all destinations must have a category != 0 (otherwise the csv is faulty - a "sanity check" for that does not exist yet.)
in that case, use grouped list for destinations.

to find out which icon and which title to use for the group headings of each category, please refer to attached file destination groups v1.1.xlsx
please convert to csv, include in resources folder, and read it in at startup.

To get all sources or all destinations, with suitable ordering, but only for one category, use (in DataKeeper)

- (NSArray*) getSourcesForCategory: (int) cat;
- (NSArray*) getAlphabeticallyOrderedDestinationsForCategory: (int) cat;
- (NSArray*) getDistanceOrderedDestinationListWithSourceNodeId: (int) sourceNodeId
                                                   ForCategory: (int) cat;

the same purpose, but when there are no categories, fulfil

- (NSArray*) getSources;
- (NSArray*) getAlphabeticallyOrderedDestinations;
- (NSArray*) getDistanceOrderedDestinationListWithSourceNodeId: (int) sourceNodeId;

===

push factor:

that's for advertisement.
items with push factors != default value are shown on top of their respective category (if categories exist), or on top of the single list (if no categories exist).

Code for getting the nodes that have a push factor is not written yet.

So for now, do nothing with the push factor.

===

note that down.png in Resources > Images > Arrows has changed.

===

note that in line 35 / 36 in Dijkstra.m, I commented out the line that fetches data from the server (if necessary), and put in the line that always uses local resources, without any server communication.

The reason is that the new csv file with the 2 new columns should be used / tested, but on the server there is still the old version without those categories.

===

Ralph will probably want to demo both the grouped and the non-grouped versions in the demo on Wednesday
("top view" / "partial floor plan" inclusion (see eyeDogGraph project) is also important for Wednesday, the push factor thing is not)

Please prepare 2 versions, one using the new csv file with groups, one using the old csv file, so that no groups are shown - otherwise identical.

(import is made so that it works with or without the 2 new coloums, so no change necessary here.)

probably best to cut of the server communication part in the grouped case, because otherwise the non - grouped csv file will be downloaded from the server, and the demo will not work.