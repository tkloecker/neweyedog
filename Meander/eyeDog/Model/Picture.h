#import <Foundation/Foundation.h>
#import "ServerPicture.h"

@interface Picture : NSObject {
    
    int from;
    int to;
    int index;
    NSString* path;
    long long timeStamp;
    NSString* serverPath;
    long long serverTimeStamp;
    BOOL isResource;
    BOOL isHTTP;
}

-(id) initWithFrom:(int) _from
                to:(int) _to
             index:(int) _index
              path:(NSString*) _path
         timeStamp:(long long) _timeStamp
        isResource:(BOOL) _isResource;

-(id) initWithServerPicture: (ServerPicture*) serverPic;

-(int) getFrom;
-(int) getTo;
-(int) getIndex;
-(NSString*) getPath;
-(NSString*) getFileName;
-(long long) getTimeStamp;
-(void) attachServerPicture: (ServerPicture*) serverPic;
-(void) logToConsole;
-(void) logToConsoleShort;
-(BOOL) picIsResource;
-(BOOL) picIsHTTP;

-(void) getFromServerAndStore;
@end
