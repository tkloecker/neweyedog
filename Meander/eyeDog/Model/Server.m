#import "Server.h"
#import "Util.h"

@implementation Server

//NSString* const serverRoot = @"http://82.192.68.204/root/buildings/";
NSString* const serverRoot = @"http://host.eyedog.mobi/root/buildings/";

//NSString* const serverRoot = @"http://host.eye-dog.nl/root/buildings/";
NSString* const building = @"building1";


+ (NSString*) serverPath {
    return [Util concat3Strings: serverRoot str2: building str3: @"/"];
}
+ (NSString*) serverPathCSV {
    return [Util concat2Strings: [self serverPath] str2: @"csv/"];
}
+ (NSString*) serverPathIMG {
    return [Util concat2Strings: [self serverPath] str2: @"img/"];
}


@end
