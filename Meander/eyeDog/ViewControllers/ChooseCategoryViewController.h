//
//  ChooseCategoryViewController.h
//  Meander
//
//  Created by Wony on 2/25/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CategoryCell.h"
#import "SchemeHolderViewController.h"

@interface ChooseCategoryViewController : SchemeHolderViewController {
    NSMutableArray *m_arrayFiltered;
}

@property (nonatomic) BOOL m_bDest;
@property (strong, nonatomic) IBOutlet CategoryCell *m_CategoryCell;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *m_SearchBar;

@property (weak, nonatomic) IBOutlet UILabel *m_labelNavTitle;
@property (weak, nonatomic) IBOutlet UIButton *m_navBackBtn;



- (IBAction)onTouchNavBackBtn:(id)sender;

@end
