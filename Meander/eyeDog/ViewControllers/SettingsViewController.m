//
//  SettingsViewController.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "SettingsViewController.h"
#import "HHTabListController.h"
#import "SelectLanguageViewController.h"


@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    [self SetLabels];

    /*
	if ([self isMovingToParentViewController]) {
		HHTabListController *tabListController = [self tabListController];
        
        UIButton *button = tabListController.revealTabListButtonItem;
        
        [self.navigationView addSubview: button];
        
        [button setFrame: CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
		
	}
     */
}

- (void) SetLabels {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    [self.m_labelNavTitle setText: [dataKeeper getTextByID: 26]];
    m_arraySettingNames = @[[dataKeeper getTextByID: 34], [dataKeeper getTextByID: 35], [dataKeeper getTextByID: 36]];
    
    [_m_tableView reloadData];

}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [m_arraySettingNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[NSBundle mainBundle] loadNibNamed:@"SettingCell" owner:self options: nil];
    
    [_m_SettingCell.m_labelTitle setText: [m_arraySettingNames objectAtIndex: indexPath.row]];
    
  
    [_m_SettingCell setTag: indexPath.row];
    [_m_SettingCell setSelectionStyle: UITableViewCellSelectionStyleNone];
    
    if (indexPath.row == 0) {
       // [_m_SettingCell.m_switch setOn: [[DataKeeper sharedInstance] m_bCapturePhoto]];
    } else if (indexPath.row == 1) {
       // [_m_SettingCell.m_switch setOn: [[DataKeeper sharedInstance] m_bPrintID]];
    } else if (indexPath.row == 2) {
        [_m_SettingCell.m_switch setHidden: YES];
    }
    
    return _m_SettingCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        SelectLanguageViewController *selectLangVC = [[SelectLanguageViewController alloc] initWithNibName:@"SelectLanguageViewController" bundle: nil];
        
        [self.navigationController pushViewController: selectLangVC animated: YES];
    }
}




@end
