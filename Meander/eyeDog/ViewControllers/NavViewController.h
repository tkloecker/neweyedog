//
//  NavViewController.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectionArrowView.h"

#import "SchemeHolderViewController.h"

@interface NavViewController : SchemeHolderViewController <UIScrollViewDelegate, UIAlertViewDelegate, DirectionArrowViewProtocol> {
    NSMutableArray *m_arrayArrows;
    NSInteger m_nCurStep;
}

@property (weak, nonatomic) IBOutlet UIScrollView *m_scrollView;
@property (weak, nonatomic) IBOutlet UILabel *m_labelDirection;
@property (weak, nonatomic) IBOutlet UILabel *m_labelStep;
@property (weak, nonatomic) IBOutlet UILabel *m_labelDistance;
@property (weak, nonatomic) IBOutlet UILabel *m_labelTime;
@property (weak, nonatomic) IBOutlet UILabel *m_labelDistination;

@property (weak, nonatomic) IBOutlet UIImageView *m_imgViewPanel;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgViewDirection;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgViewBotton;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgViewFinished;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgViewFlag;
@property (weak, nonatomic) IBOutlet DirectionArrowView *m_directionArrowView;

@property (weak, nonatomic) IBOutlet UIView *m_viewGraphFrame;
@property (weak, nonatomic) IBOutlet UIButton *m_btnGraphOpen;
@property (weak, nonatomic) IBOutlet UIView *m_viewGuideFrame;
@property (weak, nonatomic) IBOutlet UILabel *m_labelGuide;
@property (weak, nonatomic) IBOutlet UIButton *m_btnGuide;


@property (weak, nonatomic) IBOutlet UIButton *m_btnStop;


- (IBAction)onTouchNavBackBtn:(id)sender;
- (IBAction)onTouchCloseBtn:(id)sender;
- (IBAction)onTouchGraphOpenBtn:(id)sender;
- (IBAction)onTouchGuideBtn:(id)sender;


@end
