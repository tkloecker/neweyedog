//
//  NotAvailableViewController.m
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "NotAvailableViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface NotAvailableViewController ()

@property (nonatomic, weak) IBOutlet UIView *alertHolder;
@property (nonatomic, weak) IBOutlet UIButton *confirmButton;
@property (nonatomic, weak) IBOutlet UILabel *alertLabel;
@end

@implementation NotAvailableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)shvcConfigureAppearence
{
    [super shvcConfigureAppearence];
    
    //Border at text
    self.alertHolder.layer.borderColor = [[UIColor colorWithWhite:0.796 alpha:1] CGColor];
    self.alertHolder.layer.borderWidth = 1;
    self.alertHolder.layer.cornerRadius = 15;
    
    //Button border
    /*self.confirmButton.layer.borderWidth = 2;
    self.confirmButton.layer.borderColor = [[UIColor blackColor] CGColor];
    self.confirmButton.layer.cornerRadius = 15;
    */
    //Set text on screen
    self.alertLabel.text = [[DataKeeper sharedInstance] getTextByID: 27];
}
@end
