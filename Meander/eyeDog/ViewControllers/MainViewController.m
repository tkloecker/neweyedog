//
//  MainViewController.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "MainViewController.h"
#import "HHTabListController.h"
#import "ChooseCategoryViewController.h"
#import "HelperViewController.h"
#import "ScanQRViewController.h"
#import "NavViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface MainViewController ()<QRCodeReaderDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView *contentHolderView;
@property (nonatomic, weak) IBOutlet UIImageView *logoView;

@property (nonatomic, weak) IBOutlet UILabel *sourceTextLbl;
@property (nonatomic, weak) IBOutlet UILabel *destTextLbl;

@property (nonatomic, weak) IBOutlet UIImageView *sourceStatusView;
@property (nonatomic, weak) IBOutlet UIImageView *destStatusView;
@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)shvcConfigureAppearence
{
    [super shvcConfigureAppearence];
    
    //button colors
    [self.m_btnStart setBackgroundColor:[UIColor buttonsBackgroundColor]];
    [self.m_btnStart setTitleColor:[UIColor buttonsTextColor] forState:UIControlStateNormal];
    self.m_btnStart.layer.cornerRadius = 23;
    
    
    self.view.backgroundColor = [UIColor navigationBarColor];
    self.contentHolderView.backgroundColor = [UIColor mainViewBackgroundColor];
    
    self.destTextLbl.textColor = [UIColor notActiveTextColor];
    self.sourceTextLbl.textColor = [UIColor notActiveTextColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
    [self SetLabels];
    
	if ([self isMovingToParentViewController]) {
		HHTabListController *tabListController = [self tabListController];
		//UIBarButtonItem *leftBarButtonItem = tabListController.revealTabListBarButtonItem;
		
		//self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        
        UIButton *button = tabListController.revealTabListButtonItem;
        
        [self.view addSubview: button];
        
        [button setFrame: CGRectMake(0, 27, button.frame.size.width, button.frame.size.height)];
	}
    
    [self ShowSourceAndDest];
}

- (void) SetLabels {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
  //  [_m_labelGuide setText: [dataKeeper getTextByID: 18]];
    [_m_btnPickSource setTitle:[NSString stringWithFormat:@"  %@", [dataKeeper getTextByID: 19]] forState:UIControlStateNormal];
//    [_m_btnScanQR setTitle:[NSString stringWithFormat:@"  %@", [dataKeeper getTextByID: 20]] forState:UIControlStateNormal];
    [_m_btnPickDest setTitle:[NSString stringWithFormat:@"  %@", [dataKeeper getTextByID: 21]] forState:UIControlStateNormal];
    [_m_btnStart setTitle:[dataKeeper getTextByID: 50] forState:UIControlStateNormal];
    
    self.sourceTextLbl.text = [dataKeeper getTextByID: 48];
    self.destTextLbl.text = [dataKeeper getTextByID: 49];

}


- (void) ShowSourceAndDest {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    if (dataKeeper.source != nil) {
        self.sourceTextLbl.textColor = [UIColor activeTextColor];
        self.sourceTextLbl.text = [NSString stringWithFormat:@"%@", [dataKeeper.source getName]];
        
        self.sourceStatusView.image = [UIImage locationSelected];
        
        [self.m_btnPickSource setTitle:@"Change Starting Point" forState:UIControlStateNormal];
    } else {
        self.sourceTextLbl.textColor = [UIColor notActiveTextColor];
        self.sourceTextLbl.text = [dataKeeper getTextByID: 48];
        
        self.sourceStatusView.image = [UIImage locationNotSelected];
        [_m_btnPickSource setTitle:[NSString stringWithFormat:@"  %@", [dataKeeper getTextByID: 19]] forState:UIControlStateNormal];
    }
    
    if (dataKeeper.destination != nil) {
        
        self.destTextLbl.textColor = [UIColor activeTextColor];
        self.destTextLbl.text = [NSString stringWithFormat:@"%@", [dataKeeper.destination getName]];
        
        self.destStatusView.image = [UIImage locationSelected];
        [self.m_btnPickDest setTitle:@"Change Destination" forState:UIControlStateNormal];
    } else {
        
        self.destTextLbl.textColor = [UIColor notActiveTextColor];
        self.destTextLbl.text = [dataKeeper getTextByID: 49];
        
        self.destStatusView.image = [UIImage locationNotSelected];
        [_m_btnPickDest setTitle:[NSString stringWithFormat:@"  %@", [dataKeeper getTextByID: 21]] forState:UIControlStateNormal];
    }
}

#pragma mark - QRCode delegate

// When Success to read QR Code
- (void) SuccessToReadQRCode {
    NSLog(@"Success");
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    NSLog(@"QRCode = %@", dataKeeper.m_strQRCode);
    
    [dataKeeper setSource: [dataKeeper getNodeFromId: (int)[dataKeeper.m_strQRCode integerValue]]];
}

- (void) OnTouchBackBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
}

- (void) OnTouchHelpBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
    
    ScanQRViewController *qrVC = [[ScanQRViewController alloc] initWithNibName:@"ScanQRViewController" bundle: nil];
    
    [self.navigationController pushViewController: qrVC animated: YES];
}

#pragma mark - Touch Actions

- (IBAction)onTouchChooseSourceBtn:(id)sender {
    ChooseCategoryViewController *chooseCatVC = [[ChooseCategoryViewController alloc] initWithNibName:@"ChooseCategoryViewController" bundle: nil];
    
    [self.navigationController pushViewController: chooseCatVC animated: YES];
}

- (IBAction)onTouchScanQRBtn:(id)sender {
    if ([[DataKeeper sharedInstance] m_bDontShowAgainHelp]) {
        DataKeeper *dataKeeper = [DataKeeper sharedInstance];
        dataKeeper.qrcodeDelegate = self;
        [dataKeeper onScanQRCode: self];
    } else {
        ScanQRViewController *qrVC = [[ScanQRViewController alloc] initWithNibName:@"ScanQRViewController" bundle: nil];
        
        [self.navigationController pushViewController: qrVC animated: YES];
    }
}

- (IBAction)onTouchChooseDestinationBtn:(id)sender {
    
    if ([[DataKeeper sharedInstance] source] == nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Meander" message: [[DataKeeper sharedInstance] getTextByID: 51] delegate:nil cancelButtonTitle:[[DataKeeper sharedInstance] getTextByID: 52] otherButtonTitles:nil, nil];
        [alertView show];
    } else {
    
        ChooseCategoryViewController *chooseCatVC = [[ChooseCategoryViewController alloc] initWithNibName:@"ChooseCategoryViewController" bundle: nil];
        
        chooseCatVC.m_bDest = YES;
        
        [self.navigationController pushViewController: chooseCatVC animated: YES];
    }
}

- (IBAction)onTouchStartBtn:(id)sender {
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    if (dataKeeper.source == nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Meander" message:[dataKeeper getTextByID: 53] delegate:nil cancelButtonTitle:[dataKeeper getTextByID: 52] otherButtonTitles:nil, nil];
        
        [alertView show];
    } else if (dataKeeper.destination == nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Meander" message:[dataKeeper getTextByID: 54] delegate:nil cancelButtonTitle:[dataKeeper getTextByID: 52] otherButtonTitles:nil, nil];
        
        [alertView show];
    } else {
        [dataKeeper ResetPathFrom: dataKeeper.source ToNode: dataKeeper.destination];
        if (dataKeeper.m_bDontShowAgainHelp) {
            NavViewController *navVC = [[NavViewController alloc] initWithNibName:@"NavViewController" bundle: nil];
            
            [self.navigationController pushViewController: navVC animated: YES];
        } else {
            HelperViewController *helperVC = (ISiPhone5) ? [[HelperViewController alloc] initWithNibName:@"HelperViewController" bundle: nil] : [[HelperViewController alloc] initWithNibName:@"HelperViewController-iPhone4" bundle: nil];
            
            [self.navigationController pushViewController: helperVC animated: YES];
        }

    }

}

@end
