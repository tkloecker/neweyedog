//
//  ChooseLocationViewController.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "ChooseLocationViewController.h"

#import "ScanQRViewController.h"

@interface ChooseLocationViewController ()

@property (nonatomic, weak) IBOutlet UIButton *scanBtn;
@end

@implementation ChooseLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    m_arrayFiltered = [[NSMutableArray alloc] init];
    
    //[_m_labelNavTitle setText: self.title];

    [self reloadLocations: nil];
}

- (void) viewWillAppear:(BOOL)animated {
    [self SetLabels];
    
    if (_m_bDest == YES) {
        
    } else {
        
        [_m_viewSortBar setHidden: YES];
        
        CGRect rect = _m_viewSortBar.frame;
        rect.origin.y += rect.size.height;
        _m_viewSortBar.frame = rect;
        
        [_m_tableView setFrame: CGRectMake(_m_tableView.frame.origin.x, _m_tableView.frame.origin.y, _m_tableView.frame.size.width, _m_tableView.frame.size.height + _m_viewSortBar.frame.size.height)];
    }
}

- (void)shvcConfigureAppearence
{
    [super shvcConfigureAppearence];
    
    self.m_SearchBar.barTintColor = [UIColor buttonsBackgroundColor];
}

- (void) SetLabels {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    if (_m_bDest == YES) {
        [self.m_labelNavTitle setText: [dataKeeper getTextByID: 56]];
        [_m_labelSortListBy setText: [dataKeeper getTextByID: 63]];
        [_m_btnSortDistance setTitle:[dataKeeper getTextByID: 64] forState:UIControlStateNormal];
        [_m_btnAlphabetSort setTitle:[dataKeeper getTextByID: 65] forState:UIControlStateNormal];

    } else {
        [self.m_labelNavTitle setText: [dataKeeper getTextByID: 55]];
    }
    
    [_m_SearchBar setPlaceholder: [dataKeeper getTextByID: 57]];
    [self.m_navBackBtn setTitle:[dataKeeper getTextByID:62] forState:UIControlStateNormal];


}

- (void) reloadLocations: (NSString *) strKeyword {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [m_arrayFiltered removeAllObjects];
    
    NSArray *arrayTemp;
    
    if (_m_bDest == YES) {
        
        if (_m_btnAlphabetSort.selected) {
            //arrayTemp = [dataKeeper getAlphabeticallyOrderedDestinationsForCategory: _m_nCat];
            arrayTemp = [dataKeeper getAlhpabeticallyOrderedDestinationListWithSourceNodeId:[dataKeeper.source getId] ForCategory:_m_nCat];

        } else {
            arrayTemp = [dataKeeper getDistanceOrderedDestinationListWithSourceNodeId:[dataKeeper.source getId] ForCategory:_m_nCat];
        }
        //arrayTemp =
        //dataKeeper.destinations;
    } else {
        arrayTemp = [dataKeeper getSourcesForCategory: _m_nCat];
        //dataKeeper.sources;
    }

    
    if (strKeyword == nil || [strKeyword length] <= 0) {
        [m_arrayFiltered addObjectsFromArray: arrayTemp];
    } else {
       
        for (NodeListItem* nodeItem in arrayTemp) {
            
            if ([[[nodeItem getTitle] uppercaseString] rangeOfString: strKeyword.uppercaseString].location != NSNotFound) {
                [m_arrayFiltered addObject:nodeItem];
            }
        }
    }
    
    [_m_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self reloadLocations: searchText];
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize maximumLabelSize = CGSizeMake(268, FLT_MAX);
    
    NodeListItem* node = [m_arrayFiltered objectAtIndex: indexPath.row];
    
    
    NSString *strTitle = [node getTitle];
    NSString *strSubTitle = [node getSubTitle];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", strTitle, strSubTitle]];
    
    [string addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize: 17.0f] range:NSMakeRange(0, [string length])];
    
    CGSize expectedLabelSize = [string boundingRectWithSize:maximumLabelSize
                                                         options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil].size;
    
    return round(expectedLabelSize.height) + 23;
    
    
    //return 80.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [m_arrayFiltered count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSBundle mainBundle] loadNibNamed:@"ChooseLocationCell" owner:self options: nil];
    
    NodeListItem* node = [m_arrayFiltered objectAtIndex: indexPath.row];
    
    [_m_chooseLocationCell setBackgroundColor: [UIColor clearColor]];
    
    NSString *strTitle = [node getTitle];
    NSString *strSubTitle = [node getSubTitle];
    
    if (_m_bDest == NO) {
        strSubTitle = @"";
    }
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", strTitle, strSubTitle]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [strTitle length])];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange([strTitle length] + 1, [strSubTitle length])];
    [string addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize: 17.0f] range:NSMakeRange(0, [string length])];


    _m_chooseLocationCell.m_labelTitle.attributedText = string;
    
    CGSize maximumLabelSize = CGSizeMake(268, FLT_MAX);

    
    CGSize expectedLabelSize = [string boundingRectWithSize:maximumLabelSize options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin context:nil].size;
    
    [_m_chooseLocationCell.m_labelTitle setFrame: CGRectMake(35, 0, 268, expectedLabelSize.height + 23)];

    
  //  _m_chooseLocationCell.m_labelTitle.text = [node getTitle];
    
    if (indexPath.row < [m_arrayFiltered count] - 1) {
        [_m_chooseLocationCell.m_viewBottomLine setHidden: YES];
    }
    
    if (_m_bDest && [node isFeatured]) {
        [_m_chooseLocationCell.m_featuredIcon setHidden: NO];
        [_m_chooseLocationCell.m_locationIcon setHidden: YES];
    } else {
        [_m_chooseLocationCell.m_featuredIcon setHidden: YES];
        [_m_chooseLocationCell.m_locationIcon setHidden: NO];
        
    }
    
    return _m_chooseLocationCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    NodeListItem* node = [m_arrayFiltered objectAtIndex: indexPath.row];

    
    if (_m_bDest == NO) {
        
        Node* srccand = [dataKeeper getNodeFromId: [node getNodeId]];

        
        if ([dataKeeper.destination getId] == [srccand getId]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Meander" message:@"Please choose different location with destination." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
            return;
            
        }
        
        [dataKeeper setSource: [dataKeeper getNodeFromId: [node getNodeId]]];
    } else {
        
        Node* destcand = [dataKeeper getNodeFromId: [node getNodeId]];
        
        if ([dataKeeper.source getId] == [destcand getId]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Meander" message:@"Please choose different location with starting point." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
            return;
        }
        
        [dataKeeper setDestination: destcand];
    }

    [self.navigationController popToRootViewControllerAnimated: YES];
}

#pragma mark - QRCode delegate
- (void) SuccessToReadQRCode {
    NSLog(@"Success");
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    NSLog(@"QRCode = %@", dataKeeper.m_strQRCode);
    
    [dataKeeper setSource: [dataKeeper getNodeFromId: (int)[dataKeeper.m_strQRCode integerValue]]];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void) OnTouchBackBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
}

- (void) OnTouchHelpBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
}

#pragma mark - Touch Actions

- (IBAction)onTouchNavBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)onTouchSortbyAlphabet:(id)sender {
    if (_m_btnAlphabetSort.selected == NO) {
        _m_btnAlphabetSort.selected = YES;
        _m_btnSortDistance.selected = NO;
    }
    
    [self reloadLocations: _m_SearchBar.text];
}

- (IBAction)onTouchSortByDistance:(id)sender {
    if (_m_btnSortDistance.selected == NO) {
        _m_btnAlphabetSort.selected = NO;
        _m_btnSortDistance.selected = YES;
    }
    
    [self reloadLocations: _m_SearchBar.text];
}

- (IBAction)onScanButtonTouch:(id)sender
{
    if ([[DataKeeper sharedInstance] m_bDontShowAgainHelp]) {
        DataKeeper *dataKeeper = [DataKeeper sharedInstance];
        dataKeeper.qrcodeDelegate = self;
        [dataKeeper onScanQRCode: self];
    } else {
        ScanQRViewController *qrVC = [[ScanQRViewController alloc] initWithNibName:@"ScanQRViewController" bundle: nil];
        
        [self.navigationController pushViewController: qrVC animated: YES];
    }
}
@end
