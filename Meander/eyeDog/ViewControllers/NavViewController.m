//
//  NavViewController.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "NavViewController.h"
#import "Picture.h"

#import "MenuOverlayViewController.h"
#import "ScanQRViewController.h"
#import "FoundedLocationViewController.h"

@interface NavViewController ()<MenuOverlayDelegate, ScanQRViewControllerDelegate>

@property (nonatomic, strong) MenuOverlayViewController *alertOverlay;

@property (nonatomic, weak) IBOutlet UIView *bottomBarView;
@property (nonatomic, weak) IBOutlet UILabel *hintLbl;
@end

@implementation NavViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationController setNavigationBarHidden: YES];
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [_m_btnStop setTitle:[dataKeeper getTextByID: 61] forState:UIControlStateNormal];
    
    [_m_scrollView setContentInset: UIEdgeInsetsMake(-20, 0, 0, 0)];
    [_m_scrollView setContentSize: CGSizeMake(320 * ([dataKeeper.locations count]), 0)];
    
    for (NSInteger nIndex = 0; nIndex < [dataKeeper.locations count]; nIndex++) {
        NSDictionary *dictItem = [dataKeeper.locations objectAtIndex: nIndex];
        
        NSLog(@"Index (%ld) = %@", (long)nIndex, dictItem);
        
        Picture* pic = [dictItem objectForKey: @"Picture"];
        UIImage* image;
        
        if ([pic picIsResource]){
            NSLog(@"resource");
            NSString* picFileName = [pic getFileName];
            image = [UIImage imageNamed: picFileName];
        } else {
            NSString* path = [pic getPath];
            if ([pic picIsHTTP]){
                NSLog(@"loading from server");
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: path]]];
            } else {
                NSLog(@"loading from documents folder");
                image = [UIImage imageWithContentsOfFile: path];
            }
        }
        
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
        
        [imageView setFrame: CGRectMake(320 * nIndex, 0, 320, 427)];
        
        [_m_scrollView addSubview: imageView];
    }
    
    m_arrayArrows = [[NSMutableArray alloc] init];
    
    for (NSInteger i = 0; i <= 24; i++) {
        [m_arrayArrows addObject: [NSString stringWithFormat:@"arrow1_00%02d.png", i]];
    }
    _m_directionArrowView.delegate = self;
    _m_directionArrowView.m_bShown = YES;
    [self LoadStep: 0];
    
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressNavigationView)];
    recognizer.numberOfTouchesRequired = 2;
    recognizer.minimumPressDuration = 5;
    [self.navigationView addGestureRecognizer:recognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MenuOverlayViewController *)alertOverlay
{
    if (!_alertOverlay) {
        _alertOverlay = [MenuOverlayViewController new];
        _alertOverlay.delegate = self;
        _alertOverlay.view.frame = self.view.bounds;
    }
    return _alertOverlay;
}

- (void) LoadStep: (NSInteger) nStep {
    
    m_nCurStep = nStep;
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    if (nStep == [dataKeeper.locations count] - 1) {
        _m_labelDirection.numberOfLines = 2;
        [_m_labelDirection setTextAlignment: NSTextAlignmentCenter];
        [_m_labelDirection setText:[NSString stringWithFormat:@"%@", [dataKeeper getTextByID:11]]];
        
        [_m_labelDirection sizeToFit];
        
        CGRect frame = _m_labelDirection.frame;
        frame.origin.y = (self.bottomBarView.bounds.size.height - frame.size.height) / 2;
        frame.size.width = 246;
        _m_labelDirection.frame = frame;
        
        [_m_labelStep setText:@""];
        [_m_labelDistance setText:@"0m"];

        [_m_imgViewDirection setHidden: YES];
        [_m_imgViewFinished setHidden: NO];
        
        NSString *strDest = [dataKeeper getTextByID:12];
        
        
        
        [_m_labelDistination setText: strDest];
        
        CGSize maximumLabelSize = CGSizeMake(270, FLT_MAX);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:_m_labelDistination.font forKey: NSFontAttributeName];
        
        CGSize expectedLabelSize = [strDest boundingRectWithSize:maximumLabelSize
                                                         options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:stringAttributes context:nil].size;
        
        [_m_labelDistination setFrame: CGRectMake(20, 21, 270, round(expectedLabelSize.height) + 10)];
        
        [_m_imgViewPanel setFrame: CGRectMake(_m_imgViewPanel.frame.origin.x, _m_imgViewPanel.frame.origin.y, _m_imgViewPanel.frame.size.width, round(expectedLabelSize.height) + 37)];
        
        [_m_labelDistance setFrame: CGRectMake(_m_labelDistance.frame.origin.x, round(expectedLabelSize.height) + 22, _m_labelDistance.frame.size.width, _m_labelDistance.frame.size.height)];

        [_m_viewGraphFrame setHidden: YES];
        
    } else {
        
        [_m_directionArrowView setHidden: NO];

    
        NSDictionary *dictItem = [dataKeeper.locations objectAtIndex: nStep];
        
        // Guide View
        NSString *strConnectionText = [dictItem objectForKey:@"connectionText"];
        if (strConnectionText != nil && [strConnectionText isKindOfClass:[NSNull class]] == NO && [strConnectionText length] > 0) {
            [_m_viewGuideFrame setHidden: NO];
            [_m_labelGuide setText: [dataKeeper getTextByID: [strConnectionText integerValue]]];
            
        } else {
            [_m_viewGuideFrame setHidden: YES];
        }
        
        // Graph View
        if ([[dictItem objectForKey:@"showTopView"] boolValue] == YES) {
            [_m_viewGraphFrame setHidden: NO];
        } else {
            [_m_viewGraphFrame setHidden: YES];
        }
        
        [_m_labelDirection setTextAlignment: NSTextAlignmentLeft];
        [_m_labelDirection setTextColor: [UIColor whiteColor]];

        NSString *strDirection = [dictItem objectForKey:@"Direction"];
        NSNumber *nAngle = [dictItem objectForKey:@"Angle"];
        int levelChange = [[dictItem objectForKey: @"levelChange"] intValue];
        
        _m_labelDirection.numberOfLines = 1;
        [_m_labelDirection setText: strDirection];
        [_m_labelDirection sizeToFit];
        
        CGRect frame = _m_labelDirection.frame;
        frame.origin = CGPointMake(12, 8);
        _m_labelDirection.frame = frame;
        
        if ([dataKeeper.m_strLanguage isEqualToString:@"Chinese"]) {
            [_m_labelStep setText: [NSString stringWithFormat:@"%@ %ld %@ %d", [dataKeeper getTextByID:13], (unsigned long)[dataKeeper.locations count], [dataKeeper getTextByID:14], nStep + 1]];
        } else {
            [_m_labelStep setText: [NSString stringWithFormat:@"%@ %d %@ %ld", [dataKeeper getTextByID:13], nStep + 1, [dataKeeper getTextByID:14], (unsigned long)[dataKeeper.locations count]]];
        }
        
//        [_m_labelDistination setFrame: CGRectMake(19, 71, 174, 30)];
        
        NSString *strDest = [NSString stringWithFormat:@"%@ (%@)", [dataKeeper.destination getName], [dataKeeper.destination getZone]];
        
        
        [_m_labelDistination setText: strDest];
        
        CGSize maximumLabelSize = CGSizeMake(270, FLT_MAX);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:_m_labelDistination.font forKey: NSFontAttributeName];
        
        CGSize expectedLabelSize = [strDest boundingRectWithSize:maximumLabelSize
                                                             options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:stringAttributes context:nil].size;
        
        [_m_labelDistination setFrame: CGRectMake(20, 21, 270, round(expectedLabelSize.height) + 10)];
        
        [_m_imgViewPanel setFrame: CGRectMake(_m_imgViewPanel.frame.origin.x, _m_imgViewPanel.frame.origin.y, _m_imgViewPanel.frame.size.width, round(expectedLabelSize.height) + 37)];
        
        [_m_labelDistance setFrame: CGRectMake(_m_labelDistance.frame.origin.x, round(expectedLabelSize.height) + 22, _m_labelDistance.frame.size.width, _m_labelDistance.frame.size.height)];

        NSNumber *distance = [dictItem objectForKey: @"Distance"];
        [_m_labelDistance setText: [NSString stringWithFormat:@"%.0fm", [distance doubleValue]]];
 //       [_m_labelDistance setFrame: CGRectMake(148, 93, 99, 28)];

        NSString *strTime;
        NSInteger nTotalSeconds = (int)[distance doubleValue] / (int)walkingSpeedInMetersPerSecond;
        
        /*if (nTotalSeconds >= 60) {
            if (nTotalSeconds % 60 == 0) {
                strTime = [NSString stringWithFormat:@"%dmin", nTotalSeconds
                                        / 60];
                
            } else {
                strTime = [NSString stringWithFormat:@"%dmin %d sec", nTotalSeconds
                                / 60, nTotalSeconds % 60];
            }
        } else {
            strTime = [NSString stringWithFormat:@"%d sec", nTotalSeconds % 60];
        }*/
        
        strTime = [Util formattedSecondsString: nTotalSeconds];
        
        [_m_labelDistance setText: [NSString stringWithFormat:@"%.0fm  %@", [distance doubleValue], strTime]];
        
        if (levelChange == 0 && nStep < [dataKeeper.locations count] - 1) {
            
            NSDictionary *nextItem;
            
            if (nStep == 0) {
                nextItem = [dataKeeper.locations objectAtIndex: nStep];
            } else {
                nextItem = [dataKeeper.locations objectAtIndex: nStep];

            }

            [_m_directionArrowView SetDirectionArrowsWithPrevious:[[nextItem objectForKey:@"previd"] integerValue] current:[[nextItem objectForKey:@"nodeid"] integerValue] next:[[nextItem objectForKey:@"nextid"] integerValue] step:[[nextItem objectForKey:@"curstep"] integerValue] + 1 nTotal:[[nextItem objectForKey:@"totalstep"] integerValue]];
            
            NSLog(@"m_bShown = %d", _m_directionArrowView.m_bShown);
        } else {
            //_m_directionArrowView.m_bShown = NO;
            [_m_viewGraphFrame setHidden: YES];
        }
        
        //if ([strDirection isEqualToString:@"Go up"]) {
        if (levelChange > 0) {
            [_m_imgViewDirection setImage: [UIImage imageNamed:@"up.png"]];
        //} else if ([strDirection isEqualToString:@"Go down"]) {
        } else if (levelChange < 0) {
            [_m_imgViewDirection setImage: [UIImage imageNamed:@"down.png"]];
        } else if ([strDirection isEqualToString: [dataKeeper getTextByID: 4]]) {
            [_m_imgViewDirection setImage: [UIImage imageNamed:@"arrow1_0012.png"]];
        } else {
            NSInteger nIndex = (int) (([nAngle doubleValue] * -1 + 120) / 10);
            if (nIndex < 0) {
                nIndex = 0;
            }
            if (nIndex >= [m_arrayArrows count]) {
                nIndex = [m_arrayArrows count] - 1;
            }
            
            NSLog(@"current = %@", [m_arrayArrows objectAtIndex: nIndex]);
            
            [_m_imgViewDirection setImage: [UIImage imageNamed: [m_arrayArrows objectAtIndex: nIndex]]];
            
        }
        
        [_m_imgViewDirection setHidden: NO];
        [_m_imgViewBotton setHidden: NO];
        [_m_imgViewFinished setHidden: YES];
        
    }

    NSDictionary *item = [dataKeeper.locations objectAtIndex: nStep];
    Picture *picture = item[@"Picture"];
    self.hintLbl.text = [picture getFileName];
    
    CGRect frame = self.navigationView.frame;
    frame.size.height = _m_labelDistance.frame.origin.y + _m_labelDistance.frame.size.height + 3;
    
    self.navigationView.frame = frame;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSInteger nPageCnt = (scrollView.contentOffset.x / 320);
    
    if (m_nCurStep != nPageCnt) {
        [self LoadStep: nPageCnt];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    NSInteger nPageCnt = (scrollView.contentOffset.x / 320);
    
//    [self LoadStep: nPageCnt];
    
}

#pragma mark - Touch Actions

- (IBAction)onTouchNavBackBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated: YES]                                                                                                                             ;
}

- (IBAction)onTouchCloseBtn:(id)sender {
    /*
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Meander" message:[[DataKeeper sharedInstance] getTextByID: 15] delegate:self cancelButtonTitle:[[DataKeeper sharedInstance] getTextByID: 17] otherButtonTitles:[[DataKeeper sharedInstance] getTextByID: 16], nil];
    
    [alertView show];
    */
    [self.view addSubview:self.alertOverlay.view];
}

- (IBAction)onTouchGraphOpenBtn:(id)sender {
    _m_btnGraphOpen.selected = !_m_btnGraphOpen.selected;
    
    if (_m_btnGraphOpen.selected == YES) {
        [UIView animateWithDuration:0.5f animations:^(void) {
            [_m_viewGraphFrame setFrame: CGRectMake(300, 120, 94, 104)];
        }];
    } else {
        [UIView animateWithDuration:0.5f animations:^(void) {
            [_m_viewGraphFrame setFrame: CGRectMake(230, 120, 94, 104)];
        }];
    }
}

- (IBAction)onTouchGuideBtn:(id)sender {
    _m_btnGuide.selected = !_m_btnGuide.selected;
    
    if (_m_btnGuide.selected == YES) {
        [UIView animateWithDuration:0.5f animations:^(void) {
            [_m_viewGuideFrame setFrame: CGRectMake(304, self.view.frame.size.height - 168, 233, 66)];
        }];
    } else {
        [UIView animateWithDuration:0.5f animations:^(void) {
            [_m_viewGuideFrame setFrame: CGRectMake(84, self.view.frame.size.height - 168, 233, 66)];
        }];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self.navigationController popToRootViewControllerAnimated: YES];
    }
}

#pragma mark - DirectionArrowViewDelegate

- (void) GraphDrawn:(BOOL)bShown {
    if (bShown == YES) {
        //[_m_viewGraphFrame setHidden: NO];
    } else {
        //[_m_viewGraphFrame setHidden: YES];
    }

}

#pragma mark - QRCode delegate

// When Success to read QR Code
- (void) SuccessToReadQRCode {
    NSLog(@"Success");
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    NSLog(@"QRCode = %@", dataKeeper.m_strQRCode);
    
    [dataKeeper setSource: [dataKeeper getNodeFromId: (int)[dataKeeper.m_strQRCode integerValue]]];
}

- (void) OnTouchBackBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
}

- (void) OnTouchHelpBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
    
    ScanQRViewController *qrVC = [[ScanQRViewController alloc] initWithNibName:@"ScanQRViewController" bundle: nil];
    
    qrVC.delegate = self;
    
    [self.navigationController pushViewController: qrVC animated: YES];
}

- (void)scanQRControllerFinishedScan:(ScanQRViewController *)controller
{
    FoundedLocationViewController *flvc = [[FoundedLocationViewController alloc] init];
    
    NSLog(@"%@", self.navigationController.viewControllers);
    
    NSArray *array = @[self.navigationController.viewControllers[0], flvc];
    [self.navigationController setViewControllers:array animated:YES];
    
}

#pragma mark - MenuOverlayDelegate
- (void)dismissedMenuOverlay:(MenuOverlayViewController *)overlay
{
}

- (void)abortedMenuOverlay:(MenuOverlayViewController *)overlay
{
    [self.navigationController popToRootViewControllerAnimated: YES];
}

- (void)scanRequestedFromMenuOverlay:(MenuOverlayViewController *)overlay
{
    if ([[DataKeeper sharedInstance] m_bDontShowAgainHelp]) {
        DataKeeper *dataKeeper = [DataKeeper sharedInstance];
        dataKeeper.qrcodeDelegate = self;
        [dataKeeper onScanQRCode: self];
    } else {
        ScanQRViewController *qrVC = [[ScanQRViewController alloc] initWithNibName:@"ScanQRViewController" bundle: nil];
        
        qrVC.delegate = self;
        
        [self.navigationController pushViewController: qrVC animated: YES];
    }
}

#pragma mark - Hint
- (void)longPressNavigationView
{
    self.hintLbl.hidden = NO;
}
@end
