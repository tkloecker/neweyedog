//
//  ScanQRViewController.m
//  Meander
//
//  Created by Wony on 2/6/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "ScanQRViewController.h"

@interface ScanQRViewController ()

@property (nonatomic, weak) IBOutlet UISwitch *m_Switch;

@end

@implementation ScanQRViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self SetLabels];
}

- (void) SetLabels {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [self.m_labelNavTitle setText: [dataKeeper getTextByID: 29]];
    [_m_labelDesc1 setText: [dataKeeper getTextByID: 30]];
    [_m_labelDesc2 setText: [dataKeeper getTextByID: 31]];

    self.m_Switch.on = [[DataKeeper sharedInstance] m_bDontShowAgainHelp];
}

#pragma mark - QRCode delegate

// When Success to read QR Code
- (void) SuccessToReadQRCode {
    NSLog(@"Success");
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    NSLog(@"QRCode = %@", dataKeeper.m_strQRCode);
    
    [dataKeeper setSource: [dataKeeper getNodeFromId: (int)[dataKeeper.m_strQRCode integerValue]]];
    
    if (self.delegate) {
        [self.delegate scanQRControllerFinishedScan:self];
    } else {
        [self.navigationController popViewControllerAnimated: NO];
    }
}

- (void) OnTouchBackBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
    [self.navigationController popViewControllerAnimated: NO];
}

- (void) OnTouchHelpBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
}

#pragma mark - Touch Actions

- (IBAction)onTouchOKBtn:(id)sender {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    dataKeeper.qrcodeDelegate = self;
    [dataKeeper onScanQRCode: self];
}

- (IBAction)onSwitchChanged:(id)sender
{
    [[DataKeeper sharedInstance] setM_bDontShowAgainHelp: _m_Switch.on];
}
@end
