//
//  ChooseLocationViewController.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseLocationCell.h"

#import "SchemeHolderViewController.h"

@interface ChooseLocationViewController : SchemeHolderViewController {
    NSMutableArray *m_arrayFiltered;
}

@property (weak, nonatomic) IBOutlet UILabel *m_labelNavTitle;

@property (nonatomic) BOOL m_bDest;
@property (nonatomic) NSInteger m_nCat;

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

@property (weak, nonatomic) IBOutlet UIButton *m_btnAlphabetSort;
@property (weak, nonatomic) IBOutlet UIButton *m_btnSortDistance;
@property (weak, nonatomic) IBOutlet UISearchBar *m_SearchBar;
@property (weak, nonatomic) IBOutlet UIView *m_viewSortBar;

@property (weak, nonatomic) IBOutlet UIButton *m_navBackBtn;

@property (weak, nonatomic) IBOutlet UILabel *m_labelSortListBy;


@property (strong, nonatomic) IBOutlet ChooseLocationCell *m_chooseLocationCell;


- (IBAction)onTouchNavBackBtn:(id)sender;
- (IBAction)onTouchSortbyAlphabet:(id)sender;
- (IBAction)onTouchSortByDistance:(id)sender;


@end
