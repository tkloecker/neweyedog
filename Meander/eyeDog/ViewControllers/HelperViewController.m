//
//  HelperViewController.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "HelperViewController.h"
#import "NavViewController.h"

@interface HelperViewController ()

@property (nonatomic, weak) IBOutlet UIView *helpHolder;

@property (nonatomic, weak) IBOutlet UIView *page1;
@property (nonatomic, weak) IBOutlet UIView *page2;

@property (nonatomic, strong) NSArray *pages;

@property (nonatomic) NSInteger activePage;
@end

@implementation HelperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.activePage = -1;
    self.pages = @[self.page1, self.page2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [self SetLabels];
    [self loadNextPage];
}

- (void) SetLabels {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [self.m_labelNavTitle setText: [dataKeeper getTextByID: 59]];
    [_m_labelGuide setText: [dataKeeper getTextByID: 32]];
    [_m_labelDontShowAgain setText: [dataKeeper getTextByID:33]];
    [self.m_navBackBtn setTitle:[dataKeeper getTextByID:62] forState:UIControlStateNormal];
    
    _m_Switch.on = [[DataKeeper sharedInstance] m_bDontShowAgainHelp];
    //_m_labelNavTitle setText: [dataKeeper getTextByID:
}

#pragma mark - paging logik
- (void)loadNextPage
{
    self.activePage++;

    UIView *nextPage = self.pages[self.activePage];
    
    [self.helpHolder addSubview:nextPage];
    
    CGRect rect = self.helpHolder.bounds;
    if (self.activePage != 0) {
        rect.origin.x += rect.size.width;
    }
    nextPage.frame = rect;
    
    if (self.activePage != 0) {
        UIView *prevPage = self.pages[self.activePage - 1];
        [UIView animateWithDuration:0.5 animations:^{
            CGRect frame = self.helpHolder.bounds;
            
            nextPage.frame = frame;
            
            frame.origin.x -= frame.size.width;
            prevPage.frame = frame;
        } completion:^(BOOL finished) {
            [prevPage removeFromSuperview];
        }];
    }
}

- (void)loadPrevPage
{
    self.activePage--;
    
    UIView *nextPage = self.pages[self.activePage];
    
    [self.helpHolder addSubview:nextPage];
    
    CGRect rect = self.helpHolder.bounds;
    rect.origin.x -= rect.size.width;
    nextPage.frame = rect;
    
    UIView *prevPage = self.pages[self.activePage + 1];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.helpHolder.bounds;
        
        nextPage.frame = frame;
        
        frame.origin.x += frame.size.width;
        prevPage.frame = frame;
    } completion:^(BOOL finished) {
        [prevPage removeFromSuperview];
    }];
}

#pragma mark - Touch Actions

- (IBAction)onTouchOkBtn:(id)sender {
    
    if (self.activePage == [self.pages count] - 1) {
        [[DataKeeper sharedInstance] saveDataToFile];
        
        NavViewController *navVC = [[NavViewController alloc] initWithNibName:@"NavViewController" bundle: nil];
        
        [self.navigationController pushViewController: navVC animated: YES];
    } else {
        [self loadNextPage];
    }
}

- (IBAction)onTouchNavBackBtn:(id)sender {
    if (self.activePage == 0) {
        [self.navigationController popViewControllerAnimated: YES];
    } else {
        [self loadPrevPage];
    }
}

- (IBAction)onSwitchChanged:(id)sender {
    [[DataKeeper sharedInstance] setM_bDontShowAgainHelp: _m_Switch.on];
}
@end
