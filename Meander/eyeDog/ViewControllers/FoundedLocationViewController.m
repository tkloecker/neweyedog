//
//  FoundedLocationViewController.m
//  Meander
//
//  Created by HereTrix on 21.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "FoundedLocationViewController.h"

#import "NavViewController.h"

@interface FoundedLocationViewController ()

@property (nonatomic, weak) IBOutlet UILabel *sourceTextLbl;
@property (nonatomic, weak) IBOutlet UILabel *destTextLbl;
@end

@implementation FoundedLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setLabels];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLabels
{
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    self.sourceTextLbl.text = [NSString stringWithFormat:@"%@", [dataKeeper.source getName]];
    self.destTextLbl.text = [NSString stringWithFormat:@"%@", [dataKeeper.destination getName]];
}

#pragma mark - Actions
- (IBAction)continueBtnTouched:(id)sender
{
    NavViewController *navVc = [[NavViewController alloc] init];
    [self.navigationController pushViewController:navVc animated:YES];
}

- (IBAction)abortBtnTouched:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end