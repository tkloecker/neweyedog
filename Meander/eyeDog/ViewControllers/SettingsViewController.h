//
//  SettingsViewController.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingCell.h"

#import "SchemeHolderViewController.h"

@interface SettingsViewController : SchemeHolderViewController {
    NSArray *m_arraySettingNames;
}

@property (strong, nonatomic) IBOutlet SettingCell *m_SettingCell;
@property (weak, nonatomic) IBOutlet UITableView *m_tableView;


@end
