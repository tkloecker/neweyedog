//
//  MenuOverlayViewController.m
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "MenuOverlayViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "UIColor+SchemeManager.h"
#import "UIImage+SchemeManager.h"

@interface MenuOverlayViewController ()

@property (nonatomic, weak) IBOutlet UIButton *scannerButton;
@property (nonatomic, weak) IBOutlet UIButton *abortButton;
@property (nonatomic, weak) IBOutlet UIButton *dismissButton;
@end

@implementation MenuOverlayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self movcConfigureArrearence];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)movcConfigureArrearence
{
    self.view.backgroundColor = [UIColor overlayColor];
    
    [self.scannerButton setBackgroundImage:[UIImage overlayScanButtonBackground] forState:UIControlStateNormal];
    [self.scannerButton setTitleColor:[UIColor buttonsTextColor] forState:UIControlStateNormal];
    [self.scannerButton setTitle:@"Where am I?" forState:UIControlStateNormal];
    
    [self.abortButton setBackgroundImage:[UIImage overlayAbortButtonBackground] forState:UIControlStateNormal];
    [self.abortButton setTitleColor:[UIColor buttonsTextColor] forState:UIControlStateNormal];
    [self.abortButton setTitle:@"Abort Navigation" forState:UIControlStateNormal];
    
    [self.dismissButton setBackgroundImage:[UIImage overlayCloseButton] forState:UIControlStateNormal];
}
- (IBAction)dismissBtnTouched:(id)sender
{
    [self.view removeFromSuperview];
    [self.delegate dismissedMenuOverlay:self];
}
- (IBAction)scanBtnTouched:(id)sender
{
    [self.delegate scanRequestedFromMenuOverlay:self];
}
- (IBAction)abortBtnTouched:(id)sender
{
    [self.delegate abortedMenuOverlay:self];
}
@end
