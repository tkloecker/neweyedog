//
//  EmployeeSearchViewController.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "EmployeeSearchViewController.h"
#import "HHTabListController.h"


@interface EmployeeSearchViewController ()

@end

@implementation EmployeeSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if ([self isMovingToParentViewController]) {
		HHTabListController *tabListController = [self tabListController];
        
        UIButton *button = tabListController.revealTabListButtonItem;
        
        [_m_viewNavBar addSubview: button];
        
        [button setFrame: CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
		
	}
}

@end
