//
//  MainViewController.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchemeHolderViewController.h"

@interface MainViewController : SchemeHolderViewController

@property (weak, nonatomic) IBOutlet UILabel *m_labelGuide;

@property (weak, nonatomic) IBOutlet UIButton *m_btnPickSource;
@property (weak, nonatomic) IBOutlet UIButton *m_btnScanQR;
@property (weak, nonatomic) IBOutlet UIButton *m_btnPickDest;
@property (weak, nonatomic) IBOutlet UIButton *m_btnStart;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgViewSourceBack;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgViewDestinationBack;



- (IBAction)onTouchChooseSourceBtn:(id)sender;
- (IBAction)onTouchScanQRBtn:(id)sender;
- (IBAction)onTouchChooseDestinationBtn:(id)sender;
- (IBAction)onTouchStartBtn:(id)sender;

@end
