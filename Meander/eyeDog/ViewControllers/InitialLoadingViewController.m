//
//  InitialLoadingViewController.m
//  Meander
//
//  Created by Wony on 3/7/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "InitialLoadingViewController.h"

@interface InitialLoadingViewController ()

@end

@implementation InitialLoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImage *image;
    
    if ISiPhone5 {
        image = [UIImage imageNamed:@"init-568h.png"];
    } else {
        image = [UIImage imageNamed:@"init.png"];
    }
    
    [_m_imageView setImage: image];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
