//
//  ScanQRViewController.h
//  Meander
//
//  Created by Wony on 2/6/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchemeHolderViewController.h"

@protocol ScanQRViewControllerDelegate;

@interface ScanQRViewController : SchemeHolderViewController <QRCodeReaderDelegate>

@property (weak, nonatomic) IBOutlet UILabel *m_labelDesc1;
@property (weak, nonatomic) IBOutlet UILabel *m_labelDesc2;

@property (nonatomic, weak) id<ScanQRViewControllerDelegate> delegate;

- (IBAction)onTouchOKBtn:(id)sender;

@end

@protocol ScanQRViewControllerDelegate <NSObject>

- (void)scanQRControllerFinishedScan:(ScanQRViewController *)controller;

@end