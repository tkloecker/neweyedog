//
//  NotAvailableViewController.h
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchemeHolderViewController.h"

@interface NotAvailableViewController : SchemeHolderViewController

@end
