//
//  ProductSearchViewController.h
//  Meander
//
//  Created by Wony on 2/10/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchemeHolderViewController.h"

@interface ProductSearchViewController : SchemeHolderViewController

@property (weak, nonatomic) IBOutlet UIView *m_viewNavBar;


@end
