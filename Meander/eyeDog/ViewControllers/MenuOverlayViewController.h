//
//  MenuOverlayViewController.h
//  Meander
//
//  Created by HereTrix on 20.05.14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuOverlayDelegate;

@interface MenuOverlayViewController : UIViewController

@property (nonatomic, weak) id<MenuOverlayDelegate> delegate;
@end

@protocol MenuOverlayDelegate <NSObject>

- (void)dismissedMenuOverlay:(MenuOverlayViewController *)overlay;
- (void)abortedMenuOverlay:(MenuOverlayViewController *)overlay;
- (void)scanRequestedFromMenuOverlay:(MenuOverlayViewController *)overlay;

@end