//
//  HelperViewController.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchemeHolderViewController.h"

@interface HelperViewController : SchemeHolderViewController

@property (weak, nonatomic) IBOutlet UISwitch *m_Switch;

@property (weak, nonatomic) IBOutlet UILabel *m_labelGuide;
@property (weak, nonatomic) IBOutlet UILabel *m_labelDontShowAgain;


- (IBAction)onTouchOkBtn:(id)sender;
- (IBAction)onTouchNavBackBtn:(id)sender;
- (IBAction)onSwitchChanged:(id)sender;


@end
