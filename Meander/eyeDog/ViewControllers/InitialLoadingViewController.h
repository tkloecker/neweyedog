//
//  InitialLoadingViewController.h
//  Meander
//
//  Created by Wony on 3/7/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchemeHolderViewController.h"

@interface InitialLoadingViewController : SchemeHolderViewController

@property (weak, nonatomic) IBOutlet UIImageView *m_imageView;


@end
