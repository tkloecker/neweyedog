//
//  ChooseCategoryViewController.m
//  Meander
//
//  Created by Wony on 2/25/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "ChooseCategoryViewController.h"
#import "ChooseLocationViewController.h"
#import "ScanQRViewController.h"


@interface ChooseCategoryViewController ()<QRCodeReaderDelegate>

@property (nonatomic, weak) IBOutlet UIButton *scanBtn;
@end

@implementation ChooseCategoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    m_arrayFiltered = [[NSMutableArray alloc] init];
    
    [self SetLabels];
    [self reloadLocations: nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) SetLabels {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [_m_SearchBar setPlaceholder: [dataKeeper getTextByID: 57]];
    [self.m_labelNavTitle setText: [dataKeeper getTextByID: 58]];
    [self.m_navBackBtn setTitle:[dataKeeper getTextByID:62] forState:UIControlStateNormal];

}

- (void) reloadLocations: (NSString *) strKeyword {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [m_arrayFiltered removeAllObjects];
    
    NSArray *arrayTemp = [dataKeeper getCategories];
    
    if (strKeyword == nil || [strKeyword length] <= 0) {
        [m_arrayFiltered addObjectsFromArray: arrayTemp];
    } else {
        for (CategoryItem* catItem in arrayTemp) {
            
            if ([[[catItem getTitle] uppercaseString] rangeOfString: strKeyword.uppercaseString].location != NSNotFound) {
                [m_arrayFiltered addObject:catItem];
            }
        }
    }
    
    [_m_tableView reloadData];
}


#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self reloadLocations: searchText];
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize maximumLabelSize = CGSizeMake(242, FLT_MAX);
    
    CategoryItem* cat = [m_arrayFiltered objectAtIndex: indexPath.row];

    
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:17.0f] forKey: NSFontAttributeName];
    NSString *interString = [cat getTitle];
    
    CGSize expectedLabelSize = [interString boundingRectWithSize:maximumLabelSize
                                                         options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:stringAttributes context:nil].size;
        
    return round(expectedLabelSize.height) + 23;
    
    
    //return 80.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [m_arrayFiltered count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSBundle mainBundle] loadNibNamed:@"CategoryCell" owner:self options: nil];
    
    CategoryItem* cat = [m_arrayFiltered objectAtIndex: indexPath.row];
    NSString *interString = [cat getTitle];

    [_m_CategoryCell setBackgroundColor: [UIColor clearColor]];
    _m_CategoryCell.m_labelTitle.text = interString;
    
    CGSize maximumLabelSize = CGSizeMake(242, FLT_MAX);

    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:17.0f] forKey: NSFontAttributeName];
    
    CGSize expectedLabelSize = [interString boundingRectWithSize:maximumLabelSize
                                                         options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:stringAttributes context:nil].size;
    
    [_m_CategoryCell.m_labelTitle setFrame: CGRectMake(44, 0, 242, expectedLabelSize.height + 23)];
    
    if (indexPath.row < [m_arrayFiltered count] - 1) {
        [_m_CategoryCell.m_viewBottomLine setHidden: YES];
    }
    
    NSString *strIconName = [NSString stringWithFormat:@"cat%@.png", [cat iconID]];

    [_m_CategoryCell.m_imageViewIcon setImage: [UIImage imageNamed: strIconName]];
    
    return _m_CategoryCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChooseLocationViewController *chooseVC = [[ChooseLocationViewController alloc] initWithNibName:@"ChooseLocationViewController" bundle: nil];
    

    CategoryItem* cat = [m_arrayFiltered objectAtIndex: indexPath.row];

    chooseVC.m_bDest = _m_bDest;
    chooseVC.m_nCat = cat.nID;
    
    [chooseVC setTitle: cat.getTitle];
    
    [self.navigationController pushViewController: chooseVC animated: YES];

}

#pragma mark - QRCode delegate
- (void) SuccessToReadQRCode {
    NSLog(@"Success");
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    NSLog(@"QRCode = %@", dataKeeper.m_strQRCode);
    
    [dataKeeper setSource: [dataKeeper getNodeFromId: (int)[dataKeeper.m_strQRCode integerValue]]];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void) OnTouchBackBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
}

- (void) OnTouchHelpBtn {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper.m_reader dismissViewControllerAnimated:YES completion: nil];
}

#pragma mark - Actions

- (IBAction)onTouchNavBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)onScanButtonTouch:(id)sender
{
    if ([[DataKeeper sharedInstance] m_bDontShowAgainHelp]) {
        DataKeeper *dataKeeper = [DataKeeper sharedInstance];
        dataKeeper.qrcodeDelegate = self;
        [dataKeeper onScanQRCode: self];
    } else {
        ScanQRViewController *qrVC = [[ScanQRViewController alloc] initWithNibName:@"ScanQRViewController" bundle: nil];
        
        [self.navigationController pushViewController: qrVC animated: YES];
    }
}
@end
