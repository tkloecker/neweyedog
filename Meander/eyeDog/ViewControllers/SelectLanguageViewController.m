//
//  SelectLanguageViewController.m
//  Meander
//
//  Created by Wony on 3/7/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "SelectLanguageViewController.h"
#import "AppDelegate.h"
#import "HHTabListController.h"

@interface SelectLanguageViewController ()

@end

@implementation SelectLanguageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    m_arrayLanguages = [[NSMutableArray alloc] init];
    m_arrayLanguagesInEnglish = @[@"English", @"Dutch", @"German", @"French", @"Turkish", @"Danish", @"Swedish", @"Norwegian", @"Spanish", @"Italian", @"Chinese"];
    
    [self SelectLanguage: [[DataKeeper sharedInstance] m_strLanguage]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
                        
- (void) SelectLanguage: (NSString *) language {
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    
    [dataKeeper ChangeLanguage: language];
    
    [self.m_labelNavTitle setText: [dataKeeper getTextByID: 36]];
    [self.m_navBackBtn setTitle:[dataKeeper getTextByID:62] forState:UIControlStateNormal];
    [m_arrayLanguages removeAllObjects];
    
    for (NSInteger nIndex = 37; nIndex <= 47; nIndex++) {
        [m_arrayLanguages addObject: [dataKeeper getTextByID: nIndex]];
    }
    
    [_m_tableView reloadData];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    HHTabListController *tabListVC = (HHTabListController *)appDelegate.viewController;
    
    for (NSInteger nIndex = 0; nIndex < 5; nIndex++) {
        UINavigationController *navVC = (UINavigationController *)[tabListVC.viewControllers objectAtIndex: nIndex];
        
        NSString *title = [dataKeeper getTextByID: 22 + nIndex];
        
        if ([navVC isKindOfClass:[UINavigationController class]]) {
            [[navVC.viewControllers objectAtIndex: 0] setTitle: title];
        } else {
            [navVC setTitle:title];
        }
    }

}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [m_arrayLanguages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell setBackgroundColor: [UIColor clearColor]];
    cell.textLabel.text = [m_arrayLanguages objectAtIndex: indexPath.row];
    [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    
    if ([[m_arrayLanguagesInEnglish objectAtIndex: indexPath.row] isEqualToString: [[DataKeeper sharedInstance] m_strLanguage]]) {
        [cell setAccessoryType: UITableViewCellAccessoryCheckmark];
        [cell setTintColor: [UIColor grayColor]];
    } else {
        [cell setAccessoryType: UITableViewCellAccessoryNone];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *language = [m_arrayLanguagesInEnglish objectAtIndex: indexPath.row];
    [self SelectLanguage: language];
    [[DataKeeper sharedInstance] setM_strLanguage: language];
    
    [[NSUserDefaults standardUserDefaults] setObject: language forKey: @"language"];

}

#pragma mark - Touch Actions

- (IBAction)onTouchNavBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}
@end
