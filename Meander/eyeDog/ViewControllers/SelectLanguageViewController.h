//
//  SelectLanguageViewController.h
//  Meander
//
//  Created by Wony on 3/7/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SchemeHolderViewController.h"

@interface SelectLanguageViewController : SchemeHolderViewController {
    NSMutableArray *m_arrayLanguages;
    NSArray *m_arrayLanguagesInEnglish;
}

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;

- (IBAction)onTouchNavBackBtn:(id)sender;


@end
