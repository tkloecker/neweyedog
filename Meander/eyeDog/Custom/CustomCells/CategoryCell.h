//
//  CategoryCell.h
//  Meander
//
//  Created by Wony on 2/26/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *m_labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *m_imageViewIcon;
@property (weak, nonatomic) IBOutlet UIImageView *m_viewBottomLine;


@end
