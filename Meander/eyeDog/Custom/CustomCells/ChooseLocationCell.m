//
//  ChooseLocationCell.m
//  Meander
//
//  Created by Wony on 2/25/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "ChooseLocationCell.h"

@implementation ChooseLocationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
