//
//  ChooseLocationCell.h
//  Meander
//
//  Created by Wony on 2/25/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseLocationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *m_labelTitle;

@property (weak, nonatomic) IBOutlet UIImageView *m_locationIcon;
@property (weak, nonatomic) IBOutlet UIImageView *m_featuredIcon;
@property (weak, nonatomic) IBOutlet UIImageView *m_viewBottomLine;

@end
