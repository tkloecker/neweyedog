//
//  SettingCell.h
//  Meander
//
//  Created by Wony on 2/10/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *m_labelTitle;
@property (weak, nonatomic) IBOutlet UISwitch *m_switch;


@end
