//
//  HHTabListContainerView.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "HHTabListContainerView.h"

#import "HHTabList.h"

#import <QuartzCore/QuartzCore.h>


@implementation HHTabListContainerView

#pragma mark -
#pragma mark Initialization

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
	if (self) {
		self.clipsToBounds = NO;
		self.backgroundColor = [UIColor clearColor];
		self.opaque = YES;

        UIView *contentView = [[UIView alloc] initWithFrame:[self bounds]];

        [contentView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];

        _contentView = contentView;

        [self addSubview:contentView];
    }
    
    return self;
}


#pragma mark -
#pragma mark Finalization

- (void)dealloc
{
	HH_RELEASE(_contentView);

#if !HH_ARC_ENABLED
    [super dealloc];
#endif
}


#pragma mark -
#pragma mark Accessors

@synthesize contentView = _contentView;


#pragma mark -
#pragma mark Instance methods

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	CALayer *layer = self.layer;
	
	layer.shadowOffset = CGSizeZero;
	layer.shadowOpacity = 0.75f;
	layer.shadowRadius = 10.0f;
	layer.shadowColor = [UIColor blackColor].CGColor;
	layer.shadowPath = [UIBezierPath bezierPathWithRect:layer.bounds].CGPath;
}

@end
