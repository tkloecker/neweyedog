//
//  HHTabListCell.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "HHTabListCell.h"

#import "HHTabList.h"


@interface HHTabListCell ()
{
	UIView	*_selectionBackgroundView;
	UIView	*_separatorLineView;
}

@property (nonatomic, strong) UIView	*selectionBackgroundView;
@property (nonatomic, strong) UIView	*separatorLineView;

@end

@implementation HHTabListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

	if (self) {
		CGRect	frame						= self.bounds;
		UIView	*selectionBackgroundView	= [[UIView alloc] initWithFrame:frame];

		selectionBackgroundView.opaque				= YES;
		selectionBackgroundView.backgroundColor		= [UIColor orangeColor];
		selectionBackgroundView.autoresizingMask	= UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        UIImageView *markView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"menumark"]];
        [markView setFrame: CGRectMake(24, 18, 8, 8)];
        markView.tag = 3555;
        
        [selectionBackgroundView addSubview: markView];

		self.selectionBackgroundView				= selectionBackgroundView;
		self.selectedBackgroundView					= selectionBackgroundView;

		CGRect	lineFrame					= self.bounds;

		lineFrame.origin.y							+= lineFrame.size.height;
		lineFrame.size.height						= 1.0;

		UIView	*separatorLineView			= [[UIView alloc] initWithFrame:lineFrame];

		separatorLineView.opaque					= YES;
		separatorLineView.backgroundColor			= [UIColor orangeColor];
		separatorLineView.autoresizingMask			= UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;

		self.separatorLineView						= separatorLineView;

		[self addSubview:separatorLineView];
        
        _labelText = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 280, 44)];
        
        //[self.labelText setTextColor: [UIColor whiteColor]];
        [self.labelText setFont: [UIFont systemFontOfSize: 18]];
        [self addSubview: _labelText];
	}

	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	// Configure the view for the selected state
    
    if (selected) {
        self.labelText.textColor = [UIColor colorWithRed:0.41 green:0.92 blue:1 alpha:1];
    } else {
        self.labelText.textColor = [UIColor whiteColor];
    }
}

#pragma mark -
#pragma mark Finalization

- (void)dealloc
{
	HH_RELEASE(_selectionBackgroundView);
	HH_RELEASE(_separatorLineView);
	HH_RELEASE(_selectionBackgroundColor);

#if !HH_ARC_ENABLED
	[super dealloc];
#endif
}

#pragma mark -
#pragma mark Accessors

@synthesize selectionBackgroundView		= _selectionBackgroundView;
@synthesize separatorLineView			= _separatorLineView;
@synthesize selectionBackgroundColor	= _selectionBackgroundColor;

- (void)setSelectionBackgroundColor:(UIColor *)selectionBackgroundColor
{
	_selectionBackgroundColor = HH_RETAIN(selectionBackgroundColor);

	[self updateSelectionBackgroundViews];
}

#pragma mark -
#pragma mark Lifecycle

- (void)prepareForReuse
{
	[super prepareForReuse];

	[self updateSelectionBackgroundViews];
}

#pragma mark -
#pragma mark Instance methods

- (void)updateSelectionBackgroundViews
{
	UIColor *selectionBackgroundColor	= self.selectionBackgroundColor;
	UIView	*selectionBackgroundView	= self.selectionBackgroundView;
	UIView	*separatorLineView			= self.separatorLineView;

	selectionBackgroundView.backgroundColor = selectionBackgroundColor;
	separatorLineView.backgroundColor		= selectionBackgroundColor;
    
 //   [_labelText setTextColor: [UIColor colorWithRed:246.0f/255.0f green:228.0f/255.0f blue:214.0f/255.0f alpha:1.0f]];
   
}

@end
