//
//  HHTabListCell.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface HHTabListCell : UITableViewCell

@property (nonatomic, strong) UIColor *selectionBackgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UILabel *labelText;

@end
