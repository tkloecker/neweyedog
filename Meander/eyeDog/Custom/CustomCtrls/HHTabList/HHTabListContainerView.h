//
//  HHTabListContainerView.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HHTabListContainerView : UIView

@property (nonatomic, strong) UIView *contentView;

@end
