//
//  HHTabListTabsView.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "HHTabListTabsView.h"

@implementation HHTabListTabsView

#pragma mark -
#pragma mark Initialization

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame backgroundImage:[UIImage imageNamed:@"darkPattern"]];
}

- (id)initWithFrame:(CGRect)frame backgroundImage:(UIImage *)backgroundImage
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];

    if (self) {
		self.backgroundView = nil;
        
        // darkPattern.png obtained from http://subtlepatterns.com/classy-fabric/
        // Made by Richard Tabor http://www.purtypixels.com/
		self.separatorColor = [UIColor clearColor];
        self.backgroundColor = [UIColor colorWithRed:24.0f/255.0f green:24.0f/255.0f blue:24.0f/255.0f alpha:1.0f];
        //[UIColor colorWithPatternImage:backgroundImage];
    }
    
	return self;
}


#pragma mark -
#pragma mark Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
	
	id<UITableViewDataSource> dataSource = self.dataSource;
	NSUInteger count = [dataSource tableView:self numberOfRowsInSection:0];
	
    self.scrollEnabled = (self.rowHeight * count) > self.bounds.size.height;
}

@end
