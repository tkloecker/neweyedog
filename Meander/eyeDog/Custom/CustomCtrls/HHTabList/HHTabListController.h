//
//  HHTabListController.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol HHTabListControllerDelegate;


@interface HHTabListController : UIViewController

- (id)initWithViewControllers:(NSArray*)viewControllers;
- (id)initWithViewControllers:(NSArray*)viewControllers backgroundImage:(UIImage *)backgroundImage;

@property (nonatomic, copy, readonly) NSArray *viewControllers;
@property (nonatomic, copy, readonly) UIImage *backgroundImage;
@property (nonatomic, assign, getter = isTabListRevealed, readonly) BOOL tabListRevealed;
@property (nonatomic, assign) BOOL containerMayPan;

#if HH_ARC_ENABLED
@property (nonatomic, weak) id<HHTabListControllerDelegate> delegate;
#else
@property (nonatomic, assign) id<HHTabListControllerDelegate> delegate;
#endif


- (UIViewController *)selectedViewController;
- (void)setSelectedViewController:(UIViewController *)newSelectedViewController;
- (void)setSelectedViewController:(UIViewController *)newSelectedViewController animated:(BOOL)animated;

- (void)setTabListRevealed:(BOOL)tabListRevealed animated:(BOOL)animated;

- (UIBarButtonItem*)revealTabListBarButtonItem;
- (UIButton*)revealTabListButtonItem;
- (IBAction)revealTabList:(id)sender;


- (void)goToMainPage;
@end


@protocol HHTabListControllerDelegate <NSObject>

@optional
- (BOOL)tabListController:(HHTabListController*)tabListController shouldSelectViewController:(UIViewController*)viewController;
- (void)tabListController:(HHTabListController*)tabListController willSelectViewController:(UIViewController*)viewController;
- (void)tabListController:(HHTabListController*)tabListController didSelectViewController:(UIViewController*)viewController;

@end


@interface UIViewController (HHTabListController)

- (HHTabListController*)tabListController;

@end
