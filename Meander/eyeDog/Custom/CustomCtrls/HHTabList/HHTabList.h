//
//  HHTabList.h
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import <Foundation/Foundation.h>


#define HH_TAB_LIST_ANIMATION_DURATION		0.4
#define HH_TAB_LIST_WIDTH					(320 - 80)
#define HH_TAB_LIST_TRIGGER_OFFSET			75

#define HH_STATUS_BAR_TINT_HACK_ENABLED		1


#ifndef __has_feature
#define __has_feature(x) 0
#endif

#if __has_feature(objc_arc) && __clang_major__ >= 3
#define HH_ARC_ENABLED 1
#endif

#if HH_ARC_ENABLED
#define HH_RETAIN(xx)			(xx)
#define HH_RELEASE(xx)			xx = nil
#define HH_AUTORELEASE(xx)		(xx)
#define HH_CLEAN(xx)			xx = nil
#else
#define HH_RETAIN(xx)			[xx retain]
#define HH_RELEASE(xx)			[xx release], xx = nil
#define HH_AUTORELEASE(xx)		[xx autorelease]
#define HH_CLEAN(xx)			xx = nil
#endif
