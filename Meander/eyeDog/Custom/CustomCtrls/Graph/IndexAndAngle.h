//
//  IndexAndAngle.h
//  Meander
//
//  Created by mack on 24.02.14.
//  Copyright (c) 2014 mack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IndexAndAngle : NSObject {
    int index;
    double angle;
}

-(int) getIndex;
-(double) getAngle;
-(void) setAngle: (double) newAngle;

-(id) initWithIndex: (int) theIndex
              angle: (double)theAngle;

-(NSComparisonResult) compare: (IndexAndAngle*) that;
-(NSComparisonResult) compareDESC: (IndexAndAngle*) that;

@end
