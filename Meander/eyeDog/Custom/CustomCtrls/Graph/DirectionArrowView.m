//
//  View.m
//  Meander
//
//  Created by mack on 23.02.14.
//  Copyright (c) 2014 mack. All rights reserved.
//

#import "DirectionArrowView.h"
#import "Dijkstra.h"
#import "Util.h"
#import "IndexAndAngle.h"


@implementation DirectionArrowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void) SetDirectionArrowsWithPrevious: (NSInteger) nPrevID current: (NSInteger) nCurID next: (NSInteger) nNextID step: (NSInteger) nStep nTotal: (NSInteger) nTotalStep {
    m_nPreviousID = nPrevID;
    m_nCurrentID = nCurID;
    m_nNextID = nNextID;
    m_nStep = nStep;
    m_nTotalStep = nTotalStep;
    
    NSLog(@"Prev: %d, Current: %d, Next: %d", m_nPreviousID, m_nCurrentID, m_nNextID);

    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    //[self floorPlanWithPrevious: 0 current: 1 next: 2 theRect: rect];
    //[self floorPlanWithPrevious: 45 current: 39 next: 40 theRect: rect];
    //[self floorPlanWithPrevious: 70 current: 75 next: 81 theRect: rect];
    //[self floorPlanWithPrevious: 70 current: 61 next: 67 theRect: rect];
    //[self floorPlanWithPrevious: 70 current: 61 next: 62 theRect: rect];
    //[self floorPlanWithPrevious: 70 current: 61 next: 68 theRect: rect];
    //[self floorPlanWithPrevious: 70 current: 61 next: 67 theRect: rect];
    
    if (m_nPreviousID > 0 && m_nCurrentID > 0 && m_nNextID > 0) {
        NSLog(@"Prev: %d, Current: %d, Next: %d", m_nPreviousID, m_nCurrentID, m_nNextID);

        [self floorPlanWithPrevious:m_nPreviousID current:m_nCurrentID next:m_nNextID step:m_nStep totalSteps:m_nTotalStep theRect:rect];
    } else {
        _m_bShown = NO;
    }
    
    [self.delegate GraphDrawn: _m_bShown];
}

- (void) auxAngles: (double*) angles
         numAngles: (int) numAngles
       targetIndex: (int) targetIndex
            Radius: (double) R
            radius: (double) r
   arrowHeadLength: (double) arrowHeadLength
    arrowThickness: (double) arrowThickness
        baseFactor: (double) baseFactor
    antiBaseFactor: (double) antiBaseFactor
 sidePathThickness: (double) sidePathThickness
 mainPathThickness: (double) mainPathThickness
         lineWidth: (double) lineWidth
              step: (int) step
        totalSteps: (int) totalSteps
             scale: (double) scale
           theRect: (CGRect) rect {
    R *= scale;
    r *= scale;
    arrowThickness *= scale;
    arrowHeadLength *= scale;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGColorRef white = [[UIColor whiteColor] CGColor];
    //CGColorRef black = [[UIColor blackColor] CGColor];
 //   double greyLvl = 0.68;
 //   CGColorRef grey = [[UIColor colorWithRed: greyLvl green: greyLvl blue: greyLvl alpha:1] CGColor];
    CGColorRef orange = [[UIColor colorWithRed: 0.957 green: 0.518 blue: 0.278 alpha:1] CGColor];
    CGColorRef blue = [[UIColor blueColor] CGColor];
 //   CGContextSetFillColorWithColor(ctx,  grey);
 //   CGContextFillRect(ctx, self.bounds);
    double W = self.bounds.size.width;
    double H = self.bounds.size.height;
    double cx = W/2;
    double cy = H/2;
    
    double* subAngles   = malloc(numAngles * sizeof(double));
    double* superAngles = malloc(numAngles * sizeof(double));
    double* subAnglesOrange   = malloc(numAngles * sizeof(double));
    double* superAnglesOrange = malloc(numAngles * sizeof(double));
    double sharpestAngle = M_PI;
    for (int i=0; i<numAngles; i++){
        double current = angles[i];
        double previousAngle, nextAngle;
        if (i == 0){
            previousAngle = angles[numAngles-1] - 2*M_PI;
        } else {
            previousAngle = angles[i-1];
        }
        if (i == numAngles - 1){
            nextAngle = angles[0] + 2*M_PI;
        } else {
            nextAngle = angles[i+1];
        }
        double dist2Next = nextAngle - current;
        double dist2Prev = current - previousAngle;
        double miniDist = MIN(dist2Next, dist2Prev);
        if (miniDist > 0.2){
            // "ultra sharp" angles: ignore, because they are probably data errors; hence the if
            sharpestAngle = MIN(sharpestAngle, miniDist);
        }
        subAngles[i] = current - sidePathThickness;
        superAngles[i] = current + sidePathThickness;
        
        if (i == 0 || i == targetIndex){
            subAnglesOrange[i] = current - mainPathThickness;
            superAnglesOrange[i] = current + mainPathThickness;
        } else {
            subAnglesOrange[i] = current - sidePathThickness;
            superAnglesOrange[i] = current + sidePathThickness;
        }
    }
    NSLog(@"sharpest: %8.4f", sharpestAngle);
    if (sharpestAngle <= 1.57){
        double reduceStrength = 0.5;
        reduceStrength = 0.75;   // 1 = no reduction at all, 0 = maximal reduction
        double affineMixer = sharpestAngle / 1.57;
        double reduceFactor = affineMixer * 1 + (1 - affineMixer) * reduceStrength;
        r *= reduceFactor;
    }
    double* x = malloc(numAngles * sizeof(double));
    double* y = malloc(numAngles * sizeof(double));
    double* subX = malloc(numAngles * sizeof(double));
    double* subY = malloc(numAngles * sizeof(double));
    double* superX = malloc(numAngles * sizeof(double));
    double* superY = malloc(numAngles * sizeof(double));
    double* _subX = malloc(numAngles * sizeof(double));
    double* _subY = malloc(numAngles * sizeof(double));
    double* _superX = malloc(numAngles * sizeof(double));
    double* _superY = malloc(numAngles * sizeof(double));
    double* X0 = malloc(numAngles * sizeof(double));
    double* Y0 = malloc(numAngles * sizeof(double));
    double* X1 = malloc(numAngles * sizeof(double));
    double* Y1 = malloc(numAngles * sizeof(double));
    double* subXorange = malloc(numAngles * sizeof(double));
    double* subYorange = malloc(numAngles * sizeof(double));
    double* superXorange = malloc(numAngles * sizeof(double));
    double* superYorange = malloc(numAngles * sizeof(double));
    double* _subXorange = malloc(numAngles * sizeof(double));
    double* _subYorange = malloc(numAngles * sizeof(double));
    double* _superXorange = malloc(numAngles * sizeof(double));
    double* _superYorange = malloc(numAngles * sizeof(double));
    double* X0orange = malloc(numAngles * sizeof(double));
    double* Y0orange = malloc(numAngles * sizeof(double));
    double* X1orange = malloc(numAngles * sizeof(double));
    double* Y1orange = malloc(numAngles * sizeof(double));
    double epsilon = 0.5;
    double antiEpsilon = 1 - epsilon;
    for (int i=0; i<numAngles;  i++){
        angles[i] += M_PI/2;
        subAngles[i] += M_PI/2;
        superAngles[i]  += M_PI/2;
        subAnglesOrange[i] += M_PI/2;
        superAnglesOrange[i]  += M_PI/2;
        x[i] = cx + cos(angles[i]) * r;
        y[i] = cy + sin(angles[i]) * r;
        subX[i] = cx + cos(subAngles[i]) * r;
        subY[i] = cy + sin(subAngles[i]) * r;
        superX[i] = cx + cos(superAngles[i]) * r;
        superY[i] = cy + sin(superAngles[i]) * r;
        _superX[i] = (cx - cos(subAngles[i]) * r) * antiEpsilon + superX[i] * epsilon;
        _superY[i] = (cy - sin(subAngles[i]) * r) * antiEpsilon + superY[i] * epsilon;
        _subX[i] = (cx - cos(superAngles[i]) * r) * antiEpsilon + subX[i] * epsilon;
        _subY[i] = (cy - sin(superAngles[i]) * r) * antiEpsilon + subY[i] * epsilon;
        
        subXorange[i] = cx + cos(subAnglesOrange[i]) * r;
        subYorange[i] = cy + sin(subAnglesOrange[i]) * r;
        superXorange[i] = cx + cos(superAnglesOrange[i]) * r;
        superYorange[i] = cy + sin(superAnglesOrange[i]) * r;
        _superXorange[i] = (cx - cos(subAnglesOrange[i]) * r) * antiEpsilon + superXorange[i] * epsilon;
        _superYorange[i] = (cy - sin(subAnglesOrange[i]) * r) * antiEpsilon + superYorange[i] * epsilon;
        _subXorange[i] = (cx - cos(superAnglesOrange[i]) * r) * antiEpsilon + subXorange[i] * epsilon;
        _subYorange[i] = (cy - sin(superAnglesOrange[i]) * r) * antiEpsilon + subYorange[i] * epsilon;
    }
    for (int i=0; i<numAngles; i++){
        double angle = angles[i];
        double dx = R * cos(angle);
        double dy = R * sin(angle);
        double dxOrange = dx;
        double dyOrange = dy;
        if (i == 0){
            dxOrange *= baseFactor;
            dyOrange *= baseFactor;
        }
        if (i == targetIndex){
            dxOrange *= antiBaseFactor;
            dyOrange *= antiBaseFactor;
        }
        X0[i] = subX[i] + dx;
        Y0[i] = subY[i] + dy;
        X1[i] = superX[i] + dx;
        Y1[i] = superY[i] + dy;
        
        X0orange[i] = subXorange[i] + dxOrange;
        Y0orange[i] = subYorange[i] + dyOrange;
        X1orange[i] = superXorange[i] + dxOrange;
        Y1orange[i] = superYorange[i] + dyOrange;
    }
    CGContextSetLineWidth(ctx, lineWidth);
    //CGContextSetStrokeColorWithColor(ctx, black);
    CGContextSetStrokeColorWithColor(ctx, orange);
    CGContextSetFillColorWithColor(ctx, white);
    /*for (int i=1; i<numAngles; i++){
     if (i != targetIndex){
     }
     }*/
    for (int i=0; i<numAngles; i++){
        CGContextMoveToPoint(ctx, _subX[i], _subY[i]);
        CGContextAddLineToPoint(ctx, X0[i], Y0[i]);
        CGContextAddLineToPoint(ctx, X1[i], Y1[i]);
        CGContextAddLineToPoint(ctx, _superX[i], _superY[i]);
        CGContextAddLineToPoint(ctx, _subX[i], _subY[i]);
        CGContextFillPath(ctx);
    }
    int t = targetIndex;
    BOOL intersection1Exists = [Util linearIndependentWithX0:X1orange[0] y0:Y1orange[0] x1:superXorange[0] y1:superYorange[0] x2:X0orange[t] y2:Y0orange[t] x3:subXorange[t]   y3:subYorange[t]];
    BOOL intersection2Exists = [Util linearIndependentWithX0:X0orange[0] y0:Y0orange[0] x1:subXorange[0]   y1:subYorange[0]   x2:X1orange[t] y2:Y1orange[t] x3:superXorange[t] y3:superYorange[t]];
    CGPoint intersection1;
    CGPoint intersection2;
    if (intersection1Exists){
        intersection1 = [Util intersectionWithX0:X1orange[0] y0:Y1orange[0] x1:superXorange[0] y1:superYorange[0] x2:X0orange[t] y2:Y0orange[t] x3:subXorange[t]   y3:subYorange[t]];
    }
    if (intersection2Exists){
        intersection2 = [Util intersectionWithX0:X0orange[0] y0:Y0orange[0] x1:subXorange[0]   y1:subYorange[0]   x2:X1orange[t] y2:Y1orange[t] x3:superXorange[t] y3:superYorange[t]];
    }
    double angle = angles[t];
    double RR = R * antiBaseFactor + arrowHeadLength;
    double arrowHeadX = x[t] + RR * cos(angle);
    double arrowHeadY = y[t] + RR * sin(angle);
    double arrowLeftX  = X0orange[t] + arrowThickness * sin(angle);
    double arrowLeftY  = Y0orange[t] - arrowThickness * cos(angle);
    double arrowRightX = X1orange[t] - arrowThickness * sin(angle);
    double arrowRightY = Y1orange[t] + arrowThickness * cos(angle);
    CGContextSetFillColorWithColor(ctx, orange);
    
    BOOL notAlmostStraight = (fabs(angles[t] - angles[0] - M_PI) >= 0.1);
    
    for (int mode = 0; mode <= 1; mode++){
        CGContextMoveToPoint(ctx, X1orange[0], Y1orange[0]);                             // start left
        // in between previous and next: (xx[2], yy[2]);
        if (intersection1Exists && notAlmostStraight){
            NSLog(@"intersection 1 exists");
            CGContextAddLineToPoint(ctx, intersection1.x, intersection1.y);  // mid left
        }
        // in between previous and next: (xx[t2], yy[t2]);
        CGContextAddLineToPoint(ctx, X0orange[t], Y0orange[t]);                          // target left
        CGContextAddLineToPoint(ctx, arrowLeftX,  arrowLeftY);               // arrow left base
        CGContextAddLineToPoint(ctx, arrowHeadX,  arrowHeadY);               // arrow head
        CGContextAddLineToPoint(ctx, arrowRightX, arrowRightY);              // arrow right base
        CGContextAddLineToPoint(ctx, X1orange[t], Y1orange[t]);                          // target right
        //CGContextAddLineToPoint(ctx, xx[t2 + 2], yy[t2 + 2]);
        if (intersection2Exists && notAlmostStraight){
            NSLog(@"intersection 2 exists");
            CGContextAddLineToPoint(ctx, intersection2.x, intersection2.y);  // mid right
        }
        //CGContextAddLineToPoint(ctx, xx[0], yy[0]);
        CGContextAddLineToPoint(ctx, X0orange[0], Y0orange[0]);                          // start right
        CGContextAddLineToPoint(ctx, X1orange[0], Y1orange[0]);                          // start left
        if (mode == 0){
            CGContextFillPath(ctx);
        } else {
            CGContextStrokePath(ctx);
        }
    }
    CGContextSetFillColorWithColor(ctx,  blue);
    CGContextBeginPath(ctx);
    double blueXStart = 0.5 * (X0orange[0] + X1orange[0]);
    double blueYStart = 0.5 * (Y0orange[0] + Y1orange[0]);
    double blueMixer = ((double)step)/((double)(1+totalSteps));
    double blueRexim = 1 - blueMixer;
    double blueX = blueMixer * cx + blueRexim * blueXStart;
    double blueY = blueMixer * cy + blueRexim * blueYStart;
    double blueRadius = r * 0.5;
    double startAngle = 0;
    double endAngle = 2 * M_PI;
    int clockwise = YES;
    CGContextAddArc(ctx, blueX, blueY, blueRadius, startAngle, endAngle, clockwise);
    CGContextFillPath(ctx);
}


- (void) floorPlanWithPrevious: (int) previousNodeId
                       current: (int) currentNodeId
                          next: (int) nextNodeId
                          step: (int) step
                    totalSteps: (int) totalSteps
                       theRect: (CGRect) rect{
    Dijkstra* dijkstra = [Dijkstra sharedInstance];
    Node* current =  [dijkstra getNodeFromId: currentNodeId];
    int curX = [current getX];
    int curY = [current getY];
    int curLvl = [current getLevel];
    int numNeighbours = [current getNumNeighbours];
    int* neighbours = [current getNeighbours];
    NSMutableArray* connections = [[NSMutableArray alloc] init];
    for (int i = 0; i < numNeighbours; i++){
        int neighb = neighbours[i];
        Node* neighbour = [dijkstra getNodeFromId: neighb];
        int nX = [neighbour getX];
        int nY = [neighbour getY];
        int nLvl = [neighbour getLevel];
        if (curLvl == nLvl){
            int dx = nX - curX;
            int dy = nY - curY;
            //double angle = atan2(dy, dx);
            double angle = atan2(-dx, dy);
            IndexAndAngle* iaa = [[IndexAndAngle alloc] initWithIndex: neighb angle: angle];
            [connections addObject: iaa];
        }
    }
    int sourcePosition = -1;
    int targetPosition = -1;
    //NSArray* sortedConnections = [connections sortedArrayUsingSelector: @selector(compareDESC:)];
    NSArray* sortedConnections = [connections sortedArrayUsingSelector: @selector(compare:)];
    int k = [sortedConnections count];
    for (int i=0; i<k; i++){
        IndexAndAngle* iaa = [sortedConnections objectAtIndex: i];
        if ([iaa getIndex] == previousNodeId){
            sourcePosition = i;
        }
        if ([iaa getIndex] == nextNodeId){
            targetPosition = i;
        }
        NSLog(@"index: %d, angle: %8.4f", [iaa getIndex], [iaa getAngle]);
    }
    NSLog(@"source position: %d", sourcePosition);
    NSLog(@"target position: %d", targetPosition);
    
    _m_bShown = YES;
    
    if (sourcePosition < 0) {
        _m_bShown = NO;
        NSLog(@"Error!");
        return;
    }
    
    if (targetPosition < 0) {
        _m_bShown = NO;
        NSLog(@"Error!");
        return;
    }

    double sourceAngle = [[sortedConnections objectAtIndex: sourcePosition] getAngle];
    double* angles = malloc(k * sizeof(double));
    int position = sourcePosition;
    int targetIndex = -1;
    for (int i=0; i<k; i++){
        double adjustedAngle = [[sortedConnections objectAtIndex: position] getAngle] - sourceAngle;
        if (adjustedAngle < 0){
            adjustedAngle += (2 * M_PI);
        }
        angles[i] = adjustedAngle;
        NSString* blah = @"";
        if (position == targetPosition){
            targetIndex = i;
            blah = @"target";
        }
        NSLog(@"node: %d, angle: %8.4f, %@", [[sortedConnections objectAtIndex: position] getIndex], angles[i], blah);
        position++;
        if (position == k){
            position = 0;
        }
    }
    //[self auxAngles: angles numAngles: k targetIndex: targetIndex Radius: 41 radius: 14 arrowHeadLength: 33 arrowThickness: 12 baseFactor: 1.4 sidePathThickness: 0.3 mainPathThickness: 0.78 lineWidth: 0.6 scale: 0.3 theRect: rect];
    [self auxAngles: angles numAngles: k targetIndex: targetIndex Radius: 41 radius: 14 arrowHeadLength: 23 arrowThickness: 8 baseFactor: 0.9 antiBaseFactor: 0.2 sidePathThickness: 0.6 mainPathThickness: 0.32 lineWidth: 0.6 step: step totalSteps: totalSteps scale: 0.5 theRect: rect];
}


@end
