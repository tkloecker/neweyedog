//
//  View.h
//  Meander
//
//  Created by mack on 23.02.14.
//  Copyright (c) 2014 mack. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DirectionArrowViewProtocol

- (void) GraphDrawn: (BOOL) bShown;

@end

@interface DirectionArrowView : UIView {
    NSInteger m_nPreviousID;
    NSInteger m_nCurrentID;
    NSInteger m_nNextID;
    NSInteger m_nStep;
    NSInteger m_nTotalStep;
}

@property (nonatomic, assign) id <DirectionArrowViewProtocol> delegate;

@property (nonatomic) BOOL m_bShown;

- (void) SetDirectionArrowsWithPrevious: (NSInteger) nPrevID current: (NSInteger) nCurID next: (NSInteger) nNextID step: (NSInteger) nStep nTotal: (NSInteger) nTotalStep;

@end
