//
//  IndexAndAngle.m
//  Meander
//
//  Created by mack on 24.02.14.
//  Copyright (c) 2014 mack. All rights reserved.
//

#import "IndexAndAngle.h"

@implementation IndexAndAngle

-(int) getIndex {
    return index;
}
-(double) getAngle {
    return angle;
}
-(void) setAngle: (double) newAngle {
    angle = newAngle;
}

-(id) initWithIndex: (int) theIndex
              angle: (double)theAngle {
    self = [super init]; if (!self) return nil;
    index = theIndex;
    angle = theAngle;
    return self;
}

-(NSComparisonResult) compare: (IndexAndAngle*) that {
    double otherAngle = [that getAngle];
    if (angle > otherAngle){
        return 1;
    }
    if (angle < otherAngle){
        return -1;
    }
    return 0;
}

-(NSComparisonResult) compareDESC: (IndexAndAngle*) that {
    double otherAngle = [that getAngle];
    if (angle > otherAngle){
        return -1;
    }
    if (angle < otherAngle){
        return 1;
    }
    return 0;
}

@end
