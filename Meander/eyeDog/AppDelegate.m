//
//  AppDelegate.m
//  Meander
//
//  Created by Wony on 2/5/14.
//  Copyright (c) 2014 Wony Lin. All rights reserved.
//

#import "AppDelegate.h"
#import "HHTabListController.h"
#import "InitialLoadingViewController.h"
#import "MainViewController.h"
#import "ProductSearchViewController.h"
#import "EmployeeSearchViewController.h"
#import "MultiplePOIViewController.h"
#import "SettingsViewController.h"
#import "NotAvailableViewController.h"

@interface UINavigationController (CustomStatus)

@end

@implementation UINavigationController (CustomStatus)

- (void) viewDidLoad {
    [self setNeedsStatusBarAppearanceUpdate];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end

@implementation AppDelegate

@synthesize viewController = _viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.viewController = [[InitialLoadingViewController alloc] initWithNibName:@"InitialLoadingViewController" bundle: nil];
    
    self.window.rootViewController = self.viewController;
    
    [self.window makeKeyAndVisible];
    
    ;
    NSString *language = [[NSUserDefaults standardUserDefaults] stringForKey:@"language"];
    
    if (language == nil || [language isKindOfClass: [NSNull class]] == YES || [language length] <= 0) {
        language = @"English";
    }
    
    [[DataKeeper sharedInstance] setM_strLanguage: language];


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(BeginApponMainThread) name:NOTIFICATION_END_INIT2 object:nil];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[DataKeeper sharedInstance] Initialize];
    

    //[self BeginApp];

    return YES;
}

- (void) BeginApponMainThread {
    [self performSelectorOnMainThread:@selector(BeginApp) withObject:nil waitUntilDone: YES];
}

- (void) BeginApp {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    
    DataKeeper *dataKeeper = [DataKeeper sharedInstance];
    [dataKeeper loadDataFromFile];
    
	NSMutableArray *viewControllers = [NSMutableArray arrayWithCapacity: 5];
    
    MainViewController *mainVC = (ISiPhone5) ? [[MainViewController alloc] initWithNibName:@"MainViewController" bundle: nil] : [[MainViewController alloc] initWithNibName:@"MainViewController-iPhone4" bundle: nil];
    [mainVC setTitle: [dataKeeper getTextByID: 22]];
    
    UINavigationController *navMainVC = [[UINavigationController alloc] initWithRootViewController: mainVC];
    
    [navMainVC setNavigationBarHidden: YES];
    navMainVC.navigationBar.tintColor = [UIColor colorWithRed:0.235 green:0.498 blue:0.54 alpha:1];
    
    ProductSearchViewController *productsearchVC = [[ProductSearchViewController alloc] initWithNibName:@"ProductSearchViewController" bundle: nil];
    [productsearchVC setTitle: [dataKeeper getTextByID: 23]];
    UINavigationController *navProductVC = [[UINavigationController alloc] initWithRootViewController: productsearchVC];
    [navProductVC setNavigationBarHidden: YES];
    
    EmployeeSearchViewController *employeesearchVC = [[EmployeeSearchViewController alloc] initWithNibName:@"EmployeeSearchViewController" bundle: nil];
    [employeesearchVC setTitle: [dataKeeper getTextByID: 24]];
    
    UINavigationController *navSearchVC = [[UINavigationController alloc] initWithRootViewController: employeesearchVC];
    [navSearchVC setNavigationBarHidden: YES];
    
    MultiplePOIViewController *multiVC = [[MultiplePOIViewController alloc] initWithNibName:@"MultiplePOIViewController" bundle: nil];
    [multiVC setTitle: [dataKeeper getTextByID: 25]];
    UINavigationController *navMultiVC = [[UINavigationController alloc] initWithRootViewController: multiVC];
    [navMultiVC setNavigationBarHidden: YES];
    
    SettingsViewController *settingsVC = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle: nil];
    [settingsVC setTitle: [dataKeeper getTextByID: 26]];
    UINavigationController *navSettingsVC = [[UINavigationController alloc] initWithRootViewController: settingsVC];
    
    [navSettingsVC setNavigationBarHidden: YES];
    
    
    [viewControllers addObject: navMainVC];
//    [viewControllers addObject: navProductVC];
//    [viewControllers addObject: navSearchVC];
//    [viewControllers addObject: navMultiVC];
    
    //Not available controller should be replaced when logic will be implemented
    NotAvailableViewController *notAvailableVC = [NotAvailableViewController new];
    [notAvailableVC setTitle:[dataKeeper getTextByID: 23]];
    [viewControllers addObject: notAvailableVC];
    
    notAvailableVC = [NotAvailableViewController new];
    [notAvailableVC setTitle:[dataKeeper getTextByID: 24]];
    [viewControllers addObject: notAvailableVC];
    
    notAvailableVC = [NotAvailableViewController new];
    [notAvailableVC setTitle:[dataKeeper getTextByID: 25]];
    [viewControllers addObject: notAvailableVC];
    
    [viewControllers addObject: navSettingsVC];
	
	self.viewController = [[HHTabListController alloc] initWithViewControllers:viewControllers];
    
    [self.window setRootViewController: self.viewController];
        
  //  [self.window makeKeyAndVisible];
	
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
